# Deep Revelations
This codebase implements a deep-learning based social network.

## Overview
This codebase implements the `DimensionRank` algorithm for sorting posts in a social network.

The goal is that for each user, we can compute a *personalized* probability for whether or not the
user *likes* and *agrees* with the post.

![Probability Example](img/probabilities.png)

The probability is currently revealed to the user after they make a decision.

Each user is represented by an `embedding vector` in a real-dimensional space.
This is a standard technique in terms of Deep Learning and natural langauge
processing. See, e.g., [here](https://towardsdatascience.com/why-do-we-use-embeddings-in-nlp-2f20e1b632d2).

Each post is also represented in by a unique embedding vector.

Each post can be rated on multiple dimensions. For example, a user can specify that whether the `like` a post.
Separately, the user can specify whether they `agree` with a post.
We can also look at whether or not a user `clicks` on a post.
Each of these questions turns into a multi-class (e.g., binary) prediction problem.

Then, for each user *u*, each post *p*, and each attribute *a*, we can efficiently compute whether or not
*u* assigns *a* to *p*.

## System Components

There are three basic components to the architecture:

1. **frontend**: Javascript/Vue frontend, for the web application
1. **backend**: Node.js backend server, that receives REST requests from the frontend
1. **machine learning**: python/PyTorch micro-services that run neural networks on the user data, to propagate information back to the database

Underlying this system, we use two databases:

1. **persistent**: MongoDB for the posts, user data, and other data that should be persisted to hard disk
1. **RAM**: Redis for data store in memory, like neural network parameters

## Data Flow in the System
### Labeling a Post
The **user** interacts with the **Vue frontend**. This frontend sends its requests to the **Node.js backend server**.

The user can take two primary actions:

1. create a post
1. label a post

Each new label is added to the Redis queue by the Node.js backend. This label is then picked up by the `nn_server` python backend. The python server uses the new label to make parameter updates. An index for the post that was just labeled is added to another queue, and is picked up by the `update_mongo` sub-service, which copies the latest parameters values for the updated post from Redis to Mongo database.

### Retrieving the Data
When data is retrieved, the neural network parameters characterizing 1) the user and 2) each post, are returned from the backend to the frontend.

Then, the frontend code can perform the final layer of the neural network calculations, to come up with a specific probability value that the user will like or dislike, and agree or disagree with, each post that is received.

The probabilities computed on the frontend can be used to create custom sorts for the user.

## Running the Code
### Clone the Codebase
To clone the codebase, run:

```
git clone https://gitlab.com/deeprevelations/dimensionrank.git
```

### Vue server
To run a Vue server on port 8080, do:

```
cd ~/dimensionrank/nuxt
npm install
npm run dev
```

### Node server
To run a node server on port 3000, do:

```
cd ~/dimensionranknode
npm install
node server.js --config ${CONFIG_FILE}
```

Here, `${CONFIG_FILE}` should have the form:
```
{
    "port": 3000,
    "mongo": {
        "uri": "mongodb+srv://dbUser:dbPassword@yourserver.mongodb.net/dbName?retryWrites=true&w=majority"
    },
    "redis": {
        "host": "yourdatabase.yourprovider.com"
    },
    "vueBasePath": "https://yoursite.com",
    "sendgridApiKey": "SG.yourkeyaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
}
```

### NGINX
Nginx on Ubuntu can be used to tie these ports together.

```
cd ~/dimensionrank/html
./nginx/install.sh development
```

### Python/PyTorch

The final component are the python micro-services that run the deep learning update servers.

```
cd ~/dimensionrank/py
./install.sh
./nn_server.sh
```

Then, in another window, run:
```
cd ~/dimensionrank/py
./mongo_server.sh
```
