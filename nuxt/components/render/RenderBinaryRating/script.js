import cookies from '@/lib/cookies.js'
import objects from '@/lib/objects.js'
import network from '@/lib/network.js'

function AddRating(rating, delta, instance) {
    console.log('AddRating', {
        rating,
        delta,
    })
    if (rating == 0) {
        instance.total0 += delta
    } else if (rating == 1) {
        instance.total1 += delta
    }
}

export default {
    name: 'RenderBinaryRating',
    props: {
        picture1: String,
        picture0: String,
        stats: Object,
        originalRating: Number,
        currentRating: Number,
    },
    components: {},
    data() {
        return {
            total1: this.stats[1],
            total0: this.stats[0],
        }
    },
    methods: {},
    created() {
        true && console.log('RenderBinaryRating', {})

        if (this.originalRating != this.currentRating) {
            AddRating(this.originalRating, -1, this)
            AddRating(this.currentRating, 1, this)
        }
    },
    computed: {},
    mounted() {},
    watch: {
        'currentRating'(to, from) {
            AddRating(from, -1, this)
            AddRating(to, 1, this)
        }
    },
}