import cookies from '~/lib/cookies.js'
import network from '~/lib/network.js'
import cookieTokens from '~/lib/cookieToken.js'

import RenderBroadcast from '~/components/render/RenderBroadcast/index.vue'
import BroadcastWrapper from '~/components/render/BroadcastWrapper/index.vue'
import BinaryInput from '~/components/input/BinaryInput/index.vue'
import UnaryInput from '~/components/input/UnaryInput/index.vue'

function Filter(params, instance) {
    true && console.log('Filter', {
        params
    })
    const filter = params.filter
    //    if (filter < 0) {
    //    }

    // Assume there is a filter.
    var buffer = []
    for (const broadcast of params.unsorted) {
        // There is no filter, so check if we are equal.
        if (filter < 0) {
            buffer.push(broadcast)
        } else if (broadcast.attachment) {
            const agree = broadcast.attachment[1]
            if (agree == filter) {
                buffer.push(broadcast)
            }
        }
    }
    return buffer
}

function Sort(params, unsorted) {
    true && console.log('Sort', {
        params,
        unsorted,
    })
		console.log('a')
    const sort = params.sort
    var pairs = []
		console.log('b')
    for (const broadcast of unsorted) {
		console.log('d')
        if (sort == 0) {
		console.log('d1')
            // New / Time
            pairs.push({
                load: broadcast,
                score: broadcast.details.creationTime,
            })
        } else {
		console.log('d2', {broadcast})
            // Hot
            pairs.push({
                load: broadcast,
                score: broadcast.stats[0][1],
            })
        }
    }
		console.log('c')
    true && console.log('Sort', {
        pairs
    })

		console.log('d')
    pairs.sort(function(a, b) {
        if (a.score < b.score) {
            return 1
        } else
        if (a.score > b.score) {
            return -1
        } else {
            return 0
        }
    })

    true && console.log('Sort', {
        pairs
    })

    var buffer = []
    for (const pair of pairs) {
        buffer.push(pair.load)
    }
    return buffer
}

function SortBroadcasts(params, instance) {
    true && console.log('SortBroadcasts', {
        params
    })
    const filtered = Filter(params, instance)
    true && console.log('SortBroadcasts', {
        filtered
    })
    const sorted = Sort(params, filtered)
    true && console.log('SortBroadcasts', {
        sorted
    })
    instance.sorted = sorted
}

function FetchComments(params, instance) {
    true && console.log('FetchComments', {
        params
    })
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    const options = {
        cookieToken,
        reference: params.reference,
    }
    network.NodeCall('/query/comments', options)
        .then(result => {
            true && console.log('FetchComments', {
                result
            })

            console.log({
                data: result.data
            })
            const unsorted = result.data.results
            instance.unsorted = unsorted

            SortBroadcasts({
                ...params,
                unsorted,
            }, instance)
        }).catch(error => {
            true && console.log('FetchComments', {
                error
            })
        })
}

export default {
    name: 'FactCheck',
    props: {
        reference: String,
    },
    components: {
        BroadcastWrapper,
        RenderBroadcast,
        BinaryInput,
        UnaryInput,
    },
    data() {
        return {
            unsorted: null,
            sorted: null,
            filter: -1,
            sort: 1,
            sortedKey: '',
        }
    },
    methods: {
        DoResort() {
            true && console.log('DoResort', {})
            SortBroadcasts({
                filter: this.filter,
                sort: this.sort,
                unsorted: this.unsorted,
            }, this)
            this.sortedKey = (new Date()).toString()
        },
        DoReload() {
            true && console.log('DoReload', {})
            FetchComments({
                filter: this.filter,
                sort: this.sort,
                reference: this.reference,
            }, this)
        },
        HandleHot(newValue) {
            console.log('HandleHot', {
                newValue
            })
            this.sort = newValue
            this.DoResort()
        },
        HandleAgree(newValue) {
            console.log('HandleAgree', {
                newValue
            })
            this.filter = newValue
            this.DoResort()
        },
    },
    created() {
        true && console.log('FactCheck', {})
        cookieTokens.ValidateCookieTokenOrRoute({}, this)

        this.DoReload()
    },
    computed: {},
    watch: {
        '$store.state.factCheckBuffer'(to) {
            this.unsorted.push(to)
            this.DoResort()
        }
    },
}
