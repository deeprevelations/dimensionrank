import cookies from '@/lib/cookies.js'
import logging from '@/lib/logging.js'
import network from '@/lib/network.js'
import localVoteMath from '@/lib/localVoteMath.js'

import RenderTime from '@/components/render/RenderTime/index.vue'
import RenderResource from '@/components/render/RenderResource/index.vue'
import RenderBinaryRating from '@/components/render/RenderBinaryRating/index.vue'
import RenderAttachment from '@/components/render/RenderAttachment/index.vue'
import ResponseBar from '@/components/input/ResponseBar/index.vue'
import DeleteButton from '@/components/input/DeleteButton/index.vue'
import ToxicButton from '@/components/input/ToxicButton/index.vue'
import MakeBroadcast from '@/components/input/MakeBroadcast/index.vue'

export default {
    name: 'RenderBroadcast',
    props: {
        broadcast: Object,
        isFocused: Boolean,
        asRoot: Boolean,
        showControls: Boolean,
    },
    components: {
        RenderTime,
        ResponseBar,
        RenderResource,
        RenderBinaryRating,
        DeleteButton,
        ToxicButton,
        MakeBroadcast,
        RenderAttachment,
    },
    data() {
        return {
            isToxic: false,
            isDeleted: false,
            currentHot: this.broadcast.selfRatings[0],
            currentAgree: this.broadcast.selfRatings[1],
            currentReply: false,
            keyMakeBroadcast: '',
            responseKey: '',
            tokens: [],
        }
    },
    methods: {
        HandleToxicClick() {
            true && console.log('HandleToxicClick')
            this.isToxic = true
        },
        HandleDeleteClick() {
            true && console.log('HandleDeleteClick')
            this.isDeleted = true
        },
        HandleNewHot(newValue) {
            console.log('HandleNewHot 2', newValue)
            this.currentHot = newValue
        },
        HandleNewAgree(newValue) {
            console.log('HandleNewAgree 2', newValue)
            this.currentAgree = newValue
        },
        HandleReply(newValue) {
            console.log('HandleReply 2', newValue)
            this.currentReply = newValue
        },
        HandleSubmitSuccessEvent(broadcast) {
            console.log('HandleSubmitSuccessEvent')
            this.currentReply = false;
            this.keyMakeBroadcast = (new Date()).toString()
            this.responseKey = (new Date()).toString()
            this.$emit('ReplySuccess', broadcast)
        },
        ComputeTokens() {
            var buffer = ''
            var lastTokenSubstantive = false
            for (const token of this.broadcast.details.tokens) {
                if (network.IsUrl(token)) {
                    // pass
                } else if (token.startsWith('<br>') && !lastTokenSubstantive) {
                    lastTokenSubstantive = false
                } else {
                    buffer += token + ' '
                    lastTokenSubstantive = true
                }
            }
            return buffer
        },
    },
    computed: {
        IsFocused() {
            true && console.log('IsFocused', {
                isFocused: this.isFocused
            })
            return this.isFocused
        },
        ShowComments() {
            return !this.broadcast.reference
        },
        AuthorUserName() {
            return '@' + this.broadcast.details.userName
        },
        UserLink() {
            return '/user/' + this.broadcast.details.userName
        },
        RenderTime() {
            const seconds = this.broadcast.details.creationTime
            const date = new Date(seconds)
            return date.toString().split('GMT')[0]
        },
        ShowTokens() {
            return this.tokens.length > 0
        },
        RenderTokens() {
            true && console.log('RenderTokens')
            return this.tokens
        },
        ShowInsight() {
            for (const token of this.broadcast.details.tokens) {
                if (network.IsUrl(token)) {
                    // pass
                } else {
                    return true
                }
            }
            return false
        },
        BroadcastTitleClass() {
            if (this.asRoot) {
                return 'WindowTitle OriginalTitle'
            } else {
                const rating = this.broadcast.attachment ? this.broadcast.attachment[1] : null
                console.log('BroadcastTitleClass', {
                    rating
                })
                if (rating == 1) {
                    return 'WindowTitle AgreeTitle'
                } else if (rating == 0) {
                    return 'WindowTitle DisagreeTitle'
                } else {
                    return 'WindowTitle NeutralTitle'
                }
            }
        },
        ShowToxic() {
            return this.isToxic
        },
        ShowDeleted() {
            return this.isDeleted
        },
        ShowBroadcast() {
            return !this.isToxic && !this.isDeleted
        },
    },
    created() {
        true && console.log('RenderBroadcast')
        this.tokens = this.ComputeTokens()
    },
}
