import cookies from '@/lib/cookies.js'
import logging from '@/lib/logging.js'
import network from '@/lib/network.js'

export default {
    name: 'RenderNotification',
    props: {
        notification: Object,
    },
    components: {},
    data() {
        return {}
    },
    created() {
        true && console.log('RenderNotification:created', {
            notification: this.notification,
        })
    },
    methods: {},
    computed: {},
}
