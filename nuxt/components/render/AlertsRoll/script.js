import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import feedLike from '@/lib/feedLike.js'
import cookieTokens from '@/lib/cookieToken.js'
import dataInterface from '@/lib/dataInterface.js'
import render from '@/lib/render/broadcast.js'

//import PageHeader from '@/components/info/PageHeader/index.vue'
//import HeaderBar from '@/components/info/HeaderBar/index.vue'
// import RenderNotification from '@/components/render/RenderNotification/index.vue'
import RenderPair from '@/components/render/RenderPair/index.vue'

function LoadNotifications(params, instance) {
    true && console.log('LoadNotifications', {
        params
    })
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    const options = {
        cookieToken,
        channelType: 'notification',
        rankingAlgorithm: 'new',
    }
    network.NodeCall('/notification/list', options)
        .then(result => {
            true && console.log('LoadNotifications', {
                result
            })
            if (result.data.success) {
                instance.alerts = result.data.alerts
            } else {
                true && console.log('LoadNotifications', 'error')
            }
        }).catch(error => {
            true && console.log('LoadNotifications', {
                error
            })
        })
}

export default {
    name: 'AlertsRoll',
    components: {
        RenderPair,
    },
    data() {
        return {
            alerts: [],
        }
    },
    methods: {},
    created() {
//        cookieTokens.ValidateCookieTokenOrRoute({
//            successUrl: null,
//        }, this)
        LoadNotifications({
        }, this)
    },
    mounted() {},
}
