// import Intersect from 'vue-intersect'

import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import cookieTokens from '@/lib/cookieToken.js'

import RenderBroadcast from '@/components/render/RenderBroadcast/index.vue'

function sigmoid(t) {
    return 1 / (1 + Math.pow(Math.E, -t))
}

function ComputeSigmoid(left, right) {
    console.log('ComputeSigmoid')
    var dot_product = 0.0
    for (let [i, l] of left.entries()) {
        // const part = left[i] * right[i]
        const part = l * right[i]
        console.log({
            part
        })
        dot_product += part
    }

    const probability = sigmoid(dot_product)
    console.log({
        dot_product,
        probability,
    })
    return probability

}

function VectorFromString(embedding_string) {
    console.log('VectorFromString')
    if (!embedding_string) {
        return null
    }
    const str_parts = embedding_string.split(' ')
    var buffer = []
    for (const part of str_parts) {
        console.log({
            part
        })
        const f = parseFloat(part)
        console.log({
            f
        })
        buffer.push(f)
    }
    return buffer
}

function AddUserScoresInternal(data, instance, index) {
    console.log('AddUserScoresInternal', index)
    console.log({
        data
    })

    const task_index = 'task_embedding' + index.toString()
    const consumer_embedding_string = data.user_embedding
    const consumer_embedding = VectorFromString(consumer_embedding_string)
    console.log({
        consumer_embedding
    })
    data.results.forEach((broadcast, inner_index) => {
        if (broadcast[task_index]) {
            const post_embedding = VectorFromString(broadcast[task_index])

            if (consumer_embedding && post_embedding) {
                const probability = ComputeSigmoid(consumer_embedding, post_embedding)
                const prob_key = 'prob' + index.toString()
                broadcast[prob_key] = probability
                data.results[inner_index] = broadcast
                console.log('copied', broadcast)
            }
        } else {
            //        console.log('missed', {
            //            task_index
            //        })
        }
    })
}


function AddUserScores(data, instance) {
    AddUserScoresInternal(data, instance, 0)
    AddUserScoresInternal(data, instance, 1)
    for (var broadcast of data.results) {
        console.log('broadcast', broadcast)
    }
}

function FetchNewBatch(params, instance) {
    true && console.log('FetchNewBatch', {
        params
    })
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    const options = {
        cookieToken,
        objectType: params.queryRequest.objectType,
        channelType: params.queryRequest.channelType,
        channelName: params.queryRequest.channelName,
        rankingAlgorithm: params.rankingAlgorithm,
        searchTerm: params.searchTerm,
    }
    network.NodeCall('/query/batch', options)
        .then(result => {
            true && console.log('FetchNewBatch', {
                result
            })
            AddUserScores(result.data, instance)
            instance.broadcasts = result.data.results
        }).catch(error => {
            true && console.log('FetchNewBatch', {
                error
            })
            // output.reject(error)
        })
}

export default {
    name: 'QueryFeed',
    props: {
        queryRequest: Object,
        searchTerm: String,
    },
    components: {
        //        Intersect,
        RenderBroadcast,
    },
    data() {
        return {
            broadcasts: [],
            broadcastIdSet: new Set(),
            stopLoading: false,
        }
    },
    methods: {
        DoReload() {
            const queryRequest = this.queryRequest
            true && console.log('DoReload', {
                queryRequest
            })
            FetchNewBatch({
                queryRequest,
                rankingAlgorithm: this.$store.state.rankingAlgorithm,
                searchTerm: this.searchTerm,
            }, this)
        },
        //        HandleIntersection(e) {
        //            true && console.log('HandleIntersection', {
        //                stopLoading: this.stopLoading
        //            })
        //            if (this.stopLoading) {
        //                true && console.log('HandleIntersection', 'not-loading')
        //            } else {
        //                this.DoReload()
        //            }
        //        },
    },
    created() {
        true && console.log('QueryFeed', {
            queryRequest: this.queryRequest,
        })
        this.DoReload()
    },
    computed: {},
    watch: {},
}
