import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import cookieTokens from '@/lib/cookieToken.js'
import dataInterface from '@/lib/dataInterface.js'
import render from '@/lib/render/broadcast.js'

import RenderBroadcast from '@/components/render/RenderBroadcast/index.vue'

function LoadReferenceChain(params, instance) {
    true && console.log('LoadReferenceChain', {
        params
    })
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    const options = {
        cookieToken
    }
    dataInterface.RetrieveBroadcast({
        broadcastId: params.broadcast.reference,
    }).then(broadcast => {
        true && console.log('LoadReferenceChain', {
            params
        })
        // Step 1: Update instance.
        instance.reference = broadcast
    }).catch(error => {
        true && console.log('LoadReferenceChain', {
            error
        })
    })
}

export default {
    name: 'RenderPair',
    props: {
        broadcast: Object,
    },
    components: {
        RenderBroadcast,
    },
    data() {
        return {
            reference: null,
        }
    },
    methods: {},
    created() {
        true && console.log('RenderChain')
        LoadReferenceChain({
            broadcast: this.broadcast
        }, this)
    },
    mounted() {},
}
