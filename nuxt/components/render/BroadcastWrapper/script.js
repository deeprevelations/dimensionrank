// import Intersect from 'vue-intersect'

import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import cookieTokens from '@/lib/cookieToken.js'
import dataInterface from '@/lib/dataInterface.js'

import RenderBroadcast from '@/components/render/RenderBroadcast/index.vue'

function LoadBroadcast(params, instance) {
    true && console.log('LoadBroadcast', {
        params
    })
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    const options = {
        cookieToken
    }
    dataInterface.RetrieveBroadcast({
        broadcastId: params.broadcastId,
    }).then(broadcast => {
        true && console.log('LoadBroadcast', {
            params
        })
        // Step 1: Update instance.
        instance.broadcast = broadcast
    }).catch(error => {
        true && console.log('LoadBroadcast', {
            error
        })
    })
}

export default {
    name: 'BroadcastWrapper',
    props: {
        broadcastId: String,
    },
    components: {
        //        Intersect,
        RenderBroadcast,
    },
    data() {
        return {
            broadcast: null
        }
    },
    methods: {},
    created() {
        true && console.log('BroadcastWrapper', {})
        LoadBroadcast({
            broadcastId: this.broadcastId
        }, this)
    },
    computed: {},
    watch: {},
}
