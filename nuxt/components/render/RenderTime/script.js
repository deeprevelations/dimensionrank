export default {
    name: 'RenderTime',
    props: {
        timeNumber: Number,
    },
    data() {
        return {
            timeString: null,
        }
    },
    watch: {},
    methods: {},
    created() {
        const d = new Date(this.timeNumber)
				const ds = d.toDateString()
				const parts = ds.split(' ')
				const ts = parts[1] + ' ' + parts[2]
				const timePart = d.toTimeString().slice(0, 5)
				this.timeString = ts + ' ' + timePart
    },
    computed: {},
}
