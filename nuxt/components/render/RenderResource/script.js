import cookies from '@/lib/cookies.js'
import logging from '@/lib/logging.js'
import network from '@/lib/network.js'

export default {
    name: 'RenderResource',
    props: {
        resource: Object,
    },
    data() {
        return {}
    },
    created() {
        true && console.log('RenderResource created')
    },
    methods: {},
    computed: {},
}
