import cookies from '@/lib/cookies.js'
import objects from '@/lib/objects.js'
import network from '@/lib/network.js'

export default {
    name: 'RenderAttachment',
    props: {
        picture1: String,
        picture0: String,
        rating: Number,
    },
    components: {},
    data() {
        return {
            activePicture: null,
        }
    },
    methods: {},
    created() {
        true && console.log('RenderAttachment', {})

        if (this.rating == 1) {
            this.activePicture = this.picture1
        } else if (this.rating == 0) {
            this.activePicture = this.picture0
        }

    },
    computed: {},
    mounted() {},
}
