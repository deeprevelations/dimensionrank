import cookies from '@/lib/cookies.js'
import logging from '@/lib/logging.js'
import network from '@/lib/network.js'

function DestroyCookietoken(instance) {
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    const options = {
        cookieToken
    }
    true && console.log('DestroyCookietoken', {
        options
    })
    network.NodeCall('/cookieToken/delete', options)
        .then(result => {
            true && console.log('DestroyCookietoken', {
                result
            })
            if (result.data.success) {
                instance.$router.push('/')
            } else {
                // instance.$router.push('/messages/error')
                true && console.log('DestroyCookietoken', 'unexpected-error')
            }
        }, error => {
            true && console.log('DestroyCookietoken', {
                error
            })
        })
}

export default {
    name: 'LogoutPage',
    data() {
        return {}
    },
    methods: {},
    created() {
        true && console.log('cookieToken_destroy')
        DestroyCookietoken(this)
    }
}

