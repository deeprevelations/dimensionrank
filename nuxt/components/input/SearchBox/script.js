import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import cookieTokens from '@/lib/cookieToken.js'

export default {
    name: 'SearchBox',
    data() {
        return {
            searchTerm: '',
        }
    },
    methods: {
        SubmitButton() {
					console.log('SearchBox', {searchTerm: this.searchTerm})
            this.$emit('SearchEnter', this.searchTerm)
        },
    },
    created() {
        true && console.log('SearchBox')
    },
}
