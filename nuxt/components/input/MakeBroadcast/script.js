import cookies from '@/lib/cookies.js'
import cookieTokens from '@/lib/cookieToken.js'
import network from '@/lib/network.js'
import render from '@/lib/render/broadcast.js'
import Vue from 'vue'
import getUrls from 'get-urls'

import RenderResource from '@/components/render/RenderResource/index.vue'

// Returns false if OK, or else an error string.
function CheckTextareaProblem(textarea) {
    const trimmed = textarea.trim()
    if (!trimmed) {
        true && console.log('CheckTextareaProblem', {
            trimmed
        })
        return 'Revelation cannot be empty.'
    }
    return false
}

// Returns false if OK, or else an error string.
function CheckPossibleInputProblem(instance) {
    const broadcastProblem = CheckTextareaProblem(instance.textarea)
    true && console.log('CheckPossibleInputProblem', {
        broadcastProblem
    })
    if (broadcastProblem) {
        return broadcastProblem
    }

    return false
}

function SendBroadcast(params, instance) {
    true && console.log('SendBroadcast', {
        params
    })
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    const options = {
        cookieToken,
        textarea: instance.textarea,
        reference: instance.reference,
    }
    true && console.log('SendBroadcast', {
        options
    })

    const possibleErrorMessage = CheckPossibleInputProblem(instance)
    true && console.log('SendBroadcast', {
        possibleErrorMessage
    })
    if (possibleErrorMessage) {
        // Note: error case.
        true && console.log('SendBroadcast', 'Inputs Failed')
        // Note: assignment.
        instance.errorMessage = possibleErrorMessage
    } else {
        // Note: success case.
        const route = instance.reference ? '/broadcast/derived' : '/broadcast/original'
        network.NodeCall(route, options)
            .then(result => {
                true && console.log('SendBroadcast', {
                    result
                })
                instance.$emit('SubmitSuccessEvent', result.data.broadcast)
            }).catch(error => {
                true && console.log('SendBroadcast', {
                    error
                })
            })
    }
}

function UpdateResource(params, instance) {
    true && console.log('UpdateResource', {
        params
    })
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    network.NodeCall('/resource/get', {
            cookieToken,
            url: params.url,
        })
        .then(result => {
            true && console.log('/resource/get', {
                result
            })
            if (result.data.success) {
                instance.resource = result.data.resource
            } else {
                true && console.log('/resource/get', 'failure')
            }
        }).catch(error => {
            true && console.log('/resource/get', error)
        })
}

export default {
    name: 'MakeBroadcast',
    props: {
        reference: String,
    },
    components: {
        RenderResource,
    },
    data() {
        return {
            textarea: '',
            resource: null,
            channelDescriptor: {
                channelType: 'user'
            },
            errorMessage: null,
        }
    },
    methods: {
        CheckNewUrls() {
            true && console.log('CheckNewUrls')
            const urls = getUrls(this.textarea)
            for (const url of urls) {
                UpdateResource({
                    url
                }, this)
                break
            }
        },
        OnPaste() {
            this.CheckNewUrls()
        },
        OnInput(e) {
            if (e.data == ' ' || e.data == null) {
                this.CheckNewUrls()
            } else {
                // pass
            }
        },
        HandleSend() {
            SendBroadcast({}, this)
        },
        HandleCancel() {
            // Note: re-assignment.
            this.textarea = ''
        },
    },
    computed: {
        ShowError() {
            return this.errorMessage != null
        },
        MakeBroadcastTitle() {
            if (this.reference) {
                return 'Share Response'
            } else {
                return 'Share Revelation'
            }
        },
        ShareCaption() {
            return 'Publish'
        },
        TextareaClass() {
            if (this.reference) {
                return 'ReplyTextarea'
            } else {
                return 'BroadcastTextarea'
            }
        },
    },
    created() {
        true && console.log('broadcast_create')
        cookieTokens.ValidateCookieTokenOrRoute({}, this)
    }
}
