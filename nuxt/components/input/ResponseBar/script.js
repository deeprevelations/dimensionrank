import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import sendRating from '@/lib/sendRating.js'
import debug from '@/lib/debug.js'
import objects from '@/lib/objects.js'

import BinaryInput from '@/components/input/BinaryInput/index.vue'
import UnaryInput from '@/components/input/UnaryInput/index.vue'


function SendRating(params, instance) {
    true && console.log({
        params,
    })
    const options = {
        authenticatedUser: instance.$store.state.authenticatedUser,
        reference: instance.broadcast._id,
        variable: params.variable,
        value: params.value,
    }
    true && console.log({
        options
    })

    sendRating.SendRating(options).then(result => {
        true && console.log('SendRating', {
            result
        })
    }).catch(error => {
        console.error('SendRating', {
            error
        })
    })
}

export default {
    name: 'ResponseBar',
    components: {
        BinaryInput,
        UnaryInput,
    },
    props: {
        broadcast: Object,
        isFocused: Boolean,
    },
    data() {
        return {}
    },
    created() {
        true && console.log('ResponseBar')
    },
    methods: {
        HandleNewHot(newValue) {
            console.log('HandleNewHot', newValue)
            this.$emit('NewHot', newValue)

            SendRating({
                variable: 'hot',
                value: newValue
            }, this)
        },
        HandleNewAgree(newValue) {
            console.log('HandleNewAgree', newValue)
            this.$emit('NewAgree', newValue)

            SendRating({
                variable: 'agree',
                value: newValue
            }, this)
        },
        HandleReply(newValue) {
            console.log('HandleReply', newValue)
            this.$emit('Reply', newValue)
        },
    },
    computed: {},
    watch: {},
}
