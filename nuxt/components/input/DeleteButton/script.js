import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'

function DeleteBroadcast(params, instance) {
    true && console.log('DeleteBroadcast', {
        params
    })
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    const options = {
        cookieToken,
        broadcastId: params.broadcastId,
    }
    network.NodeCall('/broadcast/delete', options)
        .then(result => {
            true && console.log('DeleteBroadcast', {
                result
            })

            instance.$emit('DeleteClick')
        }).catch(error => {
            true && console.log('DeleteBroadcast', {
                error
            })
        })
}

export default {
    name: 'DeleteButton',
    props: {
        broadcast: Object,
    },
    data() {
        return {}
    },
    methods: {
        HandleClick() {
            const result = confirm('Are you sure you want to delete?')
            true && console.log('HandleClick', {
                result
            })

            if (result) {
                DeleteBroadcast({
                    broadcastId: this.broadcast._id,
                }, this)

            }
        }
    },
    computed: {
        ShowButton() {
            const lhs = this.broadcast.producerName
            const rhs = this.$store.getters.BareCookieUserName
            const same = lhs == rhs
            true && console.log('ShowButton', {
                lhs,
                rhs,
                same,
            })

            return same
        }
    },
    created() {
        true && console.log('DeleteButton')
    },
}
