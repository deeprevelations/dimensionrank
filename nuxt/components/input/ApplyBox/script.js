import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import cookieTokens from '@/lib/cookieToken.js'

const states = {
    kOriginalState: 0,
    kSendingState: 1,
    kSuccessState: 2,
    kErrorState: 3,
}

function SendApplication(instance) {
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    instance.currentState = instance.states.kSendingState
    network.NodeCall('/application/create', {
            email: instance.emailAddress,
        })
        .then(result => {
            true && console.log('/application/create', {
                result
            })
            if (result.data.success) {
                true && console.log('/application/create', 'success')
                instance.currentState = instance.states.kSuccessState
            } else {
                true && console.log('/application/create', 'failure')
                instance.currentState = instance.states.kErrorState
            }
        }).catch(error => {
            true && console.log('/application/create', {
                error
            })
        })
}


export default {
    name: 'ApplyBox',
    props: {},
    data() {
        return {
            states,
            currentState: 0,
            emailAddress: '',
            errorMessage: '',
        }
    },
    methods: {
        SubmitButton() {
            const emailIsValid = network.ValidateEmail(this.emailAddress)
            true && console.log('SubmitButton', {
                emailAddress: this.emailAddress,
                emailIsValid,
            })

            if (emailIsValid) {
                SendApplication(this)
            } else {
                this.errorMessage = "'" + this.emailAddress + "' is not a valid email address."
            }
        },
    },
    created() {
        true && console.log('ApplyBox')
        this.emailAddress = this.$store.state.emailAddress
    },
}
