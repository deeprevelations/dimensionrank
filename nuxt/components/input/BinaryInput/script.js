import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import debug from '@/lib/debug.js'
import objects from '@/lib/objects.js'

const kSelected = 'SelectImage Selected'
const kUnselected = 'SelectImage Unselected'

function SetClasses(instance) {
    if (instance.selectedValue == 1) {
        instance.class1 = kSelected
    } else {
        instance.class1 = kUnselected
    }

    if (instance.selectedValue == 0) {
        instance.class0 = kSelected
    } else {
        instance.class0 = kUnselected
    }
}

export default {
    name: 'BinaryInput',
    props: {
        name: String,
        picture1: String,
        picture0: String,
        initialValue: Number, // use -1 for not set
        forced: Boolean,
        broadcast: Object,
        index: Number,
    },
    data() {
        return {
            selectedValue: null,
            class1: kUnselected,
            class0: kUnselected,
        }
    },
    created() {
        if (this.initialValue >= 0) {
            this.selectedValue = this.initialValue
        }

        SetClasses(this)
    },
    methods: {
        HandleSelect(selectedValue) {
            true && console.log('HandleSelect', {
                name: this.name,
                selectedValue,
            })

            if (this.selectedValue == selectedValue) {
                if (!this.forced) {
                    this.selectedValue = null
                    this.$emit('NewValue', -1)
                }
            } else {
                this.selectedValue = selectedValue
                this.$emit('NewValue', this.selectedValue)
            }

            SetClasses(this)

        },
    },
    computed: {
        ShowProbability() {
            return this.selectedValue != null && this.index != null
        },
        Probability() {
            if (this.selectedValue != null && this.index != null) {
                const key = 'prob' + this.index.toString()
                const raw_prob = this.broadcast[key]
                const prob = this.selectedValue == 1 ? raw_prob : 1 - raw_prob
                if (prob) {
                    const prob_str = (prob * 100).toString()
                    console.log({
                        prob,
                        key,
                        prob_str
                    })
                    const substr = prob_str.substring(0, 3)
                    if (substr[2] == '.') {
                        return substr.substring(0, 2) + '%'
                    } else {
                        return substr + '%'
                    }
                } else {
                    return 'tbd'
                }
            } else {
                return 'error'
            }
        },
    },
    watch: {},
}