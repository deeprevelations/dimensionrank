import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import debug from '@/lib/debug.js'
import objects from '@/lib/objects.js'

const kSelected = 'SelectImage Selected'
const kUnselected = 'SelectImage Unselected'

function SetClasses(instance) {
    if (instance.selectedValue == 1) {
        instance.class1 = kSelected
    } else {
        instance.class1 = kUnselected
    }
}

export default {
    name: 'UnaryInput',
    props: {
        name: String,
        picture1: String,
        initialValue: Number, // use 0 for not set
        fixed: Boolean,
    },
    data() {
        return {
            selectedValue: 0,
            class1: kUnselected,
        }
    },
    created() {
        if (this.initialValue > 0) {
            this.selectedValue = this.initialValue
        }

        SetClasses(this)
    },
    methods: {
        HandleSelect(selectedValue) {
            true && console.log('HandleSelect', {
                name: this.name,
                selectedValue,
            })

            if (!this.fixed) {
                if (this.selectedValue == selectedValue) {
                    this.selectedValue = -1
                } else {
                    this.selectedValue = selectedValue
                }

                this.$emit('NewValue', this.selectedValue)

                SetClasses(this)
            }
        },
    },
    computed: {},
    watch: {},
}
