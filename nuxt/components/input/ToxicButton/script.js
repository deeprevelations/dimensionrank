import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import sendRating from '@/lib/sendRating.js'
import debug from '@/lib/debug.js'
import objects from '@/lib/objects.js'

function SendRating(params, instance) {
    true && console.log('SendRating', {
        params,
    })
    const options = {
        authenticatedUser: params.authenticatedUser,
        reference: params.reference,
        variable: 'toxic',
        value: 1,
    }
    true && console.log('SendRating', {
        options
    })

    // Step 1: Update the local stash.
    sendRating.SendRating(options).then(result => {
        true && console.log('SendRating', {
            result
        })
        instance.$emit('ToxicClick')
    }).catch(error => {
        console.error('SendRating', {
            error
        })
    })
}

export default {
    name: 'ToxicButton',
    props: {
        broadcast: Object,
    },
    data() {
        return {}
    },
    methods: {
        HandleClick() {
            const result = confirm('Are you sure this post is so "toxic" and should be taken down?')
            true && console.log('HandleClick', {
                result
            })
            if (result) {
                SendRating({
                    authenticatedUser: this.$store.state.authenticatedUser,
                    reference: this.broadcast._id
                }, this)
            }
        }
    },
    computed: {
        ShowButton() {
            const lhs = this.broadcast.producerName
            const rhs = this.$store.getters.BareCookieUserName
            const different = lhs != rhs
            true && console.log('ShowButton', {
                lhs,
                rhs,
                different,
            })

            return different
        }
    },
    created() {
        true && console.log('ToxicButton')
    },
}
