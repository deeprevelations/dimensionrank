import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import cookieTokens from '@/lib/cookieToken.js'

function HandleSuccess(data, instance) {
    console.log('HandleSuccess', {
        data,
    })
		cookies.SetCookie(cookies.authentication_token_cookie, data.cookieToken, 30)
		instance.$router.push('/')
}

function HandleFailure(data, instance) {
    console.log('HandleFailure', {
        data,
        error: data.error,
    })
    if (data.error.error == 'EmailTaken') {
        console.log('EmailTaken')
        instance.emailTaken = instance.email
    } else if (data.error.error == 'UserNameTaken') {
        console.log('UserNameTaken')
        instance.userNameTaken = instance.userPart
    }
}

function SendLoginRequest(instance) {
    const options = {
        userPart: instance.userPart,
        password: instance.password,
    }
    console.log({
        options
    })

    network.NodeCall('/user/login', options)
        .then(result => {
            true && console.log('SendLoginRequest', {
                result
            })
            if (result.data.success) {
                HandleSuccess(result.data, instance)
            } else {
                HandleFailure(result.data, instance)
            }
        }).catch(error => {
            true && console.log('SendLoginRequest', {
                error
            })
        })
}

export default {
    name: 'LoginBox',
    data() {
        return {
            userPart: '',
            password: '',
        }
    },
    methods: {
        SubmitButton() {
            true && console.log('SubmitButton', {})
            SendLoginRequest(this)
        },
        GoToSignup() {
            true && console.log('GoToSignup', {})
            this.$router.push({
                path: '/user/create'
            })
        }
    },
    created() {
        true && console.log('LoginBox')
    },
}
