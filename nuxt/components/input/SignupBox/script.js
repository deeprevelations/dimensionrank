import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import cookieTokens from '@/lib/cookieToken.js'

function HandleSuccess(data, instance) {
    console.log('HandleSuccess', {
        data,
    })
    cookies.SetCookie(cookies.authentication_token_cookie, data.cookieToken, 30)
    instance.$router.push('/checkEmail')
}

function HandleFailure(data, instance) {
    console.log({
        data,
        error: data.error,
    })
    if (data.error.error == 'EmailTaken') {
        console.log('EmailTaken')
        instance.emailTaken = instance.email
    } else if (data.error.error == 'UserNameTaken') {
        console.log('UserNameTaken')
        instance.userNameTaken = instance.userName
    }
}

function SendSignupRequest(instance) {
    const options = {
        userName: instance.userName,
        email: instance.email,
        password: instance.password,
    }
    console.log({
        options
    })

    // Null out the errors.
    instance.emailTaken = null
    instance.userNameTaken = null

    network.NodeCall('/user/create', options)
        .then(result => {
            true && console.log('SendSignupRequest', {
                result
            })
            if (result.data.success) {
                HandleSuccess(result.data, instance)
            } else {
                HandleFailure(result.data, instance)
            }
        }).catch(error => {
            true && console.log('SendSignupRequest', {
                error
            })
        })
}

export default {
    name: 'SignupBox',
    data() {
        return {
            email: '',
            userName: '',
            password: '',
            emailTaken: null,
            userNameTaken: null,
            invalidEmail: null,
        }
    },
    methods: {
        SubmitButton() {
            const emailIsValid = network.ValidateEmail(this.email)
            true && console.log('SubmitButton', {
                email: this.email,
                emailIsValid,
            })

            if (emailIsValid) {
                this.invalidEmail = null
                SendSignupRequest(this)
            } else {
                this.invalidEmail = this.email
            }
        },
    },
    created() {
        true && console.log('SignupBox')
    },
}
