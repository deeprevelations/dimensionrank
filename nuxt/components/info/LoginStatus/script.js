import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'

function ValidateCookieTokenImpl(params, instance) {
    true && console.log('ValidateCookieTokenImpl', {
        params,
    })
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    const options = {
        cookieToken
    }
    network.NodeCall('/cookieToken/validate', options).then(result => {
        true && console.log('ValidateCookieTokenImpl', {
            result,
        })
				instance.ready = true
        if (result.data.success) {
            const authenticatedUser = result.data.authenticatedUser
            // cookies.SetCookie(cookies.authenticatedUser, JSON.stringify(authenticatedUser), 30)
            // output.resolve(authenticatedUser)
            console.log({
                authenticatedUser
            })
						instance.$store.commit('SetAuthenticatedUser', authenticatedUser)
						const recovered = instance.$store.state.authenticatedUser
            console.log({
                recovered
            })
        } else {
            true && console.error('ValidateCookieTokenImpl', 'wtf')
            //            output.reject({
            //                error: result.data.error,
            //                details: result.data.details,
            //            })
        }
    }).catch(error => {
        true && console.error('ValidateCookieTokenImpl', {
            error,
        })
        //        output.reject({
        //            error,
        //        })
    })
}

export default {
    name: 'ServerStatus',
    props: {},
    data() {
        return {
            ready: false,
            authenticatedUser: null,
        }
    },
    watch: {},
    methods: {},
    created() {
        ValidateCookieTokenImpl({}, this)
    },
    computed: {},
}
