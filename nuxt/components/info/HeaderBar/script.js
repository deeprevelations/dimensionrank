import cookies from '@/lib/cookies.js'
import logging from '@/lib/logging.js'
import network from '@/lib/network.js'

function ValidateCookieTokenImpl(params, instance) {
    true && console.log('ValidateCookieTokenImpl', {
        params,
    })
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    const options = {
        cookieToken
    }
    network.NodeCall('/cookieToken/validate', options).then(result => {
        true && console.log('ValidateCookieTokenImpl', {
            result,
        })
        instance.ready = true
        if (result.data.success) {
            const authenticatedUser = result.data.authenticatedUser
            // cookies.SetCookie(cookies.authenticatedUser, JSON.stringify(authenticatedUser), 30)
            // output.resolve(authenticatedUser)
            console.log({
                authenticatedUser
            })
            instance.$store.commit('SetAuthenticatedUser', authenticatedUser)
            const recovered = instance.$store.state.authenticatedUser
            console.log({
                recovered
            })
        } else {
            true && console.error('ValidateCookieTokenImpl', 'wtf')
            //            output.reject({
            //                error: result.data.error,
            //                details: result.data.details,
            //            })
        }
    }).catch(error => {
        true && console.error('ValidateCookieTokenImpl', {
            error,
        })
        //        output.reject({
        //            error,
        //        })
    })
}

export default {
    name: 'HeaderBar',
    props: {
        sectionName: String,
    },
    data() {
        return {}
    },
    methods: {
        MenuButton(currName) {
            true && console.log('MenuButton', {
                currName,
                sectionName: this.sectionName,
            })
            if (currName == this.sectionName) {
                return 'MenuButton MenuButtonSelected'
            } else {
                return 'MenuButton'
            }
        }
    },
    created() {
        ValidateCookieTokenImpl({}, this)
    },
    computed: {
        MenuButton_receive() {
            true && console.log('MenuButton_receive')
            return this.MenuButton('Receive')
        },
        MenuButton_share() {
            true && console.log('MenuButton_share')
            return this.MenuButton('Share')
        },
        MenuButton_notifications() {
            true && console.log('MenuButton_notifications')
            return this.MenuButton('Notifications')
        },
        MenuButton_user() {
            true && console.log('MenuButton_user')
            return this.MenuButton('User')
        },
    },
}