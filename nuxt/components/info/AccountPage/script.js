import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'

function ReadUser(params, instance) {
    true && console.log('ReadUser', {
        params
    })
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    const options = {
        cookieToken,
    }
    network.NodeCall('/user/readSelf', options)
        .then(result => {
            true && console.log('ReadUser', {
                result
            })
            instance.userView = result.data.userView
        }).catch(error => {
            true && console.log('ReadUser', {
                error
            })
        })
}


export default {
    name: 'AccountPage',
    props: {},
    data() {
        return {
            ready: false,
            authenticatedUser: null,
            userView: null,
        }
    },
    watch: {},
    methods: {
        GoToHistory() {
            this.$router.push('/user/' + this.userView.userName)
        },
    },
    created() {
        ReadUser({}, this)
    },
    computed: {},
}
