import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'

//        <img v-bind:src="/menu-icons/black-receive.png" class="MenuIcon" title="Receive" />
//        <img v-bind:src="/menu-icons/black-receive.png" class="MenuIcon" title="Link" />
//        <img v-bind:src="/menu-icons/black-share.png" class="MenuIcon" title="Share" />
//        <img v-bind:src="/menu-icons/black-alerts.png" class="MenuIcon" title="Notifications" />
//        <img v-bind:src="/menu-icons/black-user.png" class="MenuIcon" title="User" />
export default {
    name: 'PageHeader',
    props: {
        sectionName: String,
				imagePath: String,
    },
    data() {
        return {}
    },
    watch: {},
    methods: {},
    created() {},
    computed: {
    },
}
