import cookieTokens from '@/lib/cookieToken.js'
import Vue from 'vue'

import PageHeader from '@/components/info/PageHeader/index.vue'
import HeaderBar from '@/components/info/HeaderBar/index.vue'
import MakeBroadcast from '@/components/input/MakeBroadcast/index.vue'

export default {
    name: 'broadcast_create',
    components: {
        HeaderBar,
        PageHeader,
        MakeBroadcast,
    },
    data() {
        return {}
    },
    methods: {
        HandleSubmitSuccessEvent(broadcast) {
            true && console.log('HandleSubmitSuccessEvent')
            this.$router.push('/link/' + broadcast._id)
        },
    },
    computed: {},
    created() {
        true && console.log('broadcast_create')
        cookieTokens.ValidateCookieTokenOrRoute({}, this)
    }
}
