import LoginStatus from '@/components/info/LoginStatus/index.vue'
import HeaderBar from '@/components/info/HeaderBar/index.vue'
import SearchBox from '@/components/input/SearchBox/index.vue'
import QueryFeed from '@/components/render/QueryFeed/index.vue'
import Logo from '@/components/Logo.vue'

export default {
    name: 'index',
    components: {
        LoginStatus,
        SearchBox,
        HeaderBar,
        Logo,
        QueryFeed,
    },
    data() {
        return {
            queryFeedKey: '',
						searchTerm: '',
        }
    },
    computed: {
        QueryRequest() {
            return {
                objectType: 'broadcast',
                channelType: 'network',
                channelName: 'nil',
            }
        },
    },
    created() {},
    methods: {
        HandleSearchEnter(searchTerm) {
            console.log('pages', {
                searchTerm
            })
						this.searchTerm = searchTerm
						this.queryFeedKey = (new Date()).toString()
        },
    },
}
