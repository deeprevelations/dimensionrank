import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import cookieTokens from '@/lib/cookieToken.js'

import DataPolicy from '@/components/static/DataPolicy/index.vue'
import SplashHeader from '@/components/static/SplashHeader/index.vue'
import SignupBox from '@/components/input/SignupBox/index.vue'

export default {
    name: 'user_create',
    components: {
        SignupBox,
        SplashHeader,
        DataPolicy,
    },
    data() {
        return {}
    },
    methods: {
    },
    created() {
        true && console.log('/user/create')
    },
}
