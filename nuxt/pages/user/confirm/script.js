import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import cookieTokens from '@/lib/cookieToken.js'

function HandleSuccess(data, instance) {
	instance.$router.push('/login/start')
}

function HandleFailure(data, instance) {
//    console.log({
//        data,
//        error: data.error,
//    })
//    if (data.error.error == 'EmailTaken') {
//        console.log('EmailTaken')
//        instance.emailTaken = instance.email
//    } else if (data.error.error == 'UserNameTaken') {
//        console.log('UserNameTaken')
//        instance.userNameTaken = instance.userName
//    }
}

function SendValidationRequest(params, instance) {
    const options = {
        token: params.token,
    }
    console.log({
        options
    })

    network.NodeCall('/user/confirm', options)
        .then(result => {
            true && console.log('SendSignupRequest', {
                result
            })
						HandleSuccess(result.data, instance) 
        }).catch(error => {
            true && console.log('SendSignupRequest', {
                error
            })
						HandleFailure(null, instance)
        })
}

export default {
    name: 'user_confirm',
    components: {},
    data() {
        return {
            confirmedUser: null,
        }
    },
    methods: {},
    created() {
        const token = this.$route.params.token
        true && console.log('/user/confirm', {
            token
        })
        SendValidationRequest({
            token
        }, this)
    },
}
