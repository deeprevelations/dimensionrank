import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import stateHelper from '@/lib/state.js'

import cookieTokens from '@/lib/cookieToken.js'
import AccountPage from '@/components/info/AccountPage/index.vue'
import Logo from '@/components/Logo.vue'
import PageHeader from '@/components/info/PageHeader/index.vue'
import HeaderBar from '@/components/info/HeaderBar/index.vue'

export default {
    name: 'account_edit',
    data() {
        return {
            userView: null,
        }
    },
    components: {
        AccountPage,
        Logo,
				PageHeader,
				HeaderBar,
    },
    methods: {
        GoToHistory() {
            // stateHelper.EnsureState(this.$store)
            const userName = this.$store.getters.BareCookieUserName
            this.$router.push('/user/' + userName)
        },
    },
    created() {
        //        ReadUser({}, this)
        //        cookieTokens.ValidateCookieTokenOrRoute({}, this)
    },
    computed: {},
}
