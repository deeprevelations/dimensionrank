import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import feedLike from '@/lib/feedLike.js'
import cookieTokens from '@/lib/cookieToken.js'
import dataInterface from '@/lib/dataInterface.js'
import render from '@/lib/render/broadcast.js'

import PageHeader from '@/components/info/PageHeader/index.vue'
import HeaderBar from '@/components/info/HeaderBar/index.vue'
import QueryFeed from '@/components/render/QueryFeed/index.vue'

export default {
    name: 'user_splash',
    components: {
        HeaderBar,
        QueryFeed,
        PageHeader,
    },
    data() {
        return {}
    },
    methods: {},
    created() {
        true && console.log('/user/splash', {
            params: this.$route.params
        })
        cookieTokens.ValidateCookieTokenOrRoute({}, this)
    },
    computed: {
        QueryRequest() {
            return {
                objectType: 'broadcast',
                channelType: 'user',
                channelName: this.$route.params.channelName,
            }
        },
        UserName() {
            return '@' + this.$route.params.channelName
        },
    },
    mounted() {},
}