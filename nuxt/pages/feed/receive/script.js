import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import feedLike from '@/lib/feedLike.js'
import cookieTokens from '@/lib/cookieToken.js'
import dataInterface from '@/lib/dataInterface.js'
import render from '@/lib/render/broadcast.js'

import PageHeader from '@/components/info/PageHeader/index.vue'
import HeaderBar from '@/components/info/HeaderBar/index.vue'
import SearchBox from '@/components/input/SearchBox/index.vue'
import QueryFeed from '@/components/render/QueryFeed/index.vue'

export default {
    name: 'receive_splash',
    components: {
        HeaderBar,
        QueryFeed,
        SearchBox,
        PageHeader,
    },
    data() {
        return {
            broadcasts: [],
            searchTerm: '',
            queryFeedKey: '',
        }
    },
    methods: {
        HandleSearchEnter(searchTerm) {
            this.searchTerm = searchTerm
            this.queryFeedKey = (new Date()).toString()
        },
    },
    created() {
        true && console.log('receive_splash', {
            params: this.$route.params
        })
        cookieTokens.ValidateCookieTokenOrRoute({}, this)
    },
    computed: {
        QueryRequest() {
            return {
                objectType: 'broadcast',
                channelType: 'network',
                channelName: 'nil',
            }
        },
    },
    mounted() {},
}
