import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import feedLike from '@/lib/feedLike.js'
import cookieTokens from '@/lib/cookieToken.js'
import dataInterface from '@/lib/dataInterface.js'
import render from '@/lib/render/broadcast.js'

import AlertsRoll from '@/components/render/AlertsRoll/index.vue'

import PageHeader from '@/components/info/PageHeader/index.vue'
import HeaderBar from '@/components/info/HeaderBar/index.vue'
import Logo from '@/components/Logo.vue'

export default {
    name: 'alerts',
    components: {
        AlertsRoll,
				PageHeader,
				HeaderBar,
				Logo,
    },
    data() {
        return {
        }
    },
    methods: {},
    created() {
    },
    mounted() {},
}
