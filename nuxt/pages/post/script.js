import PostPage from '@/components/page/PostPage/index.vue'
import HeaderBar from '@/components/info/HeaderBar/index.vue'
import Logo from '@/components/Logo.vue'
import MakeBroadcast from '@/components/input/MakeBroadcast/index.vue'

export default {
    name: 'post_page',
    components: {
        PostPage,
        HeaderBar,
        Logo,
        MakeBroadcast,
    },
    data() {
        return {}
    },
    methods: {
			HandleSubmitSuccessEvent(broadcast) {
				console.log('SubmitSuccessEvent', {broadcast})
				this.$router.push('/link/' + broadcast._id)
			}
		},
    created() {},
    computed: {},
}
