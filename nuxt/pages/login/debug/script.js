import cookies from '@/lib/cookies.js'
import logging from '@/lib/logging.js'
import network from '@/lib/network.js'
import cookieTokens from '@/lib/cookieToken.js'

function CreateCookieToken(params, instance) {
    true && console.log('CreateCookieToken', {
        params
    })

    sessionStorage.clear()
    localStorage.clear()

    cookies.SetCookie(cookies.authentication_token_cookie, params.cookieToken, 30)
    cookieTokens.ValidateCookieToken().then(result => {
        true && console.log({
            result
        })
    }).catch(error => {
        true && console.log({
            error
        })
    })

}

export default {
    name: 'cookieToken_debug',
    data() {
        return {
            message: '',
            cookieToken: '',
        }
    },
    methods: {
        HandleSubmit() {
            CreateCookieToken({
                cookieToken: this.cookieToken
            }, this)
        }
    },
    created() {}
}
