import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import cookieTokens from '@/lib/cookieToken.js'

import DataPolicy from '@/components/static/DataPolicy/index.vue'
import SplashHeader from '@/components/static/SplashHeader/index.vue'
import LoginBox from '@/components/input/LoginBox/index.vue'
import Logo from '@/components/Logo.vue'

export default {
    name: 'login_start',
    components: {
        Logo,
        LoginBox,
        SplashHeader,
        DataPolicy,
    },
    data() {
        return {}
    },
    methods: {},
    created() {
        true && console.log('/login/start')
    },
}
