import cookies from '@/lib/cookies.js'
import logging from '@/lib/logging.js'
import network from '@/lib/network.js'
import cookieTokens from '@/lib/cookieToken.js'

function CreateCookieToken(params, instance) {
    true && console.log('CreateCookieToken', {
        params
    })
    const options = {
        emailToken: params.emailToken,
    }
    network.NodeCall('/cookieToken/create', options)
        .then(result => {
            true && console.log('CreateCookieToken', {
                result
            })
            if (result.data.success) {
                // Note: When cookietoken is created, we reset the
                // 'authentication_token_cookie' and 'authenticatedUser'
                // cookies.
                cookies.SetCookie(cookies.authentication_token_cookie, result.data.cookieToken, 30)
                // cookies.SetCookie(cookies.authenticatedUser, JSON.stringify(result.data.authenticatedUser), 30)
                instance.$store.commit('SetAuthenticatedUser', result.data.authenticatedUser)

                // Note: This part is common with the "check and route"
                // version.
                cookieTokens.RouteByUserInfo({
                    authenticatedUser: result.data.authenticatedUser,
                    successUrl: network.SuccessUrl(),
                }, instance.$router)
            } else {
                true && console.log('CreateCookieToken', {
                    success: false
                })
                instance.showError = true
            }
        }).catch(error => {
            true && console.log('CreateCookieToken', {
                error
            })
            instance.showError = true
        })
}

export default {
    name: 'cookieToken_create',
    data() {
        return {
            showError: false,
        }
    },
    methods: {},
    created() {
        const emailToken = this.$route.params.emailToken
        true && console.log('cookieToken_create', {
            emailToken
        })
        if (emailToken) {
            CreateCookieToken({
                emailToken
            }, this)
        } else {
            true && console.log('CreateCookieToken', 'emailToken not found')
        }
    }
}
