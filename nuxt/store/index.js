import stateHelper from '@/lib/state.js'

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const state = () => ({
    loaded: false,
    emailAddress: '',
    authenticatedUser: null,
    rankingAlgorithm: 'new',
    currentBroadcastsKey: null,
    currentBroadcasts: [],
    factCheckBuffer: null,
})

export const mutations = {
    SetEmailAddress(state, rhs) {
        true && console.log('SetEmailAddress', {
            rhs
        })
        state.emailAddress = rhs
        // stateHelper.StoreState(state)
    },
    SetAuthenticatedUser(state, rhs) {
        true && console.log('SetAuthenticatedUser', {
            rhs
        })
        state.authenticatedUser = rhs
        // stateHelper.StoreState(state)
    },
    SetRankingAlgorithm(state, rhs) {
        true && console.log('SetRankingAlgorithm', {
            rhs
        })
        state.rankingAlgorithm = rhs
        // stateHelper.StoreState(state)
    },
    LoadStorage(state, rhs) {
        true && console.log('LoadStorage', {
            rhs
        })
        Object.assign(state, rhs)
    },
    // Note: This isnt part of LoadStorage in case there was no storage,
    // but we will still set to 'loaded' even if we fail. Maybe this should
    // not be split across two files.
    SetLoaded(state) {
        true && console.log('SetLoaded')
        state.loaded = true
        // stateHelper.StoreState(state)
    },
    StoreCurrentBroadcasts(state, params) {
        true && console.log('StoreCurrentBroadcasts', {
            params,
        })
        state.currentBroadcastsKey = params.broadcastsKey
        state.currentBroadcasts = params.broadcasts
        // stateHelper.StoreState(state)
    },
    CacheForFactCheck(state, broadcast) {
        true && console.log('CacheForFactCheck', {
            broadcast,
        })
        state.factCheckBuffer = broadcast
    },
}

export const getters = {
    CookieUserName(state) {
        const user = state.authenticatedUser
        true && console.log('CookieUserName', {
            user
        })
        // TODO(greg) Add a getter for the state.
        if (user) {
            return '@' + user.userName
        } else {
            return 'User'
        }
    },
    BareCookieUserName(state) {
        const user = state.authenticatedUser
        true && console.log('BareCookieUserName', {
            user
        })
        // TODO(greg) Add a getter for the state.
        if (user) {
            return user.userName
        } else {
            return null
        }
    },
}
