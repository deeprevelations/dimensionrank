const kStageStorageKey = 'kStageStorageKey'

// Note: 'key' is the name of the top-level variable. Recursion should be done
// outside.
//function StableGet(store, key) {
//    if (store.state.loaded) {
//        return store.state[key]
//    }
//
//    const storedVersion = localStorage.getItem(kStageStorageKey)
//    if (storedVersion) {
//        store.commit('LoadStorage', storedVersion)
//    }
//
//    return store.state[key]
//}

function EnsureState(store) {
    if (store.state.loaded) {
        return
    }

    const storedVersion = localStorage.getItem(kStageStorageKey)
    if (storedVersion) {
        const parsed = JSON.parse(storedVersion)
        store.commit('LoadStorage', parsed)
    }

    store.commit('SetLoaded')
}

function StoreState(state) {
    const json = JSON.stringify(state)
    localStorage.setItem(kStageStorageKey, json)
}

export default {
    //    StableGet,
    EnsureState,
    StoreState,
}
