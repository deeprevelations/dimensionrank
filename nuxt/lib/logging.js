function Log(caption, json) {
	true && console.log(caption + ': ' + JSON.stringify(json, null, 2))
}

export default {
	Log
}