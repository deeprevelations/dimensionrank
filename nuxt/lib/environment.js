// These functions abstract from the environment variables and how they are stored internally.
// TODO(greg) Figure out why .env file isn't working.

function rest_server_path() {
	return 'http://52.36.245.27:8090'
}

function image_serve_path() {
    return 'http://52.36.245.27:8070'
}

export default {
	rest_server_path, 
	image_serve_path
}
