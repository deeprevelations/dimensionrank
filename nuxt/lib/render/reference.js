
function RenderReference(referenceInfo) {
    var buffer = '<div class="reference-container">'
    if ('resource' in referenceInfo) {
        buffer += referenceInfo.resource
    }
    buffer += '</div>'
    return buffer
}

export default {
    RenderReference
}