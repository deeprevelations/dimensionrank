const ActionMapping = {
    broadcast: 0,
    rating: 1,
}

const VariableMapping = {
    hot: 0,
    agree: 1,
    toxic: 2,
}

const ValueMapping = {
    false: 0,
    true: 1,
}

function ReducedActionType(inputType) {
    if (typeof inputType == 'number') {
        return inputType
    } else if (typeof inputType == 'string') {
        return parseInt(inputType)
    } else {
        return ActionMapping[inputType]
    }
}

function ReducedVariableType(inputType) {
    if (typeof inputType == 'number') {
        return inputType
    } else if (typeof inputType == 'string') {
        return parseInt(inputType)
    } else {
        return VariableMapping[inputType]
    }
}

function ReducedValueType(inputType) {
    if (typeof inputType == 'number') {
        return inputType
    } else if (typeof inputType == 'string') {
        return parseInt(inputType)
    } else {
        return ValueMapping[inputType]
    }
}

module.exports = {
    ActionMapping,
    VariableMapping,
    ValueMapping,
    ReducedActionType,
    ReducedVariableType,
    ReducedValueType,
}
