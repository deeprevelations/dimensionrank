import cookies from '@/lib/cookies.js'
import debug from '@/lib/debug.js'
import network from '@/lib/network.js'
import stashCache from '@/lib/stashCache.js'
import localVoteMath from '@/lib/localVoteMath.js'
import objects from '@/lib/objects.js'

//function UpdateLocalStash(params) {
//    true && console.log({
//        params,
//    })
//    const broadcast = localVoteMath.UpdateBroadcastWithRating(params)
//    stashCache.StashLocal(broadcast)
//}

function SendRatingImpl(params, output) {
    true && console.log({
        params,
    })
    const options = {
        cookieToken: cookies.GetCookie(cookies.authentication_token_cookie),
        reference: params.reference,
        variable: params.variable,
        value: params.value,
    }
    true && console.log({
        options
    })

    //    // Step 1: Update the local stash.
    //    const userName = params.authenticatedUser.userName
    //    UpdateLocalStash({
    //        ...params,
    //        userName,
    //    })

    // Step 3: Update the judgment on the backend.
    network.NodeCall('/rating/create', options)
        .then(result => {
            true && console.log('SendRating', {
                result
            })
            if (result.data.success) {
                output.resolve(undefined)
            } else {
                const error = {
                    error: 'Unexpected'
                }
                console.error('SendRating', {
                    error
                })
                output.reject(error)
            }
        }).catch(error => {
            console.error('SendRating', {
                error
            })
            output.reject(error)
        })
}

function SendRating(params) {
    true && console.log('SendRating', {
        params
    })
    return new Promise(function(resolve, reject) {
        const error = debug.CheckArguments(params, ['authenticatedUser', 'reference', 'variable', 'value'])
        true && console.log('SendRating', {
            error
        })
        if (error) {
            console.error('SendRating', {
                error
            })
            reject(error)
        } else {
            SendRatingImpl(params, {
                resolve,
                reject,
            })
        }
    })
}

export default {
    SendRating,
}
