const ActuallyCheck = true

// Returns null on success.
function CheckArguments(params, requiredList) {
    if (!ActuallyCheck) {
        return null
    }
    for (const argument of requiredList) {
        const missing = params[argument] == undefined
        if (missing) {
				console.error({params})
            return {
                error: 'MissingArguments',
                observed: Object.keys(params),
								required: requiredList,
								missing: argument,
								trace: (new Error()).stack,
            }
        }
    }
    return null
}

function GetFileName(trace) {
    const line = trace.split('\n')[2]
    var fileName = null
    if (line) {
        fileName = line.trim() // line.split('(')[1].split(')')[0]
    }
    return fileName
}

const logRegex = /./
const doLogging = false

function logCommon() {}

function log(...argList) {
		const fileName = GetFileName((new Error()).stack)
    if (doLogging && fileName && fileName.match(logRegex)) {
        true && console.log(fileName, ...argList)
    }
}

function error(...argList) {
    const fileName = GetFileName((new Error()).stack)
    console.error(fileName, ...argList)
}

function warn(...argList) {
    const fileName = GetFileName((new Error()).stack)
    console.error(fileName, ...argList)
}

module.exports = {
    CheckArguments,
    log,
    warn,
    error,
}
