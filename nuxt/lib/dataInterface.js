import network from './network.js'
import cookies from './cookies.js'
import debug from './debug.js'
import stashCache from './stashCache.js'

const UseCache = false

// Creates a 'client-side storage key' for a broadcast.
// Returns 'String'.
function BroadcastKey(broadcastId) {
    true && console.log({
        broadcastId
    })
    return "broadcast:" + broadcastId
}

function LoadFresh(params, output) {
    true && console.log({
        params
    })
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    const options = {
        cookieToken,
        broadcastId: params.broadcastId,
    }
    true && console.log({
        options
    })
    return new Promise(function(resolve, reject) {
        network.NodeCall('/broadcast/read', options)
            .then(result => {
                true && console.log('RetrieveBroadcast', {
                    result
                })
                if (result.data.success) {
                    const broadcast = result.data.broadcast
                    true && console.log('RetrieveBroadcast', {
                        broadcast
                    })

                    output.resolve(broadcast)
                } else {
                    output.reject({
                        error: 'RemoteFetchFailed',
                        details: ['broadcastId', broadcastId],
                    })
                }
            }).catch(error => {
                output.reject(error)
            })
    })
}

function RetrieveBroadcastImpl(params, output) {
    true && console.log({
        params
    })
    const cached = stashCache.CheckCache(params.broadcastId)
    true && console.log({
        cached
    })
    if (cached) {
        output.resolve(cached)
    } else {
        LoadFresh(params, output)
    }
}

function RetrieveBroadcast(params) {
    true && console.log({
        params
    })

    return new Promise(function(resolve, reject) {
        const error = debug.CheckArguments(params, ['broadcastId'])
        if (error) {
            reject(error)
        } else {
            RetrieveBroadcastImpl(params, {
                resolve,
                reject
            })
        }
    })
}

export default {
    RetrieveBroadcast,
}
