import debug from '@/lib/debug.js'
import objects from '@/lib/objects.js'

function FilterRatings(original, userName) {
    debug.log({
        original,
        userName,
    })
    var buffer = []
    for (const judgment of original) {
        if (judgment.userName == userName) {
            true && console.log({
                judgment
            })
        } else {
            buffer.push(judgment)
        }
    }
    return buffer
}

function LocalUpdateRatings(original, userVote, userName) {
    debug.log({
        original,
        userVote,
				userName,
    })

    // Filter the existing original.
    var result = FilterRatings(original, userName)

    // Create the new judgment.
    const tuple = {
        userName: userName,
        judgment: objects.ReducedRatingType(userVote.judgment),
        mode: objects.ReducedModeType(userVote.mode),
    }
    result.push(tuple)
    return result
}

export default {
    LocalUpdateRatings,
}
