import cookies from '@/lib/cookies.js'
import debug from '@/lib/debug.js'
import network from '@/lib/network.js'
import stateHelper from '@/lib/state.js'
import batchFeedManager from '@/lib/batchFeedManager.js'

//function CheckIfCached(params, instance) {
//    const lhs = params.currentBroadcastsKey
//    const rhs = instance.$store.state.currentBroadcastsKey
//    true && console.log('CheckIfCached', {
//        params,
//        lhs,
//        rhs,
//    })
//    if (lhs == rhs) {
//        return instance.$store.state.currentBroadcasts
//    } else {
//        return null
//    }
//}
//
function ReceiveQueryBroadcastListImpl(params, instance) {
    true && console.log('ReceiveQueryBroadcastListImpl', {
        params
    })
    //		stateHelper.EnsureState(instance.$store)
    //    const cached = CheckIfCached(params, instance)
    //    true && console.log('ReceiveQueryBroadcastListImpl', {
    //        cached
    //    })
    const cached = false
    if (cached) {
        // Note: Skipping this.
        instance.broadcasts = cached
    } else {
        batchFeedManager.ReceiveNextFromQuery(params).then(broadcasts => {
            true && console.log('ReceiveQueryBroadcastListImpl', {
                broadcasts
            })
            instance.$store.commit('StoreCurrentBroadcasts', {
                broadcastsKey: params.currentBroadcastsKey,
                broadcasts,
            })
            const size1 = instance.broadcasts.length
            const size2 = broadcasts.length
            instance.broadcasts = instance.broadcasts.concat(broadcasts)
            const size3 = instance.broadcasts.length
            true && console.log('ReceiveQueryBroadcastListImpl', {
                size1,
                size2,
                size3,
            })
        }).catch(error => {
            true && console.log('ReceiveQueryBroadcastListImpl', {
                error,
                stack: error.stack,
            })
        })
    }
}

function ReceiveQueryBroadcastList(params, instance) {
    const error = debug.CheckArguments(params, ['queryRequest', 'rankingAlgorithm', 'currentBroadcastsKey'])
    if (error) {
        console.error({
            error
        })
    } else {
        ReceiveQueryBroadcastListImpl(params, instance)
    }
}

function ReceiveQueryBroadcastImpl(params, instance) {
    true && console.log('ReceiveQueryBroadcastImpl', {
        params
    })
    batchFeedManager.ReceiveNextFromQuery(params).then(broadcast => {
        true && console.log('ReceiveQueryBroadcastImpl', {
            broadcast
        })
        instance.broadcast = broadcast
    }).catch(error => {
        true && console.log('ReceiveQueryBroadcastImpl', {
            error,
            stack: error.stack,
        })
    })
}

function ReceiveQueryBroadcast(params, instance) {
    const error = debug.CheckArguments(params, ['queryRequest', 'rankingAlgorithm'])
    if (error) {
        console.error({
            error
        })
    } else {
        ReceiveQueryBroadcastImpl(params, instance)
    }
}

export default {
    ReceiveQueryBroadcast,
    ReceiveQueryBroadcastList,
}
