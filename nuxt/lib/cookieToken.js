import cookies from '@/lib/cookies.js'
import network from '@/lib/network.js'
import debug from '@/lib/debug.js'

// The authenticatedUser is logged in. Typically, they have all of their info. In the
// initial case, they need to sign up.
function RouteByUserInfo(params, router) {
    true && console.log('RouteByUserInfo', {
        params
    })
		// Optional: 'successUrl'
    const error = debug.CheckArguments(params, ['authenticatedUser'])
    if (error) {
        true && console.log('ValidateCookieTokenOrRoute', {
            error
        })
    } else {
        if (!params.authenticatedUser.userName) {
            // The user is logged in but doesn't have a userName.
            true && console.log('RouteByUserInfo', 'redirecting to /user/userName/initial')
            router.push('/account/selectName')
        } else {
            // Success! The user is logged in and has all their info.
            true && console.log('RouteByUserInfo', 'redirecting to successUrl')
            if (params.successUrl) {
                router.push(params.successUrl)
            }
        }
    }
}

// Note: Returns an underlying axios Promise.
function ValidateCookieTokenImpl(params, output) {
    true && console.log('ValidateCookieTokenImpl', {
        params,
    })
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    const options = {
        cookieToken
    }
    network.NodeCall('/cookieToken/validate', options).then(result => {
        true && console.log('ValidateCookieTokenImpl', {
            result,
        })
        if (result.data.success) {
            const authenticatedUser = result.data.authenticatedUser
            // cookies.SetCookie(cookies.authenticatedUser, JSON.stringify(authenticatedUser), 30)
            output.resolve(authenticatedUser)
        } else {
            output.reject({
                error: result.data.error,
                details: result.data.details,
            })
        }
    }).catch(error => {
        true && console.log('ValidateCookieTokenImpl', {
            error,
        })
        output.reject({
            error,
        })
    })
}

function ValidateCookieToken() {
    return new Promise(function(resolve, reject) {
        ValidateCookieTokenImpl({}, {
            resolve,
            reject,
        })
    })
}

function ValidateCookieTokenOrRoute(params, instance) {
    // params contains 'successUrl', which can be null
    true && console.log('ValidateCookieTokenOrRoute', {
        params
    })
    const error = debug.CheckArguments(params, [], ['successUrl'])
    if (error) {
        true && console.log('ValidateCookieTokenOrRoute', {
            error
        })
    } else {
        ValidateCookieToken().then(authenticatedUser => {
            true && console.log('ValidateCookieTokenOrRoute', {
                authenticatedUser
            })
            instance.$store.commit('SetAuthenticatedUser', authenticatedUser)
            RouteByUserInfo({
                ...params,
                authenticatedUser,
            }, instance.$router)
        }).catch(error => {
            true && console.log('ValidateCookieTokenOrRoute', {
                error
            })
            instance.$router.push('/')
        })
    }
}

export default {
    ValidateCookieToken,
    RouteByUserInfo,
    ValidateCookieTokenOrRoute,
}
