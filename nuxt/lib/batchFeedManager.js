import cookies from '@/lib/cookies.js'
import debug from '@/lib/debug.js'
import network from '@/lib/network.js'
import stashCache from '@/lib/stashCache.js'

const kPageSize = 10

// The reson for this step is explained in the comments in the stachCache.js
// file.
function HitCacheIfNecessary(params, output) {
}

function RespondFromBatch(params, output) {
    true && console.log('RespondFromBatch', {
        params
    })
    if (!params.broadcasts || params.broadcasts.length < 1) {
        output.reject({
            error: 'ClientBatchEmpty',
        })
    } else {
        const broadcasts = params.broadcasts.slice(0, kPageSize)
        const tail = params.broadcasts.slice(kPageSize)
        true && console.log('RespondFromBatch', {
            queryKey: params.queryKey,
        })
        true && console.log('RespondFromBatch', {
            broadcasts,
        })
        true && console.log('RespondFromBatch', {
            tails: JSON.stringify(tail),
        })
        sessionStorage.setItem(params.queryKey, JSON.stringify(tail))
        output.resolve(broadcasts)
    }
}

function StoreBatchAndRespond(params, output) {
    true && console.log('StoreBatchAndRespond', {
        params
    })
    const listJson = JSON.stringify(params.broadcasts)
    true && console.log('StoreBatchAndRespond', {
        listJson
    })
    sessionStorage.setItem(params.queryKey, listJson)
    RespondFromBatch(params, output)
}

function FetchNewBatch(params, output) {
    true && console.log('FetchNewBatch', {
        params
    })
    const cookieToken = cookies.GetCookie(cookies.authentication_token_cookie)
    const options = {
        cookieToken,
        objectType: params.queryRequest.objectType,
        channelType: params.queryRequest.channelType,
        channelName: params.queryRequest.channelName,
        rankingAlgorithm: params.rankingAlgorithm,
    }
    network.NodeCall('/query/batch', options)
        .then(result => {
            true && console.log('FetchNewBatch', {
                result
            })
            if (result.data.success) {
                StoreBatchAndRespond({
                    ...params,
                    broadcasts: result.data.broadcasts,
                }, output)
            } else {
                output.reject({
                    error: result.data.error,
                })
            }
        }).catch(error => {
            true && console.log('FetchNewBatch', {
                error
            })
            output.reject(error)
        })
}

function QueryKey(queryRequest, rankingAlgorithm) {
    return 'batchFeedManager:' + rankingAlgorithm + '/' + queryRequest.channelType + '/' + queryRequest.channelName
}

function TryLocalStorage(params, output) {
    true && console.log('batchFeedManager.TryLocalStorage', {
        params
    })
    const queryKey = QueryKey(params.queryRequest, params.rankingAlgorithm)
    const listJson = sessionStorage.getItem(queryKey)
    true && console.log({
        queryKey,
        listJson,
    })
    const broadcasts = JSON.parse(listJson)
    if (broadcasts) {
        true && console.log('TryLocalStorage', {
            length: broadcasts.length
        })
    } else {
        true && console.log('TryLocalStorage', 'broadcast-null')
    }

    if (broadcasts && broadcasts.length > 0) {
        true && console.log('TryLocalStorage', 'Cache items remaining')
        RespondFromBatch({
            ...params,
            queryKey,
            broadcasts,
        }, output)
    } else {
        true && console.log('TryLocalStorage', 'No cache items remaining')
        FetchNewBatch({
            ...params,
            queryKey,
        }, output)
    }
    // output.resolve(listJson)
}

function ReceiveNextFromQuery(params) {
    true && console.log('ReceiveNextFromQuery', {
        params
    })
    return new Promise(function(resolve, reject) {
        const error = debug.CheckArguments(params, ['queryRequest', 'rankingAlgorithm'])
        if (error) {
            reject(error)
        } else {
            FetchNewBatch(params, {
                resolve,
                reject,
            })
//            TryLocalStorage(params, {
//                resolve,
//                reject,
//            })
        }
    })
}

export default {
    ReceiveNextFromQuery,
}
