import debug from '@/lib/debug.js'

// In a query with only one answer, if we reload immediately, and
// asynchronously from the last judgment, we can (empirically will) get a
// version of the broadcast back, that doesn't have the user's last vote.
// This is very disorenting. So, we put things into a "cache" right after
// updating.

function StashKey(broadcastId) {
    debug.log({
        broadcastId
    })
    return 'stashKey: ' + broadcastId
}

const kDelta = 1000

function CreateExpiryMs() {
    debug.log({})
    return Date.now() + kDelta
}

function IsExpired(expiryMs) {
    debug.log({
        expiryMs
    })
    if (!expiryMs) {
        return true
    } else {
        return Date.now() > expiryMs
    }

}

// Check the cache.
// If there's nothing, return null.
// If there's something, but it's expiration has expired, return null.
// Otherwise, return the result.
function CheckCache(broadcastId) {
    debug.log({
        broadcastId
    })
    // Step 1. See if it's in the cache.
    const stashKey = StashKey(broadcastId)
    const resultString = sessionStorage.getItem(stashKey)
    if (resultString == null) {
        return null
    }

    // Step 2: Check the expiry.
    const result = JSON.parse(resultString)
    const isExpired = IsExpired(result.expiryMs)
    if (isExpired) {
        return null
    }

    // Step 3: If we made it here, then return the result.
    return result.broadcast
}

// Synchronous.
function ReplaceOrReturn(broadcast) {
    debug.log({
        broadcast
    })
    const cached = CheckCache(broadcast._id)
    if (cached) {
        return cached
    } else {
        return broadcast
    }
}

function StashLocal(broadcast) {
    debug.log({
        broadcast
    })
    const stashKey = StashKey(broadcast._id)
    const value = {
        broadcast,
        expiryMs: CreateExpiryMs(),
    }
    sessionStorage.setItem(stashKey, JSON.stringify(value))
}

export default {
    CheckCache,
    ReplaceOrReturn,
    StashLocal,
}
