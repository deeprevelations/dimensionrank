function SetCookie(cookie_name, cookie_value, days) {
    var expires = ''
    if (days) {
        var date = new Date()
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000))
        expires = '; expires=' + date.toUTCString()
    }
    document.cookie = cookie_name + '=' + (cookie_value || '') + expires + '; path=/'
}

function GetCookie(cookie_name) {
    var nameEQ = cookie_name + '='
    var ca = document.cookie.split(';')
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i]
        while (c.charAt(0) == ' ') c = c.substring(1, c.length)
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length)
    }
    return null
}

const authentication_token_cookie = 'authenticationToken'
// const authenticatedUser = 'authenticatedUser'

//function GetUserName() {
//    const userCookie = GetCookie(authenticatedUser)
//		const userJson = JSON.parse(userCookie)
//    true && console.log('GetUserName', {
//        userCookie,
//        userJson,
//    })
//    return userJson.userName
//}

function EraseCookie(cookie_name) {
    document.cookie = cookie_name + '=; Max-Age=-99999999;'
}


function GetUserName(instance) {
    const user = instance.$store.state.authenticatedUser
    if (user) {
        return user.userName
    } else {
        return ''
    }
}

export default {
    SetCookie,
    GetCookie,
    EraseCookie,
    authentication_token_cookie,
    // authenticatedUser,
    GetUserName,
}
