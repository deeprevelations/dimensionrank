from bs4 import BeautifulSoup
import requests
from opengraph_parse import parse_page
import re
import redis

# lists

redis_client = redis.Redis(host='localhost', port=6379, db=0)

# function created
def scrape(site, site_name):

		# getting the request from url
		r = requests.get(site)

		# converting the text
		soup = BeautifulSoup(r.text,'html.parser')

		for i in soup.find_all('a'):
				# print('i', i)

				if not 'href' in i.attrs:
						continue
				href = i.attrs['href']
				# print ('href', href)

				full_url = None
				if href.startswith('/'):
						full_url = site + href
				elif site_name in href:
						full_url = href

				if not full_url:
					print ('none case')
					continue

				is_member = redis_client.sismember('urls_seen', full_url)

				if is_member:
					print ('old:', full_url)
					continue

				print ('new:', full_url)
				redis_client.rpush('new_urls', full_url)

# main function
if __name__ =='__main__':
		# website to be scrape
		site='https://breitbart.com'
		site_name = 'breitbart.com'

		site='https://nytimes.com'
		site_name = 'nytimes.com'

		site='https://coindesk.com'
		site_name = 'coindesk.com'

		# calling function
		scrape(site, site_name)
