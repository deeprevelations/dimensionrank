from bs4 import BeautifulSoup
import requests
from opengraph_parse import parse_page
from urllib.parse import urlparse
import re
import redis
import sys
import json
import uuid
from elasticsearch import Elasticsearch

from pymongo import MongoClient

search_client = Elasticsearch()

kRedisPopBatchSize = 100

redis_client = redis.Redis(host='localhost', port=6379, db=0)

def get_domain_from_url(url):
	return urlparse(url).netloc

def get_open_tags(url, pages_collection):
	domain = str(get_domain_from_url(url))
	print('domain', domain)

	parsed_og_tags = parse_page(url, ['og:title', 'og:description', 'og:url', 'og:image', 'og:article:author', 'og:video', ])
	parsed_og_tags['domain'] = domain
	try:
		print('parsed_og_tags', parsed_og_tags)
		mongo_result = pages_collection.insert_one(parsed_og_tags)
		print ('mongo_result', mongo_result.inserted_id)

		reference = str(mongo_result.inserted_id)
		print('reference', reference)
		contrived_example = {
			'producerId': domain,
			'consumerId': 'init_user',
			'reference': reference,
			'variable': 0,
			'value': 0,
		}
		ex_string = json.dumps(contrived_example)
		print('ex_string', ex_string)
		redis_client.rpush('train_queue', ex_string)

		# mongo_result = pages_collection.insert_one(parsed_og_tags)
		# print ('mongo_result', mongo_result.inserted_id)
		# elastic_result = search_client.index(index="pages", id=uuid.uuid4(), body=parsed_og_tags)
		# print ('elastic_result', elastic_result)
	except Exception as error:
		print ('error:', error)

# function created
def main(mongo_db):
	pages_collection = mongo_db['pages']

	while True:
		url = redis_client.lpop('new_urls')
		
		if not url:
			print('queue is done')
			break
	
		already_contained = redis_client.sismember('urls_seen', url)
		print ('already_contained:', already_contained)

		if not already_contained:
			print ('adding:', url)
			get_open_tags(url, pages_collection)
			redis_client.sadd('urls_seen', url)


# main function
if __name__ =='__main__':
	# calling function
	config_file = sys.argv[1]
	print( 'config_file', config_file)
	config_file = open(config_file, 'r')
	config_contents = config_file.read()
	print( 'config_contents', config_contents)
	config = json.loads(config_contents)
	print( 'config', config)

	mongo_uri = config['mongo']['uri']
	print( 'mongo_uri', mongo_uri)

	mongo_client = MongoClient(mongo_uri)
	mongo_db = mongo_client.development

	main(mongo_db)
