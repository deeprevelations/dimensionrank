import torch

floats = torch.FloatTensor([1, 2, 3])
for i in range(0, 3):
	t = floats[i].item()
	print 't', t


for i in range(0, 3):
	floats[i] = 5 + i

print 'floats', floats
	

if False:
	zeroes = torch.zeros([2, 4], dtype=torch.int32)
	print 'zeroes', zeroes

	a = zeroes [0][0]
	print 'a', a

	b = zeroes [1][3]
	print 'b', b

	ones = torch.ones([1], dtype=torch.int32)
	print 'ones', ones


	a = [1, 2, 3]
	b = torch.FloatTensor(a)
	print 'b', b
