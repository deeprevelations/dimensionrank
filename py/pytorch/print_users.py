import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import redis
import sys

NUM_GROUPS = int(sys.argv[1])
print 'NUM_GROUPS', NUM_GROUPS
USERS_PER_GROUP = int(sys.argv[2])
print 'USERS_PER_GROUP', USERS_PER_GROUP
NUM_USERS = NUM_GROUPS * USERS_PER_GROUP

redis_client = redis.Redis(host='localhost', port=6379, db=0)

groups = []
for group in range(0, NUM_GROUPS):
	groups.append({'x': [], 'y': []})

for i in range(0, NUM_USERS):
	key = 'user_embedding:' + str(i)
	weights = redis_client.get(key)

	parts = [float(s) for s in weights.split(' ')]
	print i, weights, parts

	group_id = i % NUM_GROUPS
	# x = groups[group_id]['x']
	groups[group_id]['x'].append(parts[0])
	groups[group_id]['y'].append(parts[1])


for group in groups:
	plt.plot(group['x'], group['y'], 'o')
	

plt.ylabel('some numbers')
plt.savefig('numbers.png')

