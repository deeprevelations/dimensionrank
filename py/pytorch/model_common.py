from pymongo import MongoClient
import json
import redis
import json
import time
import sys
import io
import logging

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

# have tried 4, 16
PRODUCER_EMBEDDING_DIM = 8
CONSUMER_EMBEDDING_DIM = 8
POST_SPECIFIC_EMBEDDING_DIM = 4
HIDDEN_LAYER_SIZE = 32

DO_LOAD_MODEL = True
MODEL_PATH = '/home/ubuntu/model.pt'


redis_client = redis.Redis(host='localhost', port=6379, db=0)

def DeserializeTensor(string):
	print('string', string)
	# parts = str(string).split(' ')
	parts = string.split(b' ')
	float_parts = torch.FloatTensor([[float(s) for s in parts]])
	return nn.Embedding.from_pretrained(float_parts)

def SerializeTensor(embeddings, embedding_dim):
	r = ''
	for i in range(0, embedding_dim):
		r += str(embeddings[0][i].item())
		if i < embedding_dim - 1:
			r += ' '
	return r

def LoadEmbeddingsFromRedis(embeddings_key, embedding_size): 
	embedding_string = redis_client.get(embeddings_key)
	print('embedding_string', embedding_string)
	if embedding_string:
		embedding = DeserializeTensor(embedding_string)
	else:
		embedding = generate_random_embedding(embedding_size)
		
	return embedding

def SaveEmbeddingsToRedis(embeddings_key, weights, embedding_dim): 
	s = SerializeTensor(weights, embedding_dim)
	redis_client.set(embeddings_key, s)

def producer_key(i):
	return 'producer:' + str(i)
def consumer_key(i):
	return 'consumer:' + str(i)
def post_specific_key(i):
	return 'post_specific:' + str(i)
def task_key(task_id, example):
	return 'task:' + str(task_id) + ':' + str(example['reference'])

def LoadModelEmbeddings(model, example):
	# Load embeddings.
	pretrained_dict = model.state_dict()

	ck = consumer_key(example['consumerId'])
	print ('ck', ck)
	consumer_embedding = LoadEmbeddingsFromRedis(
		ck, CONSUMER_EMBEDDING_DIM)
	print('consumer_embedding', consumer_embedding)
	pretrained_dict['consumer_embedding'] = consumer_embedding
	producer_embedding = LoadEmbeddingsFromRedis(
		producer_key(example['producerId']), PRODUCER_EMBEDDING_DIM)
	pretrained_dict['producer_embedding'] = producer_embedding
	effective_post_specific_key = post_specific_key(example['reference'])
	print ('effective_post_specific_key', effective_post_specific_key)
	post_specific_embedding = LoadEmbeddingsFromRedis(
		effective_post_specific_key, POST_SPECIFIC_EMBEDDING_DIM)
	print ('post_specific_embedding', post_specific_embedding)
	pretrained_dict['post_specific_embedding'] = post_specific_embedding

	model.load_state_dict(pretrained_dict)

def SaveModelEmbeddings(model, example):
	# Load embeddings.
	pretrained_dict = model.state_dict()
	# logging.info( 'pretrained_dict', pretrained_dict
	SaveEmbeddingsToRedis(
		producer_key(example['producerId']), pretrained_dict['producer_embedding.weight'], PRODUCER_EMBEDDING_DIM)
	SaveEmbeddingsToRedis(
		consumer_key(example['consumerId']), pretrained_dict['consumer_embedding.weight'], CONSUMER_EMBEDDING_DIM)
	SaveEmbeddingsToRedis(
		post_specific_key(example['reference']), pretrained_dict['post_specific_embedding.weight'], POST_SPECIFIC_EMBEDDING_DIM)

def SavePostTaskEmbedding(task_embedding, task_id, example):
	# Make keys.
	# Load task_embeddings.
	key = task_key(task_id, example)
	SaveEmbeddingsToRedis(key, task_embedding, CONSUMER_EMBEDDING_DIM)
	redis_client.rpush('task_updates', key)

def TrainOnExample(model, example):
	logging.info( 'TrainOnExamples')
	total_loss = 0.0
	total = 0
	# loss_fn = nn.NLLLoss()
	loss_fn = nn.BCELoss()

	consumer_id = example['consumerId']
	producer_id = example['producerId']
	LoadModelEmbeddings(model, example)
	optimizer = optim.SGD(model.parameters(), lr=0.1)
	optimizer.zero_grad()

	task_id = example['variable']
	logging.info( 'task_id', task_id)
	inputs = torch.LongTensor([task_id])
	logging.info( 'inputs', inputs)
	[nn_sigmoid, post_task_embedding] = model(inputs)

	value_int = example['value']
	logging.info( 'value_int', value_int)
	ints = [value_int]
		
	if value_int >= 0:
		label = torch.tensor(1.0 * value_int)
	else:
		# unset
		label = torch.tensor(0.5)
	print ('nn_sigmoid', nn_sigmoid)
	print ('label', label)
	loss = loss_fn(nn_sigmoid, label)
	loss.backward()
	optimizer.step()

	logging.info( 'consumer_id', consumer_id, 'producer_id', producer_id, 'ints', ints, 'label', label, 'nn_sigmoid', nn_sigmoid, 'loss', loss)

	SaveModelEmbeddings(model, example)
	SavePostTaskEmbedding(post_task_embedding, task_id, example)

	total_loss += loss.item()
	total += 1

	avg = total_loss / total
	logging.info( 'avg', avg)


def generate_random_embedding(embedding_dim):
	return nn.Embedding(1, embedding_dim)

class DotProductModel(nn.Module):
	def __init__(self):
		super(DotProductModel, self).__init__()
		# Left side: consumer side.
		self.consumer_embedding = generate_random_embedding(CONSUMER_EMBEDDING_DIM)

		# Right side: embed 1) consumer, 2) id for the post
		self.producer_embedding = generate_random_embedding(PRODUCER_EMBEDDING_DIM)
		self.post_specific_embedding = generate_random_embedding(POST_SPECIFIC_EMBEDDING_DIM)

		self.hidden_layer = nn.Linear(PRODUCER_EMBEDDING_DIM + POST_SPECIFIC_EMBEDDING_DIM, HIDDEN_LAYER_SIZE)

		# Use a list, whose length is the number of dimensions we are rating on.
		self.task_specific_embedding = [
			nn.Linear(HIDDEN_LAYER_SIZE, CONSUMER_EMBEDDING_DIM),
			nn.Linear(HIDDEN_LAYER_SIZE, CONSUMER_EMBEDDING_DIM),
		]


	def forward(self, inputs):
		logging.info( 'forward:inputs', inputs)
		# currently, inputs is the index of the second linear layer
		consumer_embedding = self.consumer_embedding(torch.zeros([1], dtype=torch.long))
		logging.info( 'consumer_embedding', consumer_embedding)
		producer_embedding = self.producer_embedding(torch.zeros([1], dtype=torch.long))
		logging.info( 'producer_embedding', producer_embedding)
		post_specific_embedding = self.post_specific_embedding(torch.zeros([1], dtype=torch.long))
		logging.info( 'post_specific_embedding', post_specific_embedding)

		embedding_layer = torch.cat((producer_embedding, post_specific_embedding), 1)
		logging.info( 'embedding_layer', embedding_layer)
		common_output = F.relu(self.hidden_layer(embedding_layer))
		logging.info( 'common_output', common_output)

		task_index = inputs[0].item()
		logging.info( 'task_index', task_index)
		task_output = self.task_specific_embedding[task_index](common_output)
		logging.info( 'task_output', task_output)

		dot_product = torch.dot(consumer_embedding[0], task_output[0])
		sigmoid = torch.sigmoid(dot_product)
		logging.info( 'sigmoid', sigmoid)
		return [sigmoid, task_output]


def LoadModel():
	# 1. Load the model.
	try:
		model = torch.load(MODEL_PATH)
	except:
		model = DotProductModel()
	return model

def SaveModel(model):
	torch.save(model, MODEL_PATH)
