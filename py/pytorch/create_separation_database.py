import psycopg2
import redis
import sys

print (sys.argv)

NUM_GROUPS = int(sys.argv[1])
print 'NUM_GROUPS', NUM_GROUPS
USERS_PER_GROUP = int(sys.argv[2])
print 'USERS_PER_GROUP', USERS_PER_GROUP

NUM_USERS = NUM_GROUPS * USERS_PER_GROUP

NUM_POSTS_PER_USER = 10

redis_client = redis.Redis(host='localhost', port=6379, db=0)
postgres_client = psycopg2.connect("dbname=test user=vagrant password=vagrant")
print 'postgres_client', postgres_client

creation_commands = [
	'drop table if exists favourites',
	'drop table if exists users',
	'drop table if exists statuses',
	'create table favourites(status_id int, account_id int);',
	'create table users(account_id int primary key, email varchar(100));', 
	'create table statuses(id int primary key, account_id int);',
]

for command in creation_commands:
	postgres_cursor = postgres_client.cursor()
	print 'command', command
	result = postgres_cursor.execute(command);
	print 'result', result

for i in range(0, NUM_USERS):
	command = "insert into users(account_id, email) values (%(id)d, '%(id)d');" % {
		'id': i
	}
	print 'command', command
	postgres_cursor.execute(command);

post_to_author = {}
total_posts = 0
for account_id in range(0, NUM_USERS):
	for j in range(0, NUM_POSTS_PER_USER):
		command = "insert into statuses(id, account_id) values (%(id)d, %(account_id)d);" % {
			'id': total_posts,
			'account_id': account_id,
		}
		post_to_author[total_posts] = account_id
		print 'command', command
		postgres_cursor.execute(command);

		total_posts += 1

for account_id in range(0, NUM_USERS):
	for status_id in range(0, total_posts):
		author_id = post_to_author[status_id]
		match = account_id % NUM_GROUPS == author_id % NUM_GROUPS
		if match:
			print 'account_id', account_id, 'author_id', author_id, 'match', match
			command = "insert into favourites(status_id, account_id) values (%(status_id)d, %(account_id)d);" % {
				'status_id': status_id,
				'account_id': account_id,
			}
			print 'command', command
			postgres_cursor.execute(command);

postgres_client.commit()
