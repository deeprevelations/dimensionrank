import psycopg2
import redis
import json
import time
import sys
import io

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

EMBEDDING_DIM = 4
middle_layer_size = 8
output_size = 2

TRAIN_WAIT_SECONDS = 10

NUM_USERS = 12

redis_client = redis.Redis(host='localhost', port=6379, db=0)
conn = psycopg2.connect("dbname=test user=vagrant password=vagrant")

""" Get an iterator over all users."""
def GetAllUsers():
	cur = conn.cursor()
	cur.execute("SELECT email, account_id FROM users")
	return cur

def ReadAllPosts(user_id):
	cur = conn.cursor()
	#print 'ReadAllUserLikes', 'user_id', user_id
	cur.execute("SELECT id, account_id FROM statuses")
	r = []
	for row in cur:
		example = {
			'id': row[0],
			'account_id': row[1],
		}
		#print 'example', example
		r.append(example)
	return r

def ReadAllUserLikes(user_id):
	cur = conn.cursor()
	#print 'ReadAllUserLikes', 'user_id', user_id
	cur.execute("SELECT status_id FROM favourites WHERE account_id = " + str(user_id))
	r = set()
	for row in cur:
		#print 'row', row
		r.add(row[0])
	return r

def CreateExamples(user_id, posts_shown, all_likes):
	#print 'DetermineWhichLiked', 'user_id', user_id
	examples = []
	for post in posts_shown:
		status_id = post['id']
		#print 'status_id', status_id
		contained = status_id in all_likes
		example = {
			'post' : post,
			'user_id': user_id,
			'label': contained,
		}
		#print 'contained', contained
		examples.append(example)
	return examples

def TrainOnExamples(model, examples):
	total_loss = 0.0
	total = 0
	# loss_fn = nn.BCELoss()
	loss_fn = nn.NLLLoss()
	for example in examples:
		optimizer = optim.SGD(model.parameters(), lr=0.1)
		consumer_id = example['user_id']
		producer_id = example['post']['account_id']
		optimizer.zero_grad()
		probs = model(torch.tensor([long(consumer_id), long(producer_id)], dtype=torch.long))
		#print 'probs', probs

		if example['label']:
			ints = [1]
		else:
			ints = [0]
			
		label = torch.LongTensor(ints)
		# print 'example', example, label
		loss = loss_fn(probs, label)
		print 'consumer_id', consumer_id, 'producer_id', producer_id, 'ints', ints, 'probs', probs, 'loss', loss
		#print 'loss', loss
		loss.backward()
		optimizer.step()

		total_loss += loss.item()
		total += 1

		avg = total_loss / total
		print 'avg', avg


class LikesModel(nn.Module):
	def __init__(self):
		super(LikesModel, self).__init__()
		self.user_embeddings = nn.Embedding(NUM_USERS, EMBEDDING_DIM)
		self.linear1 = nn.Linear(2 * EMBEDDING_DIM, middle_layer_size)
		self.linear2 = nn.Linear(middle_layer_size, output_size)

	def forward(self, inputs):
		# print 'inputs', inputs
		embeds = self.user_embeddings(inputs).view((1, -1))
		# embeds = torch.cat((consumer_embedding, producer_embedding), 1)
		# print 'embeds', embeds
		out = F.relu(self.linear1(embeds))
		out = self.linear2(out)
		log_probs = F.log_softmax(out, dim=1)
		# log_probs = torch.sigmoid(out)
		# print 'log_probs', log_probs
		return log_probs

""" The main funtion of the training program."""
def main():
	model = LikesModel()

	cursor = list(GetAllUsers())
	for i in range(0, NUM_TRAINING_ROUNDS):
		for user_tuple in cursor:
			print 'user_tuple', user_tuple
			user_id = user_tuple[1]
			posts_shown = ReadAllPosts(user_id)
			#print 'posts_shown', posts_shown
			all_user_likes = ReadAllUserLikes(user_id)
			#print 'all_user_likes', all_user_likes
			examples = CreateExamples(user_id, posts_shown, all_user_likes)
			# print 'examples', examples
			#print 'examples', examples
			TrainOnExamples(model, examples)


if __name__ == "__main__":
	main()

