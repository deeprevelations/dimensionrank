import psycopg2
import redis
import json
import time
import sys
import io

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

NUM_GROUPS = int(sys.argv[1])
print 'NUM_GROUPS', NUM_GROUPS
USERS_PER_GROUP = int(sys.argv[2])
print 'USERS_PER_GROUP', USERS_PER_GROUP
NUM_USERS = NUM_GROUPS * USERS_PER_GROUP

EMBEDDING_DIM = 2
middle_layer_size = 8
output_size = 2

redis_client = redis.Redis(host='localhost', port=6379, db=0)
conn = psycopg2.connect("dbname=test user=vagrant password=vagrant")

for i in range(0, 200):
	key = 'user_embedding:' + str(i)
	redis_client.delete(key)

""" Get an iterator over all users."""
def GetAllUsers():
	cur = conn.cursor()
	cur.execute("SELECT email, account_id FROM users")
	return cur

def ReadAllPosts(user_id):
	cur = conn.cursor()
	#print 'ReadAllUserLikes', 'user_id', user_id
	cur.execute("SELECT id, account_id FROM statuses")
	r = []
	for row in cur:
		example = {
			'id': row[0],
			'account_id': row[1],
		}
		#print 'example', example
		r.append(example)
	return r

def ReadAllUserLikes(user_id):
	cur = conn.cursor()
	#print 'ReadAllUserLikes', 'user_id', user_id
	cur.execute("SELECT status_id FROM favourites WHERE account_id = " + str(user_id))
	r = set()
	for row in cur:
		#print 'row', row
		r.add(row[0])
	return r

def CreateExamples(user_id, posts_shown, all_likes):
	#print 'DetermineWhichLiked', 'user_id', user_id
	examples = []
	for post in posts_shown:
		status_id = post['id']
		#print 'status_id', status_id
		contained = status_id in all_likes
		example = {
			'post' : post,
			'user_id': user_id,
			'label': contained,
		}
		#print 'contained', contained
		examples.append(example)
	return examples

def DeserializeTensor(string):
	#print 'string', string
	#print 'weights', weights
	parts = string.split(' ')
	float_parts = torch.FloatTensor([[float(s) for s in parts]])
	return nn.Embedding.from_pretrained(float_parts)
	#print 'parts', parts
	#for i in range(0, EMBEDDING_DIM):
	#	weights[0][i] = float(parts[i])

def SerializeTensor(embeddings):
	#print 'embeddings', embeddings
	r = ''
	for i in range(0, EMBEDDING_DIM):
		r += str(embeddings[0][i].item())
		if i < EMBEDDING_DIM - 1:
			r += ' '
	return r

def LoadEmbeddingsFromRedis(embeddings_key): 
	# print 'embeddings_key', embeddings_key
	string = redis_client.get(embeddings_key)
	#print 'weights', weights
	if string:
		#print 'result_weights', 'BEFORE ', embedding.weight, 'grad', embedding.weight.grad
		embedding = DeserializeTensor(string)
		# print 'type', type(embedding), embedding.__dict__
		#print 'loading', embeddings_key, embedding.weight
	else:
		embedding = generate_embedding()
		# print 'not_found', embeddings_key, embedding.weight
		
	return embedding

def SaveEmbeddingsToRedis(embeddings_key, weights): 
	#print 'embeddings_key', embeddings_key
	#print 'saving', embeddings_key, weights
	s = SerializeTensor(weights)
	redis_client.set(embeddings_key, s)

def LoadModelEmbeddings(model, example):
	# Make keys.
	prefix = 'user_embedding:'
	producer_key = prefix + str(example['post']['account_id'])
	#print 'producer_key', producer_key
	consumer_key = prefix + str(example['user_id'])
	#print 'consumer_key', consumer_key
	# Load embeddings.
	pretrained_dict = model.state_dict()

	consumer_embedding = LoadEmbeddingsFromRedis(consumer_key)
	pretrained_dict['consumer_embedding.weight'] = consumer_embedding.weight
	producer_embedding = LoadEmbeddingsFromRedis(producer_key)
	pretrained_dict['producer_embedding.weight'] = producer_embedding.weight

	model.load_state_dict(pretrained_dict)

def SaveModelEmbeddings(model, example):
	# Make keys.
	prefix = 'user_embedding:'
	producer_key = prefix + str(example['post']['account_id'])
	consumer_key = prefix + str(example['user_id'])
	# Load embeddings.
	pretrained_dict = model.state_dict()
	# print 'pretrained_dict', pretrained_dict
	SaveEmbeddingsToRedis(consumer_key, pretrained_dict['consumer_embedding.weight'])
	SaveEmbeddingsToRedis(producer_key, pretrained_dict['producer_embedding.weight'])


def TrainOnExamples(model, examples):
	total_loss = 0.0
	total = 0
	loss_fn = nn.NLLLoss()
	for example in examples:
		consumer_id = example['user_id']
		producer_id = example['post']['account_id']
		LoadModelEmbeddings(model, example)
		optimizer = optim.SGD(model.parameters(), lr=0.1)
		optimizer.zero_grad()
		probs = model(torch.zeros([1], dtype=torch.long))

		if example['label']:
			ints = [1]
		else:
			ints = [0]
			
		label = torch.LongTensor(ints)
		loss = loss_fn(probs, label)
		loss.backward()
		optimizer.step()

		# print 'consumer_id', consumer_id, 'producer_id', producer_id, 'ints', ints, 'probs', probs, 'loss', loss

		SaveModelEmbeddings(model, example)

		total_loss += loss.item()
		total += 1

		avg = total_loss / total
		if total % 50 == 0:
			print 'total', total, 'avg', avg

def generate_embedding():
	return nn.Embedding(1, EMBEDDING_DIM)


class LikesModel(nn.Module):
	def __init__(self):
		super(LikesModel, self).__init__()
		self.consumer_embedding = generate_embedding()
		self.producer_embedding = generate_embedding()
		self.linear1 = nn.Linear(2 * EMBEDDING_DIM, middle_layer_size)
		self.linear2 = nn.Linear(middle_layer_size, output_size)

	def forward(self, inputs):
		consumer_embedding = self.consumer_embedding(torch.zeros([1], dtype=torch.long))
		producer_embedding = self.producer_embedding(torch.zeros([1], dtype=torch.long))
		#print 'consumer_embedding', consumer_embedding
		#print 'producer_embedding', producer_embedding

		embeds = torch.cat((consumer_embedding, producer_embedding), 1)
		# #print 'embeds', embeds
		out = F.relu(self.linear1(embeds))
		out = self.linear2(out)
		log_probs = F.log_softmax(out, dim=1)
		return log_probs

def ShowWeights():
	for i in range(0, NUM_USERS):
		key = 'user_embedding:' + str(i)
		weights = redis_client.get(key)
		# print 'weights', i, weights


""" The main funtion of the training program."""
def main():
	model = LikesModel()

	cursor = list(GetAllUsers())
	for i in range(0, 3):
		for user_tuple in cursor:
			print 'user_tuple', user_tuple
			user_id = user_tuple[1]
			posts_shown = ReadAllPosts(user_id)
			#print 'posts_shown', posts_shown
			all_user_likes = ReadAllUserLikes(user_id)
			#print 'all_user_likes', all_user_likes
			examples = CreateExamples(user_id, posts_shown, all_user_likes)
			# print 'examples', examples
			#print 'examples', examples
			TrainOnExamples(model, examples)
			ShowWeights()


if __name__ == "__main__":
	main()

