from pymongo import MongoClient
import json
import redis
import json
import time
import sys
import io
import logging
import time

from model_common import DotProductModel
from model_common import TrainOnExample
from model_common import LoadModel
from model_common import SaveModel

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

kSleepLength = 0.1


redis_client = redis.Redis(host='localhost', port=6379, db=0)

""" The main funtion of the training program."""
def main():
	# 1. Load the model.
	model = LoadModel()

	# TODO Load from disk if it exists.

	# 2. Train on each example in the queue.
	while True:
		example_string = redis_client.lpop('train_queue')
		if not example_string:
			time.sleep(kSleepLength)
			continue

		print('example_string', example_string)
		example = json.loads(example_string)
		print('example', example)

		reference = example['reference']
		print ('reference', reference)

		if reference:
			# This is a rating.
			# 1. Train for this actual usuer.
			TrainOnExample(model, example)

			# 2. Train for the generic user.
			example['consumerId'] = 'generic'
			TrainOnExample(model, example)
		else:
			# No reference means this is initial.
			# For each variable, create an example in which 'generic' is neutral.
			for variable in range(0, 2):
				example['variable'] = variable
				example['value'] = -1
				example['reference'] = example['_id']
				example['consumerId'] = 'generic'
				TrainOnExample(model, example)


	# 3. TODO Save the model back.
	SaveModel(model)

if __name__ == "__main__":
	config_file = sys.argv[1]
	logging.info( 'config_file', config_file)
	config_file = open(config_file, 'r')
	config_contents = config_file.read()
	logging.info( 'config_contents', config_contents)
	config = json.loads(config_contents)
	logging.info( 'config', config)

	mongo_uri = config['mongo']['uri']
	logging.info( 'mongo_uri', mongo_uri)

	main()

