from pymongo import MongoClient
from bson.objectid import ObjectId
import json
import redis
import json
import time
import sys
import io
import logging
import time

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

from model_common import DotProductModel
from model_common import TrainOnExample
from model_common import LoadModel
from model_common import SaveModel

redis_client = redis.Redis(host='localhost', port=6379, db=0)
kSleepLength = 0.1

def SplitVectorString(s):
	parts = s.split(b' ')
	float_parts = torch.FloatTensor([[float(s) for s in parts]])
	return float_parts

""" The main funtion of the training program."""
def main(mongo_db):
	# 1. Load the model.
	model = LoadModel()
	client_embedding_string = redis_client.get('consumer:generic')

	while True:
		update_key = redis_client.lpop('task_updates')
		if not update_key:
			time.sleep(kSleepLength)
			continue

		embedding_string = redis_client.get(update_key)
		print ('embedding_string', embedding_string)

		key_parts = update_key.split(b':')
		print ('key_parts', key_parts)

		mongo_broadcasts = mongo_db['broadcasts']

		generic_client_embedding = SplitVectorString(client_embedding_string)
		print ('generic_client_embedding', generic_client_embedding)
		task_embedding = SplitVectorString(embedding_string)
		print ('task_embedding', task_embedding)

		# dot_product = torch.dot(generic_client_embedding[0], task_embedding[0])
		# print('dot_product', dot_product)
		# sigmoid = torch.sigmoid(dot_product)
		# print( 'sigmoid', sigmoid)

		id_string = key_parts[2].decode('utf-8')
		print ('id_string', id_string)
		mongo_query = { '_id': ObjectId(id_string) }
		task_part = key_parts[1].decode('utf-8')

		print('task_part', task_part)
		task_key = 'task_embedding' + task_part
		generic_key = 'generic' + task_part
		mongo_update = { '$set': {
			task_key: embedding_string.decode('utf-8'),
			# generic_key: sigmoid.item(),
		} }

		result = mongo_broadcasts.update_one(mongo_query, mongo_update)
		print ('result', result)
		print ('result.acknowledged', result.acknowledged)
		print ('result.matched_count', result.matched_count)
		print ('result.modified_count', result.modified_count)


if __name__ == '__main__':
	config_file = sys.argv[1]
	logging.info( 'config_file', config_file)
	config_file = open(config_file, 'r')
	config_contents = config_file.read()
	logging.info( 'config_contents', config_contents)
	config = json.loads(config_contents)
	logging.info( 'config', config)

	mongo_uri = config['mongo']['uri']
	logging.info( 'mongo_uri', mongo_uri)

	mongo_client = MongoClient(mongo_uri)
	mongo_db = mongo_client.development

	main(mongo_db)

