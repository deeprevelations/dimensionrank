set -e
set -v

NUM_GROUPS=$1
USERS_PER_GROUP=$2

python create_separation_database.py $NUM_GROUPS $USERS_PER_GROUP

python redis_separation.py $NUM_GROUPS $USERS_PER_GROUP

python print_users.py $NUM_GROUPS $USERS_PER_GROUP
