
from elasticsearch import Elasticsearch

from datetime import datetime
from elasticsearch import Elasticsearch
search_client = Elasticsearch()

import sys

search_term = sys.argv[1]

result = search_client.search(index="pages", body={"query": {
"simple_query_string" : {
        "query": search_term,
        "fields": ['og:title', "og:description"],
        "default_operator": "and"
    }}})

print("Got %d Hits:" % result['hits']['total']['value'])
for hit in result['hits']['hits']:
	print('hit', hit)


