ID=$1

if [ -z ${ID} ]; then
	echo not set;
fi
node tools/inviteUser.js --options ~/revelations-config/node.production.json --applicationId ${ID}
