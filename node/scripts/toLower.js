// Changes the email field in some collection to make the email lowercase.
const mongoose = require('mongoose')
const rfr = require('rfr')
const options = rfr('options')
const objects = rfr('data/objects')
const dataBases = rfr('data/bases')
const debug = rfr('util/debug')

async function Main(options) {
    console.log('Main', {
        options
    })

    const loadResult = await dataBases.InitFromFile(options.config, ['mongo'])
    console.log('Main', {
        loadResult
    })


    const dataInterface = objects[options.collection]

    const results = await dataInterface.find({})
    var emails = []
    for (const result of results) {
        const email = result.email
        for (var i = 0; i < email.length; ++i) {
            if (email[i] >= 'A' && email[i] <= 'Z') {
                emails.push(email)
                break
            }
        }
    }

    for (const email of emails) {
        const emailLower = email.toLowerCase()
        console.log({
            email,
            emailLower,
        })
        if (options.hot) {
            const updateResult = await dataInterface.updateOne({
                email
            }, {
                $set: {
                    email: emailLower
                }
            })
            console.log({
                updateResult
            })
        }
    }

		process.exit(0)
}

Main(options({
    'config': String,
    'collection': String,
    'hot': Boolean,
}))
