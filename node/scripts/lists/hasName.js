// Changes the email field in some collection to make the email lowercase.
const mongoose = require('mongoose')
const rfr = require('rfr')
const options = rfr('options')
const objects = rfr('data/objects')
const dataBases = rfr('data/bases')
const debug = rfr('util/debug')

async function Main(options) {
    console.log('Main', {
        options
    })

    const loadResult = await dataBases.InitFromFile(options.config, ['mongo'])
    console.log('Main', {
        loadResult
    })


    const results = await objects.User.find({})
    var emails = []
    for (const result of results) {
        console.log({
            result
        })
        if (result.userName) {
            emails.push(result.email)
        }
    }

    for (const email of emails) {
        console.log({
            email
        })
        if (options.hot) {
            const updateResult = await objects.EmailAuthorization.updateOne({
                email,
            }, {
                $push: {
                    lists: 'HasUserName'
                },
            })
        }
    }

    process.exit(0)
}

Main(options({
    'config': String,
    'hot': Boolean,
}))
