// UpdateHandler the public tally.
// This is only used if the vote is public.
// The non-trivial task that this file handles is making sure that counts are
// still correct if we update the count.
//
// All votes are counted. Currently, we always take a person's last vote
// towards the tally. 
// If votes are made in "private" mode, then they will be counted here, but
// should never be shown to the users.
// Private and public votes are listed in the same table.
//
// Database accesses:
// 1) Download the entire record. Process it locally, including to update the
// scores.
// 2) UpdateHandler the mongo table, according to whether this is an original vote, or
// an update.
const assert = require('assert')
const mongoose = require('mongoose')
const redis = require('redis')
const rfr = require('rfr')
const TimeUuid = require('cassandra-driver').types.TimeUuid

const dataRedis = rfr('data/redis')
const debug = rfr('util/debug')
const objects = rfr('data/objects')
const enums = rfr('data/enums')
const tables = rfr('data/tables')

function AddRatingEntry(params, output) {
    debug.log({
        params
    })
    const query = {
        _id: params.broadcast._id
    }
    const update = {
        $push: {
            ratings: {
                userName: params.producerTuple.userName,
                variable: enums.ReducedVariableType(params.rating.variable),
                value: enums.ReducedValueType(params.rating.value),
            }
        }
    }
    debug.log({
        query: JSON.stringify(query, null, 4),
        update: JSON.stringify(update, null, 4),
    })
    objects.Broadcast.updateOne(query, update).then(updateResult => {
        debug.log({
            params
        })
        output.resolve(params.broadcast)
    }).catch(error => {
        debug.log({
            error
        })
        output.reject(error)
    })
}

function LookupBroadcast(params, output) {
    debug.log({
        params
    })
    objects.Broadcast.findOne({
        _id: params.rating.reference
    }).then(broadcast => {
        if (!broadcast) {
            output.reject({
                error: 'DBObjectNotFound',
                id: params.broadcast._id,
            })
        } else {
            output.resolve(broadcast)

            AddRatingEntry({
                ...params,
                broadcast
            }, output)
        }
    }).catch(error => {
        output.reject(error)
    })
}

function ContinueIfRating(params, output) {
    debug.log({
        params
    })
    if (params.rating) {
        LookupBroadcast(params, output)
    } else if (params.broadcast) {
        // Note: The contract is to pass through a broadcast. This might not
        // make sense long-term.
        output.resolve(params.broadcast)
    } else {
        output.reject(new Error())
    }
}

// Note: This accepts a broadcast and passes it through. 
//
// Returns: A broadcast.
function UpdateHandler(params) {
    debug.log({
        params
    })
    return new Promise(function(resolve, reject) {
        const error = debug.CheckArguments(params, ['updateType', 'producerTuple'])
        if (error) {
            reject(error)
        } else {
            ContinueIfRating(params, {
                resolve,
                reject,
            })
        }
    })
}

module.exports = {
    UpdateHandler,
}
