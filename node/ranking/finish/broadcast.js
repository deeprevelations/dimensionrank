const mongoose = require('mongoose')

const rfr = require('rfr')
const debug = rfr('util/debug')
const objects = rfr('data/objects')
const redact = rfr('data/broadcast/redact')

function Redact(params, output) {
    debug.log({
        params
    })
    var broadcasts = []
    for (const rawBroadcast of params.orderedBroadcasts) {
        if (!rawBroadcast.deleted) {
            const redactedBroadcast = redact.PolishBroadcast(rawBroadcast, params.authenticatedUser)
            debug.log({
                redactedBroadcast
            })
            broadcasts.push(redactedBroadcast)
        }
    }
    output.resolve({
        broadcasts
    })
}

function Reorder(params, output) {
    debug.log({
        params,
        mongoIds: params.unfinishedResult.mongoIds,
    })
    if (!params.unfinishedResult.mongoIds) {
        // I.e., this is a query.
        const orderedBroadcasts = params.broadcasts
        Redact({
            ...params,
            orderedBroadcasts,
        }, output)
    } else {
        // Note: We use a map instead of sorting in case to allow arbitrary orders.
        // Note: We are intertwining sort, redact and return due to laziness.
        var idMap = {}
        for (const broadcast of params.broadcasts) {
            const id = broadcast._id.toString()
            debug.log({
                broadcast,
                id,
            })
            idMap[id] = broadcast
        }
        var broadcasts = []
        for (const id of params.unfinishedResult.mongoIds) {
            debug.log({
                id
            })
            const rawBroadcast = idMap[id]
            if (!rawBroadcast) {
                output.reject({
                    error: 'InvalidArgument',
                    details: ['id', id, 'idMap', Object.keys(idMap)],
                })
                return // Note: Returns early.
            } else {
                broadcasts.push(rawBroadcast)
            }
        }
        Redact({
            ...params,
            orderedBroadcasts: broadcasts,
        }, output)
    }
}

function LookupByIds(params, output) {
    debug.log({
        params
    })
    const mongoIds = params.unfinishedResult.mongoIds
    debug.log({
        mongoIds
    })
    objects.Broadcast.find({
        _id: {
            $in: mongoIds
        }
    }).then(broadcasts => {
        debug.log({
            broadcasts
        })
        Reorder({
            ...params,
            broadcasts,
        }, output)
    }).catch(error => {
        debug.error({
            error
        })
        output.reject(error)
    })
}

function LookupByQuery(params, output) {
    debug.log({
        params
    })
    const mongoQuery = params.unfinishedResult.mongoQuery
    debug.log({
        mongoQuery
    })

    // Note: Invert the query.
    const fn = mongoQuery(objects.Broadcast)
    debug.log({
        fn
    })
    fn.then(broadcasts => {
        debug.log({
            broadcasts
        })
        Reorder({
            ...params,
            broadcasts,
        }, output)
    }).catch(error => {
        debug.error({
            error
        })
        output.reject(error)
    })
}

function RouteResult(params, output) {
    if (params.unfinishedResult.mongoIds) {
        LookupByIds(params, output)
    } else if (params.unfinishedResult.mongoQuery) {
        LookupByQuery(params, output)
    } else {
        reject({
            error: 'InvalidArgument',
            details: ['unfinishedResult', Object.keys(unfinishedResult)],
        })
    }
}

function Finish(params) {
    debug.log({
        params
    })
    return new Promise(function(resolve, reject) {
        const error = debug.CheckArguments(params, ['unfinishedResult', 'authenticatedUser'])
        if (error) {
            reject(error)
        } else {
            RouteResult(params, {
                resolve,
                reject
            })
        }
    })
}

module.exports = {
    Finish,
}