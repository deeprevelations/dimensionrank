const mongoose = require('mongoose')

const rfr = require('rfr')
const debug = rfr('util/debug')
const tables = rfr('data/tables')
const redact = rfr('data/broadcast/redact')

function Finish(params) {
    return new Promise(function(resolve, reject) {
        const error = debug.CheckArguments(params, ['unfinishedResult', 'authenticatedUser'])
        if (error) {
            reject(error)
        } else {
            const innerError = debug.CheckArguments(params.unfinishedResult, ['notifications'])
            if (innerError) {
                reject(innerError)
            } else {
                const notifications = params.unfinishedResult.notifications
                resolve({
                    notifications
                })
            }
        }
    })
}

module.exports = {
    Finish,
}
