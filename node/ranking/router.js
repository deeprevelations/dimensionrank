const rfr = require('rfr')

const handlerRoutes = [
    // broadcast/hot
//    '/algo/hot/table/broadcast/comment',
//    '/algo/hot/table/broadcast/network',
//    '/algo/hot/table/broadcast/user',
//    '/algo/hot/table/broadcast/toxic',
    // broadcast/new
    '/algo/new/table/broadcast/comment',
    '/algo/new/table/broadcast/network',
    '/algo/new/table/broadcast/user',
    '/algo/new/table/broadcast/toxic',
    '/algo/new/table/broadcast/notification',
    // notification/new
//    '/algo/new/table/notification/user',
    // network/popular
//    '/algo/popular/table/network/overall',
]

function CreateHandlerMap() {
    var result = {}
    for (const route of handlerRoutes) {
        result[route] = rfr('/ranking' + route)
    }
    return result
}

// Map from algorithm name to algorithms.
const handlerMap = CreateHandlerMap()

const finisherRoutes = [
    'broadcast',
    'notification',
]

function CreateFinisherMap() {
    var result = {}
    for (const route of finisherRoutes) {
        result[route] = rfr('/ranking/finish/' + route)
    }
    return result
}

const finisherMap = CreateFinisherMap()

module.exports = {
    handlerMap,
    finisherMap,
}
