const mongoose = require('mongoose')

const rfr = require('rfr')
const debug = rfr('util/debug')
const objects = rfr('data/objects')
const tables = rfr('data/tables')

const kNewResultSize = 150

function needs_broadcasts_for_network(params) {
    debug.log({
        params
    })
    // Note: Do nothing, get from Mongo.
    return false
}

// Note: Do nothing. Just take the default Mongo order for new.
function UpdateHandler(params, output) {
    debug.log({
        params,
    })
    return Promise.resolve(undefined)
}

function MakeQuery(queryRequest) {
    debug.log({
        queryRequest,
    })
    var query = {
        deleted: false,
        reference: null,
    }
    //    query._id = {}
    //    if (queryRequest.newEndpoints && queryRequest.newEndpoints.earliest) {
    //        query._id.$lt = new mongoose.Types.ObjectId(queryRequest.newEndpoints.earliest)
    //    }
    //    if (queryRequest.newEndpoints && queryRequest.newEndpoints.latest) {
    //        query._id.$gt = new mongoose.Types.ObjectId(queryRequest.newEndpoints.latest)
    //    }
    return query
}

function FinishSearchQuery(queryRequest, query) {
    debug.log({
        queryRequest,
        query,
    })
    const searchPart = {
        $text: {
            $search: queryRequest.searchTerm,
        }
    }

    const fullQuery = {
        ...query,
        ...searchPart,
    }

    debug.log({
        searchPart,
        fullQuery,
    })

    // Note: Inception.
    return (collection) => {
        return collection.find(fullQuery, {
//            score: {
//                $meta: "textScore"
//            }
        }).sort({
            generic0: -1,
        }).limit(kNewResultSize)
    }
}

function FinishGenericQuery(queryRequest, query) {
    debug.log({
        queryRequest,
        query,
    })
    // Note: Inception.
    return (collection) => {
        return collection.find(query).sort({
            _id: -1
        }).limit(kNewResultSize)
    }
}

function MakeMongoQuery(queryRequest) {
    debug.log({
        queryRequest
    })

    const query = MakeQuery(queryRequest)
    debug.log({
        query,
        kNewResultSize,
    })

    if (queryRequest.searchTerm) {
        return FinishSearchQuery(queryRequest, query)

    } else {
        return FinishGenericQuery(queryRequest, query)
    }
}

function QueryHandler(params) {
    debug.log({
        params
    })
    return new Promise(function(resolve, reject) {
        const error = debug.CheckArguments(params, [
            'queryRequest', 'authenticatedUser'
        ])
        debug.log({
            error
        })
        if (error) {
            debug.error({
                error
            })
            reject(error)
        } else {
            const mongoQuery = MakeMongoQuery(params.queryRequest)
            debug.log({
                mongoQuery
            })
            resolve({
                mongoQuery
            })
        }
    })
}

module.exports = {
    UpdateHandler,
    QueryHandler,
}
