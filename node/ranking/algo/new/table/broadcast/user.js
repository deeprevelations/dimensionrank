const rfr = require('rfr')
const debug = rfr('util/debug')
const enums = rfr('data/enums')
const tables = rfr('data/tables')

function needs_broadcast_for_user(params) {
    debug.log({
        params
    })
		// 1) is a broadcast
		// 2) the broadcast does not have a reference (i.e., is not a comment)
    return params.updateType == enums.ActionMapping.broadcast && !params.broadcast.reference
}

function UpdateHandler(params) {
    debug.log({
        params
    })
    if (needs_broadcast_for_user(params)) {
        const entry = {
            producername: params.producerTuple.userName,
            broadcastid: params.broadcast._id.toString(),
        }
        debug.log({
            entry
        })
        return tables.broadcast_for_user.Insert(entry)
    } else {
        return Promise.resolve()
    }
}

function QueryHandler(params) {
    debug.log({
        params
    })
    const promise = tables.broadcast_for_user.SelectMostRecent(
        'producername', params.queryRequest.channelName, 'broadcastid', params.batchSize)
    debug.log({
        promise
    })
    return tables.MongoIdsFromCassandraResult(promise, 'broadcastid')
}

module.exports = {
    UpdateHandler,
    QueryHandler,
}
