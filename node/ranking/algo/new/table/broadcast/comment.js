const rfr = require('rfr')
const debug = rfr('util/debug')
const objects = rfr('data/objects')
const tables = rfr('data/tables')

function needs_comments_for_broadcast(params) {
    debug.log({
        params
    })
    // return params.broadcast.reference
		return false
}


function UpdateHandler(params, output) {
    debug.log({
        params
    })
    const needs = needs_comments_for_broadcast(params)
    debug.log({
        needs
    })
    if (needs) {
        const entry = {
            channelname: params.broadcast.reference,
            broadcastid: params.broadcast._id.toString(),
        }
        debug.log({
            entry
        })
        return tables.comments_for_broadcast.Insert(entry)
    } else {
        return Promise.resolve(undefined)
    }
}

function MakeQuery(queryRequest) {
    debug.log({
        queryRequest,
    })
		// TODO(greg) Make this a mongo id and index on it.
    const query = {
        deleted: false,
        reference: queryRequest.channelName,
    }
    return query
}

function MakeMongoQuery(queryRequest) {
    debug.log({
        queryRequest
    })

    const query = MakeQuery(queryRequest)
    debug.log({
        query,
    })

    // Note: Inception.
    return (collection) => {
        return collection.find(query)
    }
}

function QueryHandler(params) {
    debug.log({
        params
    })
    return new Promise(function(resolve, reject) {
        const error = debug.CheckArguments(params, [
            'queryRequest', 'authenticatedUser'
        ])
        debug.log({
            error
        })
        if (error) {
            debug.error({
                error
            })
            reject(error)
        } else {
            const mongoQuery = MakeMongoQuery(params.queryRequest)
            debug.log({
                mongoQuery
            })
            resolve({
                mongoQuery
            })
        }
    })
}

//function QueryHandler(params) {
//    debug.log({
//        params
//    })
//    const promise = tables.comments_for_broadcast.SelectMostRecent(
//        'channelname', params.queryRequest.channelName, 'broadcastid', params.batchSize)
//    debug.log({
//        promise
//    })
//    return tables.MongoIdsFromCassandraResult(promise, 'broadcastid')
//    //		return tables.MongoIdsFromCassandraResult(tables.comments_for_broadcast.SelectMostRecent('channelname', queryRequest.channelName, 'broadcastid', params.batchSize), 'broadcastid')
//}


module.exports = {
    UpdateHandler,
    QueryHandler,
}
