const rfr = require('rfr')
const debug = rfr('util/debug')
const objects = rfr('data/objects')
const tables = rfr('data/tables')

const kEmptyChannel = ''

function needs_overall_toxic(params) {
    return false
}

function UpdateHandler(params, output) {
    debug.log({
        params
    })
    return Promise.resolve(undefined)
}

function QueryHandler(params) {
    return null
}

module.exports = {
    UpdateHandler,
    QueryHandler,
}
