const rfr = require('rfr')
const tables = rfr('data/tables')
const debug = rfr('util/debug')
const objects = rfr('data/objects')
const TimeUuid = require('cassandra-driver').types.TimeUuid

function needs_notification_for_user(params) {
    debug.log({
        params
    })
    return params.broadcast && params.referenceBroadcast

    // return false
    //		// is 1) a broadcast and 2) has a reference
    //    const isComment = params.updateType == objects.ActionMapping.broadcast && params.broadcast && params.broadcast.reference
    //		// is 1) a rating and 2) is public
    //		const isPublic = params.updateType == objects.ActionMapping.rating && params.visbility == objects.VisibilityMapping.public
    //		return isComment || isPublic
}

// We assume that either 'params.rating' or 'params.broadcast'.
function ChooseHandler(params) {
    if (params.rating) {
        return Promise.resolve()
        //        const entry = {
        //            channelname: params.rating.producerid,
        //						producername: params.rating.producername,
        //            ratingid: params.rating.id,
        //            rating: JSON.stringify(params.rating),
        //            commentid: null,
        //        }
        //        return tables.notification_for_user.Insert(entry)
    } else if (params.broadcast && params.referenceBroadcast) {
        debug.log({
            broadcast: params.broadcast,
            referenceBroadcast: params.referenceBroadcast,
        })
        const entry = {
            consumerid: params.referenceBroadcast.producerId,
            broadcastid: params.broadcast._id.toString(),
        }
        return tables.notification_for_user.Insert(entry)
    } else {
        return Promise.reject({
            error: 'InvalidArgument',
            details: [params],
        })
    }
}

function UpdateHandler(params) {
    debug.log({
        params,
    })
    if (needs_notification_for_user(params)) {
        const error = debug.CheckArguments(params, [])
        if (error) {
            return Promise.reject(error)
        } else {
            return ChooseHandler(params)
        }
    } else {
        return Promise.resolve()
    }
}

function QueryHandler(params) {
    debug.log({
        params
    })
    return tables.NotificationsFromCassandra(
        tables.notification_for_user.SelectMostRecent('consumerid', params.queryRequest.channelName, 'broadcastid', params.batchSize))
}

module.exports = {
    UpdateHandler,
    QueryHandler,
}
