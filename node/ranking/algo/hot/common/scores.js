//const rfr = require('rfr')
//const debug = rfr('util/debug')
//const objects = rfr('data/objects')
//
//function ComputeCountsMap(broadcast) {
//    var counts = {}
//    counts[objects.JudgmentMapping.money] = 0
//    counts[objects.JudgmentMapping.crap] = 0
//    counts[objects.JudgmentMapping.toxic] = 0
//    for (const judgment of broadcast.judgments) {
//        counts[judgment.judgment] += 1
//    }
//    return counts
//}
//
//function ComputeHotScore(broadcast) {
//    debug.log({
//        broadcast
//    })
//
//    const counts = ComputeCountsMap(broadcast)
//    const positive = counts[objects.JudgmentMapping.money]
//    const negative = counts[objects.JudgmentMapping.crap] + counts[objects.JudgmentMapping.toxic]
//    const numerator = positive
//    const denominator = positive + negative + 1
//    const ratio = 1.0 * numerator / denominator
//    debug.log({
//        counts,
//        positive,
//        negative,
//        numerator,
//        denominator,
//        ratio,
//    })
//    return ratio
//}
//
//function ComputeToxicScore(broadcast) {
//    debug.log({
//        broadcast
//    })
//
//    const counts = ComputeCountsMap(broadcast)
//    const positive = counts[objects.JudgmentMapping.toxic]
//    const negative = counts[objects.JudgmentMapping.money] + counts[objects.JudgmentMapping.crap]
//    const numerator = positive
//    const denominator = positive + negative + 1
//    const ratio = 1.0 * numerator / denominator
//    debug.log({
//        counts,
//        positive,
//        negative,
//        numerator,
//        denominator,
//        ratio,
//    })
//    return ratio
//}
//
//function ComputeScores(broadcast) {
//    debug.log({
//        broadcast
//    })
//    const result = {
//        hot: ComputeHotScore(broadcast),
//        toxic: ComputeToxicScore(broadcast),
//    }
//    debug.log({
//        result
//    })
//    return result
//}
//
//module.exports = {
//    ComputeScores,
//}
