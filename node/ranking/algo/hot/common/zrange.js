const rfr = require('rfr')
const debug = rfr('util/debug')
const objects = rfr('data/objects')
const dataRedis = rfr('data/redis')

function ZrangeKey(queryRequest) {
    debug.log({
        queryRequest,
    })
    const reducedType = objects.ReducedChannelType(queryRequest.channelType)
    const key = 'hot/' + reducedType + '/' + queryRequest.channelName
    debug.log({
        key,
    })
    return key
}

function ZrangePut(queryRequest, score, id) {
    debug.log({
        queryRequest,
        score,
        id
    })
    return new Promise(function(resolve, reject) {
        const key = ZrangeKey(queryRequest)
        dataRedis.client().zadd(key, score, id, function(error) {
            debug.log({
                error
            })
            if (error) {
                reject(error)
            } else {
                resolve(undefined)
            }
        })
    })
}

function ZrangeRange(queryRequest, from, to) {
    debug.log({
        queryRequest,
        from,
        to,
    })
    return new Promise(function(resolve, reject) {
        debug.log({})
        const key = ZrangeKey(queryRequest)
        debug.log({
            key
        })
        dataRedis.client().zrevrange(key, from, to, function(error, results) {
            debug.log({
                error,
                results,
            })
            if (error) {
                reject(error)
            } else {
                resolve(results)
            }
        })
    })
}

function MongoIdsFromSimpleList(promise) {
    debug.log({
        promise,
    })
    return new Promise(function(resolve, reject) {
        promise.then(mongoIds => {
            debug.log({
                mongoIds,
            })
            resolve({
                mongoIds,
            })
        }).catch(error => {
            debug.error({
                error,
            })
            reject(error)
        })
    })
}


module.exports = {
    ZrangePut,
    ZrangeRange,
    MongoIdsFromSimpleList,
}