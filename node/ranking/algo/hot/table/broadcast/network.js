//const rfr = require('rfr')
//const debug = rfr('util/debug')
//const enums = rfr('data/enums')
//const zrange = rfr('ranking/algo/hot/common/zrange')
//const finishBroadcast = rfr('ranking/finish/broadcast')
//
//function needs_network_hot_set(params) {
//    debug.log({
//        params
//    })
//    return params.updateType == enums.ModeMapping.original
//}
//
//function UpdateHandler(params) {
//    debug.log({
//        params
//    })
//    if (needs_network_hot_set(params)) {
//        const queryRequest = {
//            objectType: 'broadcast',
//            channelType: enums.ChannelTypeMapping.network,
//            channelName: params.broadcast.channelName,
//        }
//        return zrange.ZrangePut(queryRequest, params.scores.hot, params.broadcast._id.toString())
//    } else {
//        return Promise.resolve(undefined)
//    }
//}
//
//function QueryHandler(params, output) {
//    debug.log({
//        params,
//    })
//    return zrange.MongoIdsFromSimpleList(zrange.ZrangeRange(params.queryRequest, 0, 100))
//}
//
//module.exports = {
//    UpdateHandler,
//    QueryHandler,
//}
