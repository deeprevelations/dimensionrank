const rfr = require('rfr')
const debug = rfr('util/debug')
const objects = rfr('data/objects')
const zrange = rfr('ranking/algo/hot/common/zrange')
const finishBroadcast = rfr('ranking/finish/broadcast')

function needs_toxic_set(params) {
    debug.log({
        params
    })
    return params.broadcast.nontrivial
}

function UpdateHandler(params) {
    debug.log({
        params
    })
    if (needs_toxic_set(params)) {
        const queryRequest = {
            objectType: 'broadcast',
            channelType: objects.ChannelTypeMapping.toxic,
            channelName: '',
        }
        return zrange.ZrangePut(queryRequest, params.scores.toxic, params.broadcast._id.toString())
    } else {
        return Promise.resolve(undefined)
    }
}

function QueryHandler(params, output) {
    debug.log({
        params,
    })
    return zrange.MongoIdsFromSimpleList(zrange.ZrangeRange(params.queryRequest, 0, 100))
}

module.exports = {
    UpdateHandler,
    QueryHandler,
}
