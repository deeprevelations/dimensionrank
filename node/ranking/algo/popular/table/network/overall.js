//const rfr = require('rfr')
//const dataRedis = rfr('data/redis')
//const debug = rfr('util/debug')
//const enums = rfr('data/enums')
//
//// Return 100 results at a time.
//const kTopBatchSize = 100
//
//function SplitResults(singles) {
//    var pairs = []
//    for (var i = 0; i < singles.length / 2; ++i) {
//        pairs.push({
//            channelName: singles[i * 2],
//            count: singles[i * 2 + 1],
//        })
//    }
//    return pairs
//}
//
//function ListTopNetworksImpl(params, output) {
//    debug.log({
//        params
//    })
//    dataRedis.client().zrevrange('topNetworks', 0, kTopBatchSize, 'withscores', function(error, result) {
//        debug.log({
//            error,
//            result,
//        })
//        if (error) {
//            debug.error({
//                error
//            })
//            output.reject(error)
//        } else {
//            const pairs = SplitResults(result)
//            output.resolve({
//                networks: pairs
//            })
//        }
//    })
//}
//
//// For "true" networks, i.e., not users.
//function QueryHandler(params) {
//    debug.log({
//        params
//    })
//    return new Promise(function(resolve, reject) {
//        const error = debug.CheckArguments(params, [])
//        debug.log({
//            error
//        })
//        if (error) {
//            reject(error)
//        } else {
//            ListTopNetworksImpl(params, {
//                resolve,
//                reject,
//            })
//        }
//    })
//}
//
//function TallyNetwork(params, output) {
//    debug.log({
//        params
//    })
//    dataRedis.client().zincrby('topNetworks', 1, params.broadcast.channelName, function(error, result) {
//        debug.log({
//            error
//        })
//        if (error) {
//            debug.error({
//                error
//            })
//            output.reject(error)
//        } else {
//            debug.log({
//                result
//            })
//            output.resolve(undefined)
//        }
//    })
//}
//
//function UpdateFilterInputs(params, output) {
//    debug.log({
//        params
//    })
//    if (params.broadcast) {
//        const mode = enums.ReducedModeType(params.broadcast.mode)
//        debug.log({
//            mode,
//						other: enums.ActionMapping.original,
//        })
//        if (mode == enums.ActionMapping.original) {
//            TallyNetwork(params, output)
//        } else {
//            output.resolve(undefined)
//        }
//    } else {
//        output.resolve(undefined)
//    }
//}
//
//
//function UpdateHandler(params) {
//    debug.log({
//        params
//    })
//    return new Promise(function(resolve, reject) {
//        const error = debug.CheckArguments(params, ['updateType', 'producerTuple'])
//        // TODO(greg) Check params here?
//        UpdateFilterInputs(params, {
//            resolve,
//            reject
//        })
//    })
//}
//
//module.exports = {
//    UpdateHandler,
//    QueryHandler,
//}
