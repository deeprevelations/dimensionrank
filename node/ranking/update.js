// Root ranking/update module.

const rfr = require('rfr')
const debug = rfr('util/debug')
const updateMongo = rfr('ranking/start/updateMongo')
//const commonScores = rfr('ranking/algo/hot/common/scores')
const router = rfr('ranking/router')

function ConjoinAllUpdates(params, output) {
    debug.log({
        params
    })

    // Note: We should always have a broadcast at this point.
    const error = debug.CheckArguments(params, ['broadcast'])
    debug.log({
        error
    })

    if (error) {
        debug.error({
            error
        })
        output.reject(error)
    } else {
        // const scores = commonScores.ComputeScores(params.broadcast)

        var promises = []
        for (const [path, module] of Object.entries(router.handlerMap)) {
            debug.log({
                path
            })
//            promises.push(module.UpdateHandler({
//                ...params,
//                scores,
//            }))
            promises.push(module.UpdateHandler(params))
        }

        Promise.all(promises).then(allResult => {
            debug.log({
                allResult
            })
            output.resolve(undefined)
        }).catch(error => {
            debug.error({
                error
            })
            output.reject(error)
        })
    }
}

function UpdateMongo(params, output) {
    debug.log({
        params
    })
    updateMongo.UpdateHandler(params).then(broadcast => {
        debug.log({
            broadcast
        })
        ConjoinAllUpdates({
            ...params,
            broadcast,
        }, output)
    }).catch(error => {
        output.reject(error)
    })
}

function UpdateHandler(params) {
    debug.log({
        params
    })
    return new Promise(function(resolve, reject) {
        const error = debug.CheckArguments(params, ['updateType', 'producerTuple'])
        debug.log({
            error
        })
        if (error) {
            debug.error({
                error
            })
            reject(error)
        } else {
            // TODO(greg) Check params here?
            UpdateMongo(params, {
                resolve,
                reject
            })
        }
    })
}

module.exports = {
    UpdateHandler,
}
