const rfr = require('rfr')

const login = rfr('middleware/login')
const debug = rfr('util/debug')
const router = rfr('ranking/router')

function ComputeFinished(params, output) {
    debug.log({
        params
    })
    const finisher = router.finisherMap[params.queryRequest.objectType]
    if (!finisher) {
        output.reject({
            error: 'RouteNotFound',
            details: ['objectType', params.queryRequest.objectType],
        })
    } else {
        finisher.Finish(params).then(finishedResult => {
            debug.log({
                finishedResult
            })
            output.resolve(finishedResult)
        }).catch(error => {
            debug.error({
                error
            })
            output.reject(error)
        })
    }
}

function ComputeUnfinished(params, output) {
    debug.log({
        params
    })
    const routerKey = '/algo/' + params.queryRequest.rankingAlgorithm + '/table/' + params.queryRequest.objectType + '/' + params.queryRequest.channelType
    const handler = router.handlerMap[routerKey]
    debug.log({
        routerKey,
        handler,
    })

    if (!handler) {
        const error = {
            error: 'RouteNotFound',
            details: ['routerKey', routerKey],
        }
        debug.error({
            error
        })
        output.reject(error)
    } else {
        const innerParams = {
            ...params,
            batchSize: 25,
        }
        debug.log({
            innerParams
        })
        // Step 2: Run handler.
        handler.QueryHandler(innerParams).then(unfinishedResult => {
            debug.log({
                unfinishedResult
            })
            ComputeFinished({
                ...params,
                unfinishedResult,
            }, output)
        }).catch(error => {
            debug.error({
                error
            })
            output.reject(error)
        })
    }
}

function QueryHandler(params) {
    debug.log({
        params
    })
    return new Promise(function(resolve, reject) {
        // Step 1: Check the top level params.
        const error = debug.CheckArguments(params, [
            'queryRequest', 'authenticatedUser'
        ])
        debug.log({
            error
        })
        if (error) {
            debug.error({
                error
            })
            reject(error)
        } else {
            // Step 2: Check the inner queryRequest params.
            const queryError = debug.CheckArguments(params.queryRequest, [
                'rankingAlgorithm', 'objectType', 'channelType',
            ])
            debug.log({
                queryError
            })
            if (queryError) {
                debug.error({
                    queryError
                })
                reject(queryError)
            } else {
                // Step 3: Run the query.
                ComputeUnfinished(params, {
                    resolve,
                    reject,
                })
            }
        }
    })
}

module.exports = {
    QueryHandler,
}
