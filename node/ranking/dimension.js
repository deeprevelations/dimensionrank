// Root ranking/update module.

const rfr = require('rfr')
const dataRedis = rfr('data/redis')
const objects = rfr('data/objects')
const debug = rfr('util/debug')
const updateMongo = rfr('ranking/start/updateMongo')
const router = rfr('ranking/router')

const redact = rfr('data/broadcast/redact')

const kSearchLimit = 100

function Redact(params, output) {
    debug.log({
        params
    })
    var broadcasts = []
    for (const rawBroadcast of params.results) {
        if (!rawBroadcast.deleted) {
            const redactedBroadcast = redact.PolishBroadcast(rawBroadcast, params.authenticatedUser)
            debug.log({
                redactedBroadcast
            })
            broadcasts.push(redactedBroadcast)
        }
    }
    output.resolve({
        results: broadcasts,
        user_embedding: params.user_embedding,
    })
}

function MakeSearchQuery(params) {
    debug.log({
        params,
    })
    const trimmed = params.queryRequest.searchTerm.trim()

    var fullQuery = {
        deleted: false,
        reference: null,
    }

    debug.log({
        fullQuery,
    })
    var query = null
    if (trimmed) {
        fullQuery = {
            ...fullQuery,
            $text: {
                $search: trimmed,
            },
        }
        query = objects.Broadcast.find(fullQuery, {
            score: {
                $meta: "textScore"
            },
        }).sort({
            score: {
                $meta: "textScore"
            }
        })
    } else {
        query = objects.Broadcast.find(fullQuery).sort({
            _id: -1
        })
    }

    query = query.limit(kSearchLimit)

    debug.log({
        query,
    })
    return query
}

function MakeUserParamQuery(params) {
    debug.log({
        params
    })
    // Note: hard-coded keys
    const userParamKey = params.authenticatedUser ? 'consumer:' + params.authenticatedUser._id : 'consumer:generic'
    debug.log({
        userParamKey
    })
    return new Promise(function(resolve, reject) {
        dataRedis.client().get(userParamKey, function(error, result) {
            if (error) {
                reject(error)
            } else {
                resolve(result)
            }
        })
    })
}

function RunQueries(params, output) {
    console.log('RunQueries')
    debug.log({
        params
    })
    const queries = [
        MakeSearchQuery(params),
        MakeUserParamQuery(params),
    ]
    debug.log({
        queries
    })

    Promise.all(queries).then(allResult => {
        debug.log({
            allResult
        })
        Redact({
            results: allResult[0],
            user_embedding: allResult[1],
            authenticatedUser: params.authenticatedUser,
        }, output)
    }).catch(error => {
        debug.error({
            error
        })
    })
}

function QueryHandler(params) {
    debug.log({
        params
    })
    return new Promise(function(resolve, reject) {
        const error = debug.CheckArguments(params, ['authenticatedUser', 'queryRequest'])
        debug.log({
            error
        })
        if (error) {
            console.log('error')
            debug.error({
                error
            })
            reject(error)
        } else {
            console.log('hi1')
            const output = {
                resolve,
                reject,
            }
            console.log('hi1a')
            console.log({
                params,
                output
            })
            RunQueries(params, output)
        }
    })
}

module.exports = {
    QueryHandler,
}
