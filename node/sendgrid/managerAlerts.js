const sgMail = require('@sendgrid/mail');

const rfr = require('rfr')
const globalOptions = rfr('globalOptions')
const debug = rfr('util/debug')

function LoginAttempt(params) {
    debug.log({
        params
    })
    sgMail.setApiKey(globalOptions.options().sendgridApiKey)
    const managerEmail = globalOptions.options().managerEmail
		const message = globalOptions.options().vueBasePath
    debug.log({
        managerEmail,
				message,
    })
    const msg = {
        to: managerEmail,
        from: {
            name: 'Manager Alerts',
            email: 'mailman@deeprevelations.com',
        },
        subject: 'There has been a LOG-IN ATTEMPT',
        text: message,
        html: message,
    }
    debug.log({
        msg
    })
    return sgMail.send(msg)
}

module.exports = {
    LoginAttempt,
}
