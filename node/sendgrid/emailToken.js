const sgMail = require('@sendgrid/mail');

const rfr = require('rfr')
const globalOptions = rfr('globalOptions')
const debug = rfr('util/debug')

function MakeText(params) {
    return `
		Hello ${params.email},
		
		Use this link to log on to <strong>Deep Revelations</strong>:
		<a href="${params.url}">${params.url}</a>

		Thanks!	
	`
}

function MakeHtml(params) {
    return `
		Hello ${params.email},<br>
		<br>
		Use this link to log on to <strong>Deep Revelations</strong>:<br>
		<a href="${params.url}">${params.url}</a><br>
		<br>

		Thanks!
	`
}

function SendTheEmail(params) {
    sgMail.setApiKey(globalOptions.options().sendgridApiKey)
    const msg = {
        to: params.email,
        from: {
            name: 'Deep Revelations Live',
            email: 'mail@deeprevelations.com',
        },
        subject: 'Log-In Link',
        text: MakeText(params),
        html: MakeHtml(params),
    }
    return sgMail.send(msg)
}

module.exports = {
    SendTheEmail,
}
