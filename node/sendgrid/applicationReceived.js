const sgMail = require('@sendgrid/mail');

const rfr = require('rfr')

const globalOptions = rfr('globalOptions')
const objects = rfr('data/objects')
const mongoData = rfr('data/mongo')
const debug = rfr('util/debug')

function MakeText(params) {
    return `
		Hello ${params.fullName},
		
		We have received your application. We'll get back to you as soon as possible.
		
		Think different, again.
	`
}

function MakeHtml(params) {
    return `
		Hello ${params.fullName},<br>
		<br>
		We have received your application. We'll get back to you as soon as possible.
		<br>
		Think different, again.
	`
}

function SendTheEmail(email, fullName) {
    console.log('SendTheEmail', {
        email,
        fullName
    })
    sgMail.setApiKey(globalOptions.options().sendgridApiKey)
    const params = {
        email,
        fullName,
    }
    const msg = {
        to: params.email,
        from: 'Deep Revelations <mailman@deeprevelations.com>',
        subject: 'Application Received',
        text: MakeText(params),
        html: MakeHtml(params),
    }
    return sgMail.send(msg)
}

module.exports = {
    SendTheEmail,
}
