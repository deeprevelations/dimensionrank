const mongoose = require('mongoose')

const fs = require('fs')
const rfr = require('rfr')
const options = rfr('options')
const debug = rfr('util/debug')
const globalOptions = rfr('globalOptions')
const app = rfr('api/app')
const dataCassandra = rfr('data/cassandra')
const dataMongo = rfr('data/mongo')
const dataRedis = rfr('data/redis')

function LoadOptions(optionsFname) {
    const fileText = fs.readFileSync(optionsFname)
    return JSON.parse(fileText)
}

async function Main(options) {
    if (options.log) {
        debug.SetRegex(options.log)
    }

    const jsonConfig = LoadOptions(options.config)
    debug.log('Main', {
        jsonConfig: 'hidden'
    })

    const loadResult = globalOptions.LoadOptions(jsonConfig)
    debug.log('Main', {
        loadResult
    })

//    const cassandraResult = await dataCassandra.ResetCassandraClient(jsonConfig.cassandra)
//    debug.log('Main', {
//        cassandraResult,
//    })

    const mongoResult = await dataMongo.ResetMongoClient(jsonConfig.mongo)
    debug.log('Main', {
        mongoResult,
    })

    const redisResult = await dataRedis.ResetRedisClient(jsonConfig.redis)
    debug.log('Main', {
        redisResult,
    })

    const port = jsonConfig.port
    debug.log('Main', {
        port,
    })

    app.listen(port, () => debug.log(`Example app listening on port ${port}!`))
}

Main(options({
    'config': String,
    'log': String,
}))
