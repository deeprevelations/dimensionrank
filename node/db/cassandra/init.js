// Changes the email field in some collection to make the email lowercase.
const mongoose = require('mongoose')
const rfr = require('rfr')
const options = rfr('options')
const dataBases = rfr('data/bases')
const dataCassandra = rfr('data/cassandra')
const debug = rfr('util/debug')
const timeKey = rfr('util/timeKey')

const resetQueries = require('./queries/resetQueries')
const objects = require('./queries/objects')
const events = require('./queries/events')

async function RunQueries() {
    // const queries = [...resetQueries, ...objects, ...events]
    const queries = [...objects, ...events]
    for (const query of queries) {
        console.log('Main', {
            query
        })
        const result = await dataCassandra.Execute(query)
        console.log('Main', {
            result
        })
    }
}

async function Main(options) {
    console.warn('Main', {
        options: 'hidden',
        hot: options.hot,
        productionDataOverride: options.productionDataOverride,
    })

    const loadResult = await dataBases.InitDevelopmentOnly(options.config, ['cassandra'], options.productionDataOverride)
    console.log('Main', {
        loadResult
    })


    const keyIsValid = timeKey.CheckTimeKeyIsValid(options.hot)
    console.log('Main', {
        keyIsValid
    })
    if (keyIsValid) {
        debug.warn({
            hot: options.hot
        })
        const deleteResult = await RunQueries()
    } else {
        debug.warn('not running')
    }

    process.exit(0)
}

Main(options({
    'config': String,
    'hot': Number,
    'productionDataOverride': Number,
}))
