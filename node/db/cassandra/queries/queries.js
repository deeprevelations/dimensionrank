module.exports = [
`
CREATE TABLE broadcast_object(
    id TIMEUUID PRIMARY KEY,
		producer ASCII,
		mode INT,
		judgment INT,
		reference ASCII,
		channelType INT,
		channelName ASCII,
		resource ASCII,
		details TEXT,
		);
`,
`
CREATE TABLE rating_event(
    id TIMEUUID PRIMARY KEY,
		consumer ASCII,
		producer ASCII,
		type INT,
		reference ASCII,
		referenceType INT,
		mode INT,
		judgment INT,
		details TEXT,
		);
`,
`
CREATE TABLE notification_object(
    id TIMEUUID PRIMARY KEY,
		producer ASCII,
		consumer ASCII,
		mode INT,
		judgment INT,
		reference ASCII,
		details TEXT,
		);
`,
`
CREATE TABLE login_attempt_event(
    id TIMEUUID PRIMARY KEY,
		email ASCII,
		);
`,
`
CREATE TABLE application_event(
    id TIMEUUID PRIMARY KEY,
		email ASCII,
		fullname TEXT,
		profile TEXT,
		);
`,
`
CREATE TABLE email_use_event(
    id TIMEUUID PRIMARY KEY,
		emailtoken ASCII,
		userid ASCII,
		);
`,
`
CREATE TABLE logout_event(
    id TIMEUUID PRIMARY KEY,
		userid ASCII,
		);
`,
`
CREATE TABLE label_event(
    id TIMEUUID PRIMARY KEY,
		userid ASCII,
		objectid ASCII,
		labelname ASCII,
		);
`,
`
CREATE TABLE feed_single_transmission(
    id TIMEUUID PRIMARY KEY,
		userid ASCII,
		objectid ASCII,
		);
`,
`
CREATE TABLE recommendation_feed_action(
    id TIMEUUID PRIMARY KEY,
		userid ASCII,
		objectid ASCII,
		recommendation_made ASCII,
		details TEXT,
		);
`,
`
CREATE TABLE label_train_action(
    id TIMEUUID PRIMARY KEY,
		userid ASCII,
		objectid ASCII,
		labelname ASCII,
		gold INT,
		prediction INT,
		details TEXT,
		);
`,
]
