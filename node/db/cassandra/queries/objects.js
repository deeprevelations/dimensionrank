module.exports = [
    // TODO(greg) Remove this, use a query.
    `
				CREATE TABLE IF NOT EXISTS broadcast_for_user(
						producername VARCHAR,
						broadcastid VARCHAR,
						primary key (producername, broadcastid)
						) with clustering order by (broadcastid desc);
				`,
    `
        CREATE TABLE IF NOT EXISTS notification_for_user(
						consumerid VARCHAR,
						broadcastid VARCHAR,
            primary key(consumerid, broadcastid)
        ) with clustering order by(broadcastid desc);
        `,
    `
        CREATE TABLE IF NOT EXISTS rating_event(
            id TIMEUUID PRIMARY KEY,
            raterid ASCII,
            producerid ASCII,
            reference ASCII,
            variable INT,
            value INT,
        );
        `,
]