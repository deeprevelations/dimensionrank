module.exports = [
`
CREATE TABLE IF NOT EXISTS login_attempt_event(
    id TIMEUUID PRIMARY KEY,
		email ASCII,
		);
`,
`
CREATE TABLE IF NOT EXISTS application_event(
    id TIMEUUID PRIMARY KEY,
		email ASCII,
		fullname TEXT,
		profile TEXT,
		);
`,
`
CREATE TABLE IF NOT EXISTS email_use_event(
    id TIMEUUID PRIMARY KEY,
		emailtoken ASCII,
		userid ASCII,
		);
`,
`
CREATE TABLE IF NOT EXISTS logout_event(
    id TIMEUUID PRIMARY KEY,
		userid ASCII,
		);
`,
`
CREATE TABLE IF NOT EXISTS label_event(
    id TIMEUUID PRIMARY KEY,
		userid ASCII,
		objectid ASCII,
		labelname ASCII,
		);
`,
`
CREATE TABLE IF NOT EXISTS feed_single_transmission(
    id TIMEUUID PRIMARY KEY,
		userid ASCII,
		objectid ASCII,
		);
`,
`
CREATE TABLE IF NOT EXISTS recommendation_feed_action(
    id TIMEUUID PRIMARY KEY,
		userid ASCII,
		objectid ASCII,
		recommendation_made ASCII,
		details TEXT,
		);
`,
`
CREATE TABLE IF NOT EXISTS label_train_action(
    id TIMEUUID PRIMARY KEY,
		userid ASCII,
		objectid ASCII,
		labelname ASCII,
		gold INT,
		prediction INT,
		details TEXT,
		);
`,
]
