module.exports = [
`
DROP KEYSPACE IF EXISTS development1;
`,
`
CREATE KEYSPACE development1
WITH replication = {'class':'SimpleStrategy', 'replication_factor' : 1};
`,
]
