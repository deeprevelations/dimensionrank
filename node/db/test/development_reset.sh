CONFIG_FILE=$1
HOT_KEY=$2

if [ -z $CONFIG_FILE ]; then
	echo 'arguments required: CONFIG_FILE HOT_KEY'
	exit 1
fi

if [ -z $HOT_KEY ]; then
	echo 'arguments required: CONFIG_FILE HOT_KEY'
	exit 1
fi

node cassandra/reset.js --config $CONFIG_FILE --hot $HOT_KEY
node cassandra/init.js --config $CONFIG_FILE --hot $HOT_KEY
node redis/reset.js --config $CONFIG_FILE --hot $HOT_KEY
node mongo/reset.js --config $CONFIG_FILE --hot $HOT_KEY
node mongo/addDebugUsers.js --config $CONFIG_FILE --hot $HOT_KEY


