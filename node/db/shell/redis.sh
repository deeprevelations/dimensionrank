CONFIG=$1

if [ -z ${CONFIG} ]; then
	echo CONFIG not set;
	exit 1
fi

OUTPUT="$(node redis/shellUtil.js ${CONFIG})"

exec $OUTPUT
