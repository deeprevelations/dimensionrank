// Changes the email field in some collection to make the email lowercase.
const mongoose = require('mongoose')
const rfr = require('rfr')
const options = rfr('options')
const objects = rfr('data/objects')
const dataBases = rfr('data/bases')
const debug = rfr('util/debug')
const timeKey = rfr('util/timeKey')

async function Main(options) {
    console.warn('Main', {
        options: 'hidden',
        hot: options.hot,
        productionDataOverride: options.productionDataOverride,
    })

    const loadResult = await dataBases.InitDevelopmentOnly(options.config, ['mongo'], options.productionDataOverride)
    console.log('Main', {
        loadResult
    })


    const keyIsValid = timeKey.CheckTimeKeyIsValid(options.hot)
    console.log('Main', {
        keyIsValid
    })
    if (keyIsValid) {
        debug.warn({
            hot: options.hot
        })
        const deleteResult = await objects.Broadcast.deleteMany({})
        console.log('Main', {
            deleteResult
        })
    } else {
        const statsResult = await objects.Broadcast.find({})
        debug.warn({
            length: statsResult.length,
        })
    }

    process.exit(0)
}

Main(options({
    'config': String,
    'hot': Number,
    'productionDataOverride': Number,
}))