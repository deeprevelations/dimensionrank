// Changes the email field in some collection to make the email lowercase.
const mongoose = require('mongoose')
const rfr = require('rfr')
const options = rfr('options')
const objects = rfr('data/objects')
const dataBases = rfr('data/bases')
const debug = rfr('util/debug')
const timeKey = rfr('util/timeKey')
const resetCookie = rfr('data/cookieToken/reset')

function AddCookies(params, output) {
    debug.log({
        params
    })
    var promises = []
    for (const user of params.users) {
        debug.log({
            user
        })
        // Note: use the 'userName' as the cookieToken.
        promises.push(resetCookie.ResetCookieToken(user.userName, user))
    }

    Promise.all(promises).then(result => {
        output.resolve(undefined)
    }).catch(error => {
        output.reject(error)
    })

}

function CreateTheUsers(params, output) {
    debug.log({
        params
    })
    var promises = []
    var users = []
    for (var i = 0; i < 10; ++i) {
        const userName = "user" + i
        const email = userName + "@email.com"
        const user = new objects.User({
            _id: new mongoose.Types.ObjectId(),
            email,
            userName,
        })
        users.push(user)
        promises.push(user.save())
    }

    Promise.all(promises).then(result => {
        AddCookies({
            users
        }, output)
    }).catch(error => {
        output.reject(error)
    })
}

function ClearUsers(params, output) {
    debug.log({
        params
    })
    const promise = objects.User.deleteMany({})
    console.log({
        promise
    })
    promise.then(result => {
        CreateTheUsers(params, output)
    }).catch(error => {
        output.reject(error)
    })
}

function CreateDebugUsers(params) {
    debug.log({
        params
    })
    return new Promise(function(resolve, reject) {
        ClearUsers(params, {
            resolve,
            reject
        })
    })
}

async function Main(options) {
    debug.warn('Main', {
        options: 'hidden'
    })

    const loadResult = await dataBases.InitDevelopmentOnly(options.config, ['mongo', 'redis'])
    debug.log('Main', {
        loadResult
    })


    const keyIsValid = timeKey.CheckTimeKeyIsValid(options.hot)
    debug.log('Main', {
        keyIsValid
    })
    if (keyIsValid) {
        debug.warn({
            hot: options.hot
        })
        CreateDebugUsers({}).then(result => {
            debug.warn({
                result
            })
            process.exit(0)
        }).catch(error => {
            debug.warn({
                error
            })
            process.exit(1)
        })
    } else {
        debug.warn('doing nothing')
        process.exit(0)
    }

}

Main(options({
    'config': String,
    'hot': Number,
}))
