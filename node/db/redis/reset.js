// Changes the email field in some collection to make the email lowercase.
const rfr = require('rfr')
const options = rfr('options')
const objects = rfr('data/objects')
const dataBases = rfr('data/bases')
const dataRedis = rfr('data/redis')
const debug = rfr('util/debug')
const timeKey = rfr('util/timeKey')

function ClearRedis() {
    debug.warn({})
    return new Promise(function(resolve, reject) {
        dataRedis.client().flushall(function(error) {
            debug.warn({
                error
            })
            if (error) {
                reject(error)
            } else {
                resolve(undefined)
            }
        })
    })
}

async function Main(options) {
    console.warn('Main', {
        options: 'hidden',
        hot: options.hot,
        productionDataOverride: options.productionDataOverride,
    })

    const loadResult = await dataBases.InitDevelopmentOnly(options.config, ['redis'], options.productionDataOverride)
    console.log('Main', {
        loadResult
    })


    const keyOk = timeKey.CheckTimeKeyIsValid(options.hot)
    console.log('Main', {
        keyOk
    })
    if (keyOk) {
        const deleteResult = await ClearRedis()
        debug.warn({
            deleteResult
        })
    } else {
        debug.warn('doing nothing')
    }

    process.exit(0)
}

Main(options({
    'config': String,
    'hot': Number,
    'productionDataOverride': Number,
}))
