const fs = require('fs')

const debug = false

const optionsFname = process.argv[2]
if (!optionsFname) {
    console.log('No file name given')
    process.exit(1)
}

const fileText = fs.readFileSync(optionsFname)
const json = JSON.parse(fileText)
if (debug)
console.log({ json })

const uri = json.redis.host

const finalCommand = 'redis-cli -h ' + uri 

console.log(finalCommand)
