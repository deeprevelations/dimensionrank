// This file is for the *middleware* function that adds the user info to the
// request.

const rfr = require('rfr')

const objects = rfr('data/objects')
const cookieTokens = rfr('data/cookieToken/find')
const debug = rfr('util/debug')

function NotRequired(request, response, next) {
    next()
}

function Required(request, response, next) {
    const cookieToken = request.body.cookieToken
    debug.log({
        cookieToken
    })

    // Short-circuit if cookieToken is null, i.e. don't hit DB.
    if (!cookieToken) {
        response.status(200).json({
            success: false,
            error: 'AuthorizationFailed',
        })
    } else {
        cookieTokens.FindCookieTokenUser(cookieToken).then(userPair => {
            debug.log({
                userPair
            })
            if (userPair) {
                request.authenticatedUser = userPair
                next()
            } else {
                response.status(200).json({
                    success: false,
                    error: 'AuthorizationFailed',
                })
            }
        }).catch(error => {
            debug.log({
                error
            })
            response.status(200).json({
                success: false,
                error: 'AuthorizationFailed',
            })
        })
    }
}

function Optional(request, response, next) {
    const cookieToken = request.body.cookieToken
    debug.log({
        cookieToken
    })

    // Short-circuit if cookieToken is null, i.e. don't hit DB.
    if (!cookieToken) {
        request.authenticatedUser = {
            anonymous: true
        }
        next()
    } else {
        cookieTokens.FindCookieTokenUser(cookieToken).then(userPair => {
            debug.log({
                userPair
            })
            if (userPair) {
                request.authenticatedUser = userPair
                next()
            } else {
                response.status(200).json({
                    success: false,
                    error: 'AuthorizationFailed',
                })
            }
        }).catch(error => {
            debug.log({
                error
            })
            response.status(200).json({
                success: false,
                error: 'AuthorizationFailed',
            })
        })
    }
}

module.exports = {
    Required,
    NotRequired,
		Optional,
}
