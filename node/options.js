const nopt = require('nopt')

function ParseOptions(configTuple) {
    return nopt(configTuple, {}, process.argv, 2)
}

module.exports = ParseOptions
