const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const objects = rfr('data/objects')
const login = rfr('middleware/login')
const createBroadcast = rfr('data/broadcast/original')
const debug = rfr('util/debug')

function ApiCreateBroadcast(params, response) {
    debug.log({
        params
    })
    createBroadcast.CreateOriginalBroadcast(params).then(broadcast => {
        debug.log({
            broadcast
        })
        response.status(200).json({
            success: true,
            broadcast,
        })
    }).catch(error => {
        debug.log({
            error
        })
        response.status(200).json({
            success: false,
            error,
        })
    })
}

router.post('', login.Required, (request, response, next) => {
    debug.log({
        body: request.body
    })

    const params = {
        authenticatedUser: request.authenticatedUser,
        textarea: request.body.textarea,
    }
    debug.log({
        params
    })

    const error = debug.CheckArguments(params, ['authenticatedUser', 'textarea'])
    debug.log({
        error
    })
    if (error) {
        response.status(200).json({
            success: false,
            error,
        })
    } else {
        ApiCreateBroadcast(params, response)
    }
})

module.exports = router
