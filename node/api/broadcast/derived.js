const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const objects = rfr('data/objects')
const login = rfr('middleware/login')
const derivedBroadcast = rfr('data/broadcast/derived')
const debug = rfr('util/debug')

function ApiDerivedBroadcast(params, response) {
    debug.log({
        params
    })
    derivedBroadcast.CreateDerivedBroadcast(params).then(broadcast => {
        debug.log({
            broadcast
        })
        response.status(200).json({
            success: true,
            broadcast,
        })
    }).catch(error => {
        debug.error({
            error
        })
        response.status(200).json({
            success: false,
            error,
        })
    })
}

router.post('', login.Required, (request, response, next) => {
    debug.log({
        body: request.body
    })

    const params = {
        authenticatedUser: request.authenticatedUser,
        textarea: request.body.textarea,
        reference: request.body.reference,
    }
    debug.log({
        params
    })

    const error = debug.CheckArguments(params, ['authenticatedUser', 'textarea', 'reference'])
    debug.log({
        error
    })
    if (error) {
        debug.log({
            error
        })
        response.status(200).json({
            success: false,
            error,
        })
    } else {
        ApiDerivedBroadcast(params, response)
    }
})

module.exports = router
