const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const objects = rfr('data/objects')
const login = rfr('middleware/login')
const broadcastDelete = rfr('data/broadcast/delete')
const debug = rfr('util/debug')

function DeleteBroadcast(params, response) {
    debug.log({
        params
    })
    broadcastDelete.DeleteBroadcast(params).then(result => {
        response.status(200).json({
            success: true,
        })
    }).catch(error => {
        response.status(200).json({
            success: false,
            error,
        })
    })
}

router.post('', login.Required, (request, response, next) => {
    debug.log({
        body: request.body
    })
    const broadcastId = request.body.broadcastId
    if (!broadcastId) {
        response.status(200).json({
            success: false,
            error: 'MissingArguments',
            details: ['broadcastId', broadcastId],
        })
    } else {
        DeleteBroadcast({
            authenticatedUser: request.authenticatedUser,
            broadcastId,
        }, response)
    }
})

module.exports = router
