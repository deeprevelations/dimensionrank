const express = require('express')
const router = express.Router()
const rfr = require('rfr')
const mongoose = require('mongoose')

const objects = rfr('data/objects')
const login = rfr('middleware/login')
const debug = rfr('util/debug')
const redact = rfr('data/broadcast/redact')

function ReadAndRespond(params, response) {
    debug.log({
        params
    })
    const id = new mongoose.Types.ObjectId(params.broadcastId)
    debug.log({
        id
    })
    objects.Broadcast.findOne(id).then(internalBroadcast => {
        debug.log({
            internalBroadcast
        })
        if (!internalBroadcast) {
            response.status(200).json({
                success: false,
                error: 'CassandraObjectNotFound',
                details: ['broadcast', params.broadcastId],
            })
        } else {
            const publicBroadcast = redact.PolishBroadcast(internalBroadcast, params.authenticatedUser)
            debug.log({
                internalBroadcast,
                publicBroadcast,
            })
            response.status(200).json({
                success: true,
                broadcast: publicBroadcast,
            })
        }
    }).catch(error => {
        debug.error({
            error
        })
        response.status(200).json({
            success: false,
            error,
        })
    })
}

router.post('', login.Required, (request, response, next) => {
    debug.log('/api/broadcast/read', {
        body: request.body
    })
    const broadcastId = request.body.broadcastId
    if (!broadcastId) {
        response.status(200).json({
            success: false,
            error: 'MissingArguments',
            detail: ['broadcastId', broadcastId],
        })
    } else {
        ReadAndRespond({
            broadcastId,
            authenticatedUser: request.authenticatedUser,
        }, response)
    }
})

module.exports = router