const express = require('express')
const router = express.Router()
const rfr = require('rfr')
const objects = rfr('data/objects')
const debug = rfr('util/debug')

function ValidateEmailToken(params, response) {
    debug.log('ValidateEmailToken', {
        params
    })
    objects.EmailToken.findById(params.emailToken).then(result => {
        debug.log('ValidateEmailToken', {
            result
        })
        if (result) {
            response.status(200).json({
                success: true,
                email: result.email,
            })
        } else {
            response.status(200).json({
                success: false,
                error: 'resultIsNull',
            })
        }
    }).catch(error => {
        response.status(200).json({
            success: false,
            error,
        })
    })
}

router.post('', (request, response, next) => {
    debug.log('/api/emailToken/validate', {
        body: request.body
    })
    const emailToken = request.body.emailToken
    if (!emailToken) {
        response.status(200).json({
            error: 'MissingArguments',
            details: ['emailToken'],
        })
    } else {
        ValidateEmailToken({
            emailToken
        }, response)
    }
})

module.exports = router
