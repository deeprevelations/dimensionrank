// 1. Check if email is authorized
// 2. Create the EmailToken
// 3. Send an email
const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const rfr = require('rfr')

const debug = rfr('util/debug')
const globalOptions = rfr('globalOptions')
const objects = rfr('data/objects')
const findEmailAuthorization = rfr('data/emailAuthorization/find')
const sendEmailToken = rfr('sendgrid/emailToken')
const logEvents = rfr('data/log/events')

function LogLoginAttempt(params, response) {
    debug.log({
        params
    })
    logEvents.LogLoginAttempt(params).then(logResponse => {
        debug.log({
            logResponse
        })
    }).catch(error => {
        debug.log({
            error
        })
    })
}

function SendTheEmail(params, response) {
    debug.log({
        params
    })
    sendEmailToken.SendTheEmail(params).then(result => {
        // Tell the user to check their email.
        response.status(200).json({
            success: true,
        })
    }).catch(error => {
        debug.log({
            error
        })
        response.status(200).json({
            success: false,
            error,
        })
    })
}

function CreateTheToken(params, response) {
    debug.log({
        params
    })
    const emailToken = new objects.EmailToken({
        _id: new mongoose.Types.ObjectId(),
        email: params.email,
        //        user: params.user,
    })

    emailToken.save().then(function(result) {
        const token = result._id
        debug.log({
            result,
        })
        const url = globalOptions.options().vueBasePath + '/login/check/' + token
        debug.log({
            url
        })
        SendTheEmail({
            ...params,
            url
        }, response)

    }).catch(error => {
        debug.log({
            error
        })

        response.status(200).json({
            success: false,
            error,
        })

    })
}

//function CheckEmailAuthorization(params, response) {
//    debug.log({
//        params
//    })
//    objects.User.findOne({
//        email: params.email
//    }).then(userResult => {
//        debug.log({
//            userResult
//        })
//        if (userResult) {
//            CreateTheToken({
//                email: params.email,
//                user: userResult,
//            }, response)
//        } else {
//            response.status(200).json({
//                success: false,
//                emailNotAuthorized: true,
//            })
//        }
//    }).catch(error => {
//        debug.log({
//            error
//        })
//        response.status(200).json({
//            success: false,
//            error,
//        })
//    })
//}

router.post('', (request, response, next) => {
    debug.log({
        body: request.body
    })

    const email = request.body.email.toLowerCase()
    const params = {
        email
    }
    const error = debug.CheckArguments(params, ['email'])
    debug.log({
        error
    })
    if (error) {
        response.status(200).json({
            success: false,
            error,
        })
    } else {
        LogLoginAttempt({
            email
        })
        CreateTheToken({
            email
        }, response)
    }
})

module.exports = router
