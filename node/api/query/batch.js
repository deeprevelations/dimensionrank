const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const queryBatch = rfr('ranking/query')
const login = rfr('middleware/login')
const debug = rfr('util/debug')

const dimension = rfr('ranking/dimension')

function RouteTheQuery(params, response) {
    debug.log({
        params
    })
    dimension.QueryHandler(params).then(rankingResponse => {
        debug.log({
            rankingResponse
        })
        response.status(200).json({
            success: true,
            ...rankingResponse,
        })
    }).catch(error => {
        response.status(200).json({
            success: false,
            error,
        })
    })
}

router.post('', login.Optional, (request, response, next) => {
    debug.log({
        body: request.body
    })
    const params = {
        authenticatedUser: request.authenticatedUser,
        queryRequest: {
						searchTerm: request.body.searchTerm,
        },
    }
    debug.log({
        params
    })
    const error = debug.CheckArguments(params, ['authenticatedUser', 'queryRequest'])
    debug.log({
        error
    })
    if (error) {
        response.status(200).json({
            success: false,
            error,
        })
    } else {
        RouteTheQuery(params, response)
    }
})

module.exports = router
