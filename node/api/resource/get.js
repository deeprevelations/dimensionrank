// 'resourceResult/get' is 
const express = require('express')
const router = express.Router()

const rfr = require('rfr')

const objects = rfr('data/objects')
const getResource = rfr('data/resource/get')
const login = rfr('middleware/login')
const debug = rfr('util/debug')

function ReadResource(params, response) {
    debug.log('ReadResource', {
        params
    })
    getResource.BlockingResouceFromUrl({
        url: params.url
    }).then(resource => {
        debug.log('ReadResource', {
            resource
        })
        response.status(200).json({
            success: true,
            resource,
        })
    }).catch(error => {
        debug.log('ReadResource', {
            error
        })
        response.status(200).json({
            success: false,
            error,
        })
    })
}

router.post('', login.Required, (request, response, next) => {
    const url = request.body.url
    if (!url) {
        response.status(200).json({
            success: false,
            error: 'MissingArguments',
            details: ['url', url],
        })
    } else {
        ReadResource({
            url
        }, response)
    }
})

module.exports = router
