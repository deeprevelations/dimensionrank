const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const login = rfr('middleware/login')
const objects = rfr('data/objects')
const userConfirm = rfr('data/user/confirm')
const resetCookieToken = rfr('data/cookieToken/reset')
const debug = rfr('util/debug')

function DoConfirm(params, response) {
    debug.log({
        params
    })
    userConfirm.ConfirmUser(params).then(user => {
        debug.log({
            user
        })
        response.status(200).json({
            success: true,
            user,
        })
    }).catch(error => {
        debug.log({
            error
        })
        response.status(200).json({
            success: false,
            error,
        })
    })
}

router.post('', (request, response, next) => {
    debug.log({
        body: request.body
    })
    const params = {
        token: request.body.token,
    }
    debug.log({
        params
    })
    const error = debug.CheckArguments(params, ['token'])
    if (error) {
        response.status(200).json({
            success: false,
            error,
        })
    } else {
        DoConfirm(params, response)
    }
})

module.exports = router
