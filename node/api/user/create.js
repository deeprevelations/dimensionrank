const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const login = rfr('middleware/login')
const objects = rfr('data/objects')
const userCreate = rfr('data/user/create')
const resetCookieToken = rfr('data/cookieToken/reset')
const debug = rfr('util/debug')

//function ResetCookieToken(params, response) {
//    debug.log({
//        params
//    })
//    objects.User.findById(params.userId).then(userResult => {
//        debug.log({
//            userResult
//        })
//        resetCookieToken.ResetCookieToken(params.cookieToken, userResult).then(cookieResponse => {
//            debug.log({
//                cookieResponse
//            })
//            response.status(200).json({
//                success: true,
//                cookieToken: cookieResponse.cookieToken,
//                authenticatedUser: cookieResponse.authenticatedUser,
//            })
//        }).catch(error => {
//            debug.error({
//                error
//            })
//            response.status(200).json({
//                success: false,
//                error,
//            })
//        })
//    }).catch(error => {
//        debug.error({
//            error
//        })
//        response.status(200).json({
//            success: false,
//            error,
//        })
//    })
//}

function DoUserCreate(params, response) {
    debug.log({
        params
    })
    userCreate.CreateUser(params).then(user => {
        debug.log({
            user
        })
        response.status(200).json({
            success: true,
            user,
        })
//        ResetCookieToken({
//            ...params,
//            user
//        }, response)
    }).catch(error => {
        debug.log({
            error
        })
        response.status(200).json({
            success: false,
            error,
        })
    })
}

router.post('', (request, response, next) => {
    debug.log({
        body: request.body
    })
    const params = {
        userName: request.body.userName,
        email: request.body.email,
        password: request.body.password,
    }
    debug.log({
        params
    })
    const error = debug.CheckArguments(params, ['userName', 'email', 'password'])
    if (error) {
        response.status(200).json({
            success: false,
            error,
        })
    } else {
        DoUserCreate(params, response)
    }
})

module.exports = router
