const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const objects = rfr('data/objects')
const login = rfr('middleware/login')
const userHistory = rfr('data/user/history')
const debug = rfr('util/debug')

function LoadHistory(params, response) {
    debug.log('LoadHistory', {
        params
    })
    // TODO(greg) Check the arguments.
    userHistory.LoadUserHistory(params).then(broadcasts => {
        debug.log('LoadHistory', {
            broadcasts
        })
        response.status(200).json({
            success: true,
            ...broadcasts,
        })
    }).catch(error => {
        debug.log('LoadHistory', {
            error
        })
        response.status(200).json({
            success: false,
            error,
        })
    })
}

router.post('', login.Required, (request, response, next) => {
    LoadHistory({
        authenticatedUser: request.authenticatedUser,
    }, response)
})

module.exports = router
