const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const objects = rfr('data/objects')
const login = rfr('middleware/login')
const debug = rfr('util/debug')

function RedactSelfTuple(fullUser) {
    var buffer = {
        _id: fullUser._id,
        email: fullUser.email,
        userName: fullUser.userName,
        profile: fullUser.profile,
        description: fullUser.description,
    }
    return buffer
}

function ReadSelfUser(params, response) {
    debug.log('ReadSelfUser', {
        params
    })
    objects.User.findById(params.userId).then(user => {
        const userView = RedactSelfTuple(user)
        response.status(200).json({
            success: true,
            userView,
        })
    }).catch(error => {
        response.status(200).json({
            success: false,
            error,
        })
    })
}

function ParseArguments(request, response) {
    ReadSelfUser({
        userId: request.authenticatedUser._id,
    }, response)
}

router.post('', login.Required, (request, response, next) => {
    ParseArguments(request, response)
})

module.exports = router
