const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const login = rfr('middleware/login')
const objects = rfr('data/objects')
const initialSelect = rfr('data/user/userName/initialSelect')
const resetCookieToken = rfr('data/cookieToken/reset')
const debug = rfr('util/debug')

function ResetCookieToken(params, response) {
    debug.log('ResetCookieToken', {
        params
    })
    objects.User.findById(params.userId).then(userResult => {
        debug.log('ResetCookieToken', {
            userResult
        })
        resetCookieToken.ResetCookieToken(params.cookieToken, userResult).then(cookieResponse => {
            debug.log('ResetCookieToken', {
                cookieResponse
            })
            response.status(200).json({
                success: true,
                cookieToken: cookieResponse.cookieToken,
                authenticatedUser: cookieResponse.authenticatedUser,
            })
        }).catch(error => {
            debug.log('ResetCookieToken', {
                error
            })
            response.status(200).json({
                success: false,
                error,
            })
        })
    }).catch(error => {
        debug.log('ResetCookieToken', {
            error
        })
        response.status(200).json({
            success: false,
            error,
        })
    })
}

function PerformInitialSelect(params, response) {
    debug.log('PerformInitialSelect', {
        params
    })
    initialSelect.InitialSelectUserName(params.userId, params.userName).then(result => {
        debug.log('PerformInitialSelect', {
            result
        })
        ResetCookieToken(params, response)
    }).catch(e => {
        debug.log('PerformInitialSelect', {
            e
        })
        response.status(200).json({
            success: false,
            error: e.error,
            detail: e.detail,
        })
    })
}

router.post('', login.Required, (request, response, next) => {
    const userName = request.body.userName
    const cookieToken = request.body.cookieToken
    if (!userName || !cookieToken) {
        response.status(200).json({
            success: false,
            error: 'MissingArguments',
            detail: ['userName', userName, 'cookieToken', cookieToken],
        })
    } else {
        PerformInitialSelect({
            userId: request.authenticatedUser._id,
            userName,
						cookieToken: request.body.cookieToken,
        }, response)
    }
})

module.exports = router
