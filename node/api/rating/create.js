const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()

const rfr = require('rfr')

const objects = rfr('data/objects')
const login = rfr('middleware/login')
const createRating = rfr('data/rating/create')
const debug = rfr('util/debug')

function CreateRating(params) {
    debug.log({
        params
    })
    // TODO(greg) Should we really block on this? The UI doesn't block on the
    // response anyway, currently.
    createRating.LabelBroadcast(params).then(result => {
        debug.log({
            result
        })
    }).catch(error => {
        debug.error({
            error
        })
    })
}

router.post('', login.Required, (request, response, next) => {
    const params = {
        authenticatedUser: request.authenticatedUser,
        reference: request.body.reference,
        variable: request.body.variable,
        value: request.body.value,
    }
    debug.log({
        params
    })
    const error = debug.CheckArguments(params, ['authenticatedUser', 'reference', 'variable', 'value'])
    debug.log({
        error
    })
    if (error) {
        debug.error({
            error
        })
        response.status(200).json({
            success: false,
            error,
        })
    } else {
        // Return early.
        response.status(200).json({
            success: true,
        })

				// Create the rating.
        CreateRating(params)
    }
})

module.exports = router
