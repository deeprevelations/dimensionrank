const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const objects = rfr('data/objects')
const applicationReceived = rfr('sendgrid/applicationReceived')
const logEvents = rfr('data/log/events')
const debug = rfr('util/debug')

function LogApplicationEvent(params) {
    debug.log({
        params
    })
    logEvents.LogApplication(params).then(response => {
        debug.log({
            response
        })
    }).catch(error => {
        debug.log({
            error
        })
    })
}

//function SendReceivedEmail(params, response) {
//    debug.log({
//        params
//    })
//    applicationReceived.SendTheEmail(params.email, params.fullName).then(result => {
//        debug.log({
//            result
//        })
//        response.status(200).json({
//            success: true,
//        })
//    }).catch(error => {
//        response.status(200).json({
//            success: false,
//        })
//    })
//}

function CreateApplication(params, response) {
    debug.log({
        params
    })
    const application = new objects.Application({
        _id: new mongoose.Types.ObjectId(),
        email: params.email,
        fullName: 'nil',
        profile: 'nil',
        creationTime: Date.now(),
    })
    debug.log({
        application
    })

    application.save().then(result => {
        debug.log({
            result
        })

				// Note: DO NOT send an email.
        // SendReceivedEmail(params, response)

        response.status(200).json({
            success: true,
        })

    }).catch(error => {
        debug.error({
            error
        })
        response.status(200).json({
            success: false,
            error,
        })
    })
}

router.post('', (request, response, next) => {
    const params = {
        email: request.body.email,
    }

    const error = debug.CheckArguments(params, ['email'])

    if (error) {
        response.status(200).json({
            success: false,
            error,
        })
    } else {
        LogApplicationEvent(params, response)
        CreateApplication(params, response)
    }
})

module.exports = router
