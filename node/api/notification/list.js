const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const objects = rfr('data/objects')
const queryBatch = rfr('ranking/query')
const login = rfr('middleware/login')
const debug = rfr('util/debug')

function RouteTheQuery(params, response) {
    debug.log({
        params
    })
    objects.Alert.find({
        consumerId: params.authenticatedUser._id
    }).sort({
        _id: -1
    }).then(alerts => {
        debug.log({
            alerts
        })
        response.status(200).json({
            success: true,
            alerts,
        })
    }).catch(error => {
        response.status(200).json({
            success: false,
            error,
        })
    })
}

router.post('', login.Required, (request, response, next) => {
    debug.log({
        body: request.body
    })
    const params = {
        authenticatedUser: request.authenticatedUser,
        queryRequest: {
            objectType: 'broadcast',
            channelType: 'notification',
            channelName: request.authenticatedUser._id.toString(),
            rankingAlgorithm: 'new',
        },
        batchSize: 50,
    }
    debug.log({
        params
    })
    const error = debug.CheckArguments(params, ['authenticatedUser', 'queryRequest'])
    debug.log({
        error
    })
    if (error) {
        response.status(200).json({
            success: false,
            error,
        })
    } else {
        RouteTheQuery(params, response)
    }
})

module.exports = router
