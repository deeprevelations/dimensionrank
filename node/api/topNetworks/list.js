const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const objects = rfr('data/objects')
const login = rfr('middleware/login')
const debug = rfr('util/debug')
const queryTopNetworks = rfr('ranking/query')

function LoadListings(params, response) {
    debug.log({
        params
    })
    // Note: Currently this takes no arguments. I.e., "popular" assumes we are
    // restricted to "networks".
    queryTopNetworks.QueryHandler(params).then(listings => {
        debug.log({
            listings
        })
        response.status(200).json({
            success: true,
            ...listings,
        })
    }).catch(error => {
        debug.error({
            error
        })
        response.status(200).json({
            success: false,
            error,
        })
    })
}

router.post('', login.Required, (request, response, next) => {
    const queryRequest = {
        objectType: 'network',
        rankingAlgorithm: 'popular',
        channelType: 'overall',
    }
    LoadListings({
        queryRequest,
				authenticatedUser: request.authenticatedUser,
    }, response)
})

module.exports = router
