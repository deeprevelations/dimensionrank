const express = require("express")
const router = express.Router()
const rfr = require('rfr')

const objects = rfr("data/objects")
const clearCookieToken = rfr('data/cookieToken/clear')
const findCookieToken = rfr('data/cookieToken/find')
const debug = rfr('util/debug')

function DeleteCookieToken(cookieToken, response) {
    debug.log('DeleteCookieToken', {
        cookieToken
    })
    clearCookieToken.ClearCookieToken(cookieToken).then(result => {
        debug.log('DeleteCookieToken', {
            result
        })
        response.status(200).json({
            success: true
        })
    }).catch(error => {
        response.status(200).json({
            success: false,
            error,
        })
    })
}

function CheckCookieToken(request, response) {
    if (request.body && request.body.cookieToken) {
        const cookieToken = request.body.cookieToken
        debug.log('CheckCookieToken', {
            cookieToken
        })
        findCookieToken.FindCookieTokenUser(cookieToken).then(userId => {
            if (cookieToken) {
                DeleteCookieToken(cookieToken, response)
            } else {
                response.status(200).json({
                    success: false,
                    error: "cookieToken not found",
                })
            }
        }).catch(error => {
            response.status(200).json({
                success: false,
                error,
            })
        })
    } else {
        debug.log('CheckCookieToken', 'request.userId not found')
        response.status(200).json({
            success: false,
            error: "Arguments missing",
        })
    }
}

router.post("", (request, response, next) => {
    CheckCookieToken(request, response)
})

module.exports = router
