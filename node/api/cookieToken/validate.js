const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const objects = rfr('data/objects')
const findCookieToken = rfr('data/cookieToken/find')
const debug = rfr('util/debug')

function DoValidateCookieToken(params, response) {
    debug.log('DoValidateCookieToken', {
        params
    })
    findCookieToken.FindCookieTokenUser(params.cookieToken).then(authenticatedUser => {
        debug.log('DoValidateCookieToken', {
            authenticatedUser
        })
        response.status(200).json({
            success: true,
            authenticatedUser,
        })
    }).catch(error => {
        debug.log('DoValidateCookieToken', {
            error
        })
        response.status(200).json({
            success: false,
            error,
        })
    })
}

router.post('', (request, response, next) => {
    const cookieToken = request.body.cookieToken
    if (!cookieToken) {
        response.status(200).json({
            success: false,
            error: 'MissingArguments',
            detail: ['cookieToken', cookieToken],
        })
    } else {
        DoValidateCookieToken({
            cookieToken
        }, response)
    }
})

module.exports = router
