const express = require('express')
const router = express.Router()

const rfr = require('rfr')

const objects = rfr('data/objects')
const debug = rfr('util/debug')

function DoReadImage(request, response) {
    debug.log('DoReadImage', request.body)
    const params = request.params
    try {
        const user = objects.Image.findOne({
            _id: params.imageId
        })
        debug.log('DoListInvitations', user)
        response.status(200).json({
            user,
        })
    } catch (error) {
        response.status(500).json({
            error,
        })
    }
}

// TODO(gcoppola) Can login.Required run in sequence here?
router.post('', upload.single('uploadImage'), (request, response, next) => {
    DoReadImage(request, response)
})

module.exports = router
