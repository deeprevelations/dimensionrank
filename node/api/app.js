const express = require('express')
const app = express()
const BodyParser = require('body-parser')
const cors = require('cors')

const rfr = require('rfr')
const debug = rfr('util/debug')


app.use(BodyParser.urlencoded({
    extended: false
}))
app.use(BodyParser.json())
app.use(cors())

app.use('/api/application/create', require('./application/create'))
app.use('/api/broadcast/original', require('./broadcast/original'))
app.use('/api/broadcast/derived', require('./broadcast/derived'))
app.use('/api/broadcast/delete', require('./broadcast/delete'))
app.use('/api/broadcast/read', require('./broadcast/read'))
// app.use('/api/cookieToken/create', require('./cookieToken/create'))
app.use('/api/cookieToken/delete', require('./cookieToken/delete'))
app.use('/api/cookieToken/validate', require('./cookieToken/validate'))
app.use('/api/emailToken/create', require('./emailToken/create'))
app.use('/api/emailToken/validate', require('./emailToken/validate'))
app.use('/api/query/batch', require('./query/batch'))
app.use('/api/query/comments', require('./query/comments'))
app.use('/api/rating/create', require('./rating/create'))
app.use('/api/topNetworks/list', require('./topNetworks/list'))
app.use('/api/notification/list', require('./notification/list'))
app.use('/api/resource/get', require('./resource/get'))
app.use('/api/user/create', require('./user/create'))
app.use('/api/user/confirm', require('./user/confirm'))
app.use('/api/user/login', require('./user/login'))
app.use('/api/user/readSelf', require('./user/readSelf'))
app.use('/api/user/userName/initial', require('./user/userName/initialSelect'))
app.use('/api/user/history', require('./user/history'))


// Error handling
app.use((request, response, next) => {
    const error = new Error('Not found')
    error.status = 404
    debug.error({
        error,
				request,
    })
    next(error)
})

app.use((error, request, response, next) => {
    response.status(error.status || 500)
    response.json({
        error: {
            message: error.message
        }
    })
})

module.exports = app
