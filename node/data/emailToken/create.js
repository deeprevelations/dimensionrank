const mongoose = require("mongoose")
const cheerio = require('cheerio')
const getUrls = require('get-urls')
const fetch = require('node-fetch')
const rfr = require('rfr')

const objects = rfr('data/objects')
const debug = rfr('util/debug')

function DoCreateToken(params, output) {
    debug.log('DoCreateToken', {
        params
    })
    const emailToken = new objects.EmailToken({
        _id: new mongoose.Types.ObjectId(),
        email,
    })
    return emailToken.save().then(function(result) {
        output.resolve(result)
    }).catch(function(error) {
        output.reject(error)
    })
}

function CreateEmailToken(email) {
    return new Promise(function(resolve, reject) {
        DoCreateToken({
            email
        }, {
            resolve,
            reject
        })
    })
}

module.exports = {
    CreateEmailToken,
}
