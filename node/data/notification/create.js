//const getUrls = require('get-urls')
//const rfr = require('rfr')
//const TimeUuid = require('cassandra-driver').types.TimeUuid
//
//const objects = rfr('data/objects')
//const debug = rfr('util/debug')
//const getResource = rfr('data/resource/get')
//const config = rfr('data/config')
//const broadcastCommon = rfr('data/broadcast/common')
//const tables = rfr('data/tables')
//
//function CreateNotificationImpl(params, output) {
//    debug.log('CreateNotificationImpl', {
//        params
//    })
//    const referenceBroadcast = params.referenceBroadcast
//    const derivedBroadcast = params.derivedBroadcast
//    tables.notification_for_user.Insert({
//        userid: referenceBroadcast.producer,
//        broadcastid: referenceBroadcast._id,
//    }).then(insertResult => {
//        debug.log({
//            insertResult
//        })
//    }).catch(error => {
//        debug.log({
//            error
//        })
//    })
//}
//
//function LookUpReference(params, output) {
//    debug.log('LookUpReference', {
//        params
//    })
//
//    objects.Broadcast.findOne(params.derivedBroadcast.reference).then(referenceBroadcast => {
//        CreateNotificationImpl({
//            ...params,
//            referenceBroadcast,
//        }, output)
//    }).catch(error => {
//        output.reject(error)
//    })
//}
//
//// The notification is based on a 'broadcast'.
//function CreateNotification(params) {
//    debug.log('CreateNotification', {
//        params
//    })
//    return new Promise(function(resolve, reject) {
//        const error = debug.CheckArguments(params, ['producerName', 'derivedBroadcast'])
//        debug.log('CreateNotification', {
//            error
//        })
//        if (error) {
//            reject(error)
//        } else if (!params.derivedBroadcast.reference) {
//            // Also check if we have a reference.
//            output.reject({
//                error: 'InvalidArgument',
//                details: ['no-reference', 'derivedBroadcast', JSON.stringify(params.derivedBroadcast)]
//            })
//        } else {
//            LookUpReference(
//                params, {
//                    resolve,
//                    reject,
//                })
//        }
//    })
//}
//
//module.exports = {
//    CreateNotification,
//}
