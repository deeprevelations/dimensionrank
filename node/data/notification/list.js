const getUrls = require('get-urls')
const rfr = require('rfr')
const TimeUuid = require('cassandra-driver').types.TimeUuid

const objects = rfr('data/objects')
const debug = rfr('util/debug')
const getResource = rfr('data/resource/get')
const config = rfr('data/config')
const broadcastCommon = rfr('data/broadcast/common')


function ListNotifications(params) {
    debug.log('ListNotifications', {
        params
    })
    return new Promise(function(resolve, reject) {
        const error = debug.CheckArguments(params, ['authenticatedUser'])
        if (error) {
            reject(error)
        } else {
            ListNotificationsImpl(params, {
                resolve,
                reject,
            })
        }
    })
}

module.exports = {
    ListNotifications,
}
