const rfr = require('rfr')
const cassandraData = rfr('data/cassandra')
const debug = rfr('util/debug')

const broadcast_for_user = new cassandraData.CassandraTable('broadcast_for_user', ['producername', 'broadcastid'])
//const broadcasts_for_network = new cassandraData.CassandraTable('broadcasts_for_network', ['channelname', 'broadcastid'])
//const comments_for_broadcast = new cassandraData.CassandraTable('comments_for_broadcast', ['channelname', 'broadcastid'])
//const broadcasts_for_toxic = new cassandraData.CassandraTable('broadcasts_for_toxic', ['channelname', 'broadcastid'])
const notification_for_user = new cassandraData.CassandraTable('notification_for_user', ['consumerid', 'broadcastid'])
const rating_event = new cassandraData.CassandraTable('rating_event', ['id', 'raterid', 'producerid', 'reference', 'variable', 'value'])

function ProjectInternal(cassandraResult, fieldname) {
    var buffer = []
    for (var i = 0; i < cassandraResult.rowLength; ++i) {
        const row = cassandraResult.rows[i]
        buffer.push(row[fieldname])
    }
    return buffer
}

// Takes 'cassandraPromise', and runs it to get a result, a list of Cassandra
// rows, wich is and returns a list of single elements, representing the values
// for row key 'fieldname'.
function MongoIdsFromCassandraResult(cassandraPromise, fieldname) {
    debug.log({
        cassandraPromise,
        fieldname,
    })
    return new Promise(function(resolve, reject) {
        cassandraPromise.then(cassandraResult => {
            debug.log({
                cassandraResult
            })
            const mongoIds = ProjectInternal(cassandraResult, fieldname)
            debug.log({
                mongoIds
            })
            resolve({
                mongoIds
            })
        }).catch(error => {
            reject(error)
        })
    })
}

function NotificationsFromCassandra(cassandraPromise) {
    debug.log({
        cassandraPromise
    })
    return new Promise(function(resolve, reject) {
        cassandraPromise.then(cassandraResult => {
            debug.log({
                cassandraResult
            })
            var notifications = []
            for (var i = 0; i < cassandraResult.rowLength; ++i) {
                //                const json = JSON.parse(cassandraResult.rows[i].rating)
                //                debug.log({
                //                    json
                //                })
                var row = cassandraResult.rows[i]
                if (row.rating) {
                    row.rating = JSON.parse(row.rating)
                }
                notifications.push(row)
            }
            resolve({
                notifications
            })
        }).catch(error => {
            reject(error)
        })
    })
}

module.exports = {
    broadcast_for_user,
    notification_for_user,
    rating_event,
    MongoIdsFromCassandraResult,
    NotificationsFromCassandra,
}
