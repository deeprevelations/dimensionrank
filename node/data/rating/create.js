const assert = require('assert')
const mongoose = require('mongoose')
const redis = require('redis')
const rfr = require('rfr')
const TimeUuid = require('cassandra-driver').types.TimeUuid

const dataRedis = rfr('data/redis')
const debug = rfr('util/debug')
const objects = rfr('data/objects')
const enums = rfr('data/enums')
const tables = rfr('data/tables')
const routerUpdate = rfr('ranking/update')

function AddForTraining(params) {
    dataRedis.client().lpush('train_queue', JSON.stringify(params.alert_), function(error) {
        debug.error({
            error
        })
    })
}

function AddAlert(params) {
    debug.log({
        params
    })
    const variable = enums.ReducedVariableType(params.variable)
    const value = enums.ReducedValueType(parseInt(params.value))
    const alert_ = new objects.Alert({
        _id: new mongoose.Types.ObjectId(),
        consumerId: params.authenticatedUser._id,
        producerId: params.referenceBroadcast.producerId,
        producerName: params.referenceBroadcast.producerName,
        reference: params.referenceBroadcast._id,
        variable,
        value,
    })
    debug.log({
        alert_
    })

    alert_.save().then(result => {
        debug.log({
            result
        })

        AddForTraining({
            ...params,
            alert_,
        })

        // Already returned.
    }).catch(error => {
        debug.error({
            error
        })
        // Already returned.
    })
}

function AddRatingToRankings(params, output) {
    debug.log({
        params
    })
    routerUpdate.UpdateHandler({
            updateType: enums.ActionMapping.rating,
            rating: params.rating,
            producerTuple: params.authenticatedUser,
            referenceBroadcast: params.referenceBroadcast, // Note: not used yet but should be
        })
        .then(result => {
            debug.log({
                result
            })

            // Return early.
            output.resolve(params.rating)

            AddAlert(params)
        }).catch(error => {
            debug.error({
                error
            })
            output.reject(error)
        })
}

// Write a rating_event to the cassandra path.
function CreateRatingEvent(params, output) {
    debug.log({
        params
    })

    const variable = enums.ReducedVariableType(params.variable)
    const value = enums.ReducedValueType(parseInt(params.value))
    debug.log({
        variable,
        value,
    })

    //    const rating_event = {
    //        id: TimeUuid.now(),
    //        raterid: params.authenticatedUser._id,
    //        producerid: params.referenceBroadcast.producerId,
    //        reference: params.referenceBroadcast.id,
    //        variable,
    //        value,
    //    }
    //    tables.rating_event.Insert(rating_event).then(saveResult => {

    const rating = new objects.Rating({
        _id: new mongoose.Types.ObjectId(),
        consumerId: params.authenticatedUser._id,
        producerId: params.referenceBroadcast.producerId,
        reference: params.referenceBroadcast.id,
        variable,
        value,
    })

    rating.save().then(saveResult => {
            debug.log({
                saveResult
            })
            AddRatingToRankings({
                ...params,
                rating,
            }, output)
        })
        .catch(error => {
            debug.error({
                error
            })
            output.reject(error)
        })
}

// Look-up the reference, in every case.
function LookupReference(params, output) {
    debug.log({
        params
    })
    const oid = new mongoose.Types.ObjectId(params.reference)
    objects.Broadcast.findOne(oid)
        .then(referenceBroadcast => {
            debug.log({
                referenceBroadcast
            })
            if (referenceBroadcast) {
                CreateRatingEvent({
                    ...params,
                    referenceBroadcast,
                }, output)
            } else {
                output.reject({
                    error: 'ObjectNotFound',
                    details: ['reference', params.reference],
                })
            }
        })
        .catch(error => {
            debug.error({
                error
            })
            output.reject(error)
        })
}

function LabelBroadcast(params) {
    debug.log({
        params
    })
    return new Promise(function(resolve, reject) {
        const error = debug.CheckArguments(params, ['authenticatedUser', 'reference', 'variable', 'value'])
        if (error) {
            reject(error)
        } else {
            LookupReference(params, {
                resolve,
                reject,
            })
        }
    })
}

module.exports = {
    LabelBroadcast,
}
