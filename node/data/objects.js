const mongoose = require('mongoose')

const PossibleUserSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    // Note: Email is the only thing currently required to create a user.
    // They can choose a unique userName later. Until then, their name is ''.
    email: {
        type: String,
        required: true
    },
    userName: {
        type: String,
        required: true
    },
    passwordHash: {
        type: String,
        required: true
    },
    confirmed: {
        type: Boolean,
        required: true
    },
})
const PossibleUser = mongoose.model('PossibleUser', PossibleUserSchema)

const UserSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    // Note: Email is the only thing currently required to create a user.
    // They can choose a unique userName later. Until then, their name is ''.
    email: {
        type: String,
        required: true
    },
    userName: {
        type: String,
        required: true
    },
    passwordHash: {
        type: String,
        required: true
    },
    // s/description/longName/ TODO(greg) change?
    description: {
        type: String,
        default: ''
    },
    profile: {
        type: String,
        default: ''
    },
    pictureFileName: {
        type: String,
        default: ''
    },
})
const User = mongoose.model('User', UserSchema)

const InvitationSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    candidateEmail: {
        type: String,
        required: true
    },
    sponsorId: {
        type: String,
        required: true
    },
    creationTime: {
        type: Number,
        required: true
    },
})
const Invitation = mongoose.model('Invitation', InvitationSchema)

const ImageSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    userId: {
        type: String,
        required: true
    },
    fileInfo: {
        type: String,
        required: true
    },
    creationTime: {
        type: Number,
        required: true
    },
})
const Image = mongoose.model('Image', ImageSchema)

const EmailTokenSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    email: {
        type: String,
        required: true
    },
})
const EmailToken = mongoose.model('EmailToken', EmailTokenSchema)

const CookieTokenSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    userId: {
        type: String,
        required: true
    },
})
const CookieToken = mongoose.model('CookieToken', CookieTokenSchema)

// Note: This is also like 'email list'.
const EmailAuthorizationSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    email: {
        type: String,
        required: true
    },
    emailHistory: {
        type: [String],
        required: true
    },
    lists: {
        type: [String],
        required: true
    },
})
const EmailAuthorization = mongoose.model('EmailAuthorization', EmailAuthorizationSchema)

const ResourceSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    url: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: false
    },
    favicon: {
        type: String,
        required: false
    },
    description: {
        type: String,
        required: false
    },
    image: {
        type: String,
        required: false
    },
    author: {
        type: String,
        required: false
    },
})
const Resource = mongoose.model('Resource', ResourceSchema)

const ApplicationSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    fullName: {
        type: String,
        required: false,
    },
    email: {
        type: String,
        required: true,
    },
    profile: {
        type: String,
        required: false,
    },
    creationTime: {
        type: Number,
        required: true,
    },
    invitationTime: {
        type: Number,
        required: false,
    },
})
const Application = mongoose.model('Application', ApplicationSchema)

const RatingBundleSchema = mongoose.Schema({
    userName: {
        type: String,
        required: true,
    },
    variable: {
        type: Number,
        required: false,
    },
    value: {
        type: Number,
        required: false,
    },
})

const BroadcastSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    producerId: {
        type: String,
        required: true,
    },
    producerName: {
        type: String,
        required: true,
    },
    reference: {
        type: String,
        required: false,
    },
    deleted: {
        type: Boolean,
        default: false,
    },
    ratings: [RatingBundleSchema],
    attachment: {
        type: mongoose.Schema.Types.Mixed,
        required: true,
    },
    details: {
        type: String,
        required: true,
    },
    task_embedding0: {
        type: String,
        required: false,
    },
    task_embedding1: {
        type: String,
        required: false,
    },
})
const Broadcast = mongoose.model('Broadcast', BroadcastSchema)

const AlertSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    consumerId: {
        type: String,
        required: true,
    },
    producerId: {
        type: String,
        required: true,
    },
    producerName: {
        type: String,
        required: true,
    },
    reference: {
        type: String,
        required: false,
    },
		// Either put a reply, or a rating.
    comment: {
        type: String,
        required: false,
    },
    variable: {
        type: Number,
        required: false,
    },
    value: {
        type: Number,
        required: false,
    },
})
const Alert = mongoose.model('Alert', AlertSchema)

const RatingSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    consumerId: {
        type: String,
        required: true,
    },
    producerId: {
        type: String,
        required: true,
    },
    reference: {
        type: String,
        required: true,
    },
    variable: {
        type: Number,
        required: false,
    },
    value: {
        type: Number,
        required: false,
    },
})
const Rating = mongoose.model('Rating', RatingSchema)

module.exports = {
    Application,
    Broadcast,
    CookieToken,
    EmailAuthorization,
    EmailToken,
    Image,
    Invitation,
    Resource,
    User,
		PossibleUser,
		Alert,
		Rating,
}
