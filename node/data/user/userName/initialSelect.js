const express = require('express')
const router = express.Router()
const rfr = require('rfr')

const debug = rfr('util/debug')
const login = rfr('middleware/login')
const objects = rfr('data/objects')
const initialSelect = rfr('data/user/userName/initialSelect')

// 1. check if userId has a userName yet
// 2. check if userName is taken yet
// 3. set userName to be the name for userId
function ActuallySetUserName(params, output) {
    objects.User.updateOne({
        _id: params.userId
    }, {
        $set: {
            userName: params.userName
        }
    }).then(updateResult => {
        output.resolve(updateResult)
    }).catch(error => {
        output.reject(error)
    })
}

function CheckUserNameAvailable(params, output) {
    objects.User.findOne({
        email: params.email
    }).then(userResult => {
        if (userResult) {
            output.reject({
                error: 'UserNameAlreadyTaken',
                details: ['userId', params.userId, 'userName', userResult.userName],
            })
        } else {
            ActuallySetUserName(params, output)
        }
    }).catch(error => {
        output.reject(error)
    })
}

// For this version, you cannot have a user name set yet.
function CheckUserNameUnset(params, output) {
    objects.User.findById(params.userId).then(userResult => {
        if (!userResult) {
            output.reject({
                error: 'ObjectNotFound',
                details: ['userId', params.userId],
            })
        } else {
            if (userResult.userName.length > 0) {
                output.reject({
                    error: 'UserAlreadyHasName',
                    details: ['userId', params.userId, 'userResult.userName', userResult.userName],
                })
            } else {
                CheckUserNameAvailable(params, output)
            }
        }
    }).catch(error => {
        output.reject(error)
    })
}

// On success, returns the underlying Mongo update result.
// Errors: UserAlreadyHasName, UserNameAlreadyTaken
function InitialSelectUserName(userId, userName) {
    return new Promise(function(resolve, reject) {
        CheckUserNameUnset({
            userId,
            userName,
        }, {
            resolve,
            reject,
        })
    })
}

module.exports = {
    InitialSelectUserName,
}
