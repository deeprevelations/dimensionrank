const mongoose = require('mongoose')
const rfr = require('rfr')

const queryNew = rfr('ranking/query')
const debug = rfr('util/debug')

function LoadUserHistoryImpl(params, output) {
    debug.log({
        params
    })

    const queryParams = {
        queryRequest: {
            objectType: 'broadcast',
            channelType: 'user',
            channelName: params.authenticatedUser.userName,
        },
        authenticatedUser: params.authenticatedUser,
    }
    debug.log({
        queryParams
    })
    queryNew.QueryHandler(queryParams).then(result => {
        output.resolve(result)
    }).catch(error => {
        output.reject(error)
    })
}

function LoadUserHistory(params) {
    debug.log({
        params
    })
    return new Promise(function(resolve, reject) {
        const error = debug.CheckArguments(params, ['authenticatedUser'])
        if (error) {
            reject(error)
        }

        LoadUserHistoryImpl(params, {
            resolve,
            reject
        })
    })
}

module.exports = {
    LoadUserHistory,
}
