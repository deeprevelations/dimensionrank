const mongoose = require('mongoose')
const rfr = require('rfr')
const bcrypt = require('bcrypt')
const random = rfr('util/random')
const globalOptions = rfr('globalOptions')

const debug = rfr('util/debug')
const objects = rfr('data/objects')
const cookieToken = rfr('data/cookieToken/reset')
const sendEmailToken = rfr('sendgrid/emailToken')
const logEvents = rfr('data/log/events')

function LogValidationAttempt(params, response) {
    debug.log({
        params
    })
    logEvents.LogValidationAttempt(params).then(logResponse => {
        debug.log({
            logResponse
        })
    }).catch(error => {
        debug.log({
            error
        })
    })
}

function MarkConfirmed(params, output) {
    debug.log({
        params
    })
    const userName = params.possibleUser.userName
    const query = {
        _id: new mongoose.Types.ObjectId(params.token)
    }
    const update = {
        $set: {
            confirmed: true
        }
    }
    debug.log({
        query,
        update,
    })
    objects.PossibleUser.update(query, update).then(updateResult => {
        debug.log({
            updateResult
        })
    }).catch(error => {
        debug.error({
            error
        })
        output.reject(error)
    })
}

function TransferUser(params, output) {
    debug.log({
        params
    })
    const possibleUser = params.possibleUser
    const user = new objects.User({
        _id: new mongoose.Types.ObjectId(),
        userName: possibleUser.userName,
        email: possibleUser.email,
        passwordHash: possibleUser.passwordHash,
    })
    user.save().then(result => {
        debug.log({
            user,
        })
        output.resolve({
            user
        })

        // asynchronous
        MarkConfirmed(params, output)
    }).catch(error => {
        debug.error({
            error
        })
        output.reject(error)
    })
}

function EnsureUserNameStillFree(params, output) {
    debug.log({
        params
    })
    const userName = params.possibleUser.userName
    objects.User.findOne({
        userName
    }).then(possibleUser => {
        if (possibleUser) {
            output.reject({
                error: 'UserNameNotAvailable',
                details: ['userName', userName],
            })
        } else {
            TransferUser(params, output)
        }
    }).catch(error => {
        debug.error({
            error
        })
        output.reject(error)
    })
}

function EnsureEmailStillFree(params, output) {
    debug.log({
        params
    })
    const email = params.possibleUser.email
    objects.User.findOne({
        email
    }).then(possibleUser => {
        debug.log({
            possibleUser
        })
        if (possibleUser) {
            output.reject({
                error: 'EmailNotAvailable',
                details: ['email', email],
            })
        } else {
            EnsureUserNameStillFree(params, output)
        }
    }).catch(error => {
        debug.error({
            error
        })
        output.reject(error)
    })
}

function LoadToken(params, output) {
    debug.log({
        params
    })
    objects.PossibleUser.findById(params.token).then(possibleUser => {
        debug.log({
            possibleUser
        })
        if (possibleUser) {
            if (possibleUser.confirmed) {
                output.reject({
                    error: 'AlreadyConfirmed',
                    details: ['token', params.token],
                })
            } else {
                EnsureEmailStillFree({
                    ...params,
                    possibleUser
                }, output)
            }
        } else {
            output.reject({
                error: 'TokenNotFound',
                details: ['token', params.token],
            })
        }
    }).catch(error => {
        debug.error({
            error
        })
        output.reject(error)
    })
}

// Returns a Promise to the Mongo creation result.
function ConfirmUser(params) {
    debug.log({
        params
    })
    return new Promise(function(resolve, reject) {
        LogValidationAttempt({
            token: params.token ? params.token : 'empty',
        })

        const error = debug.CheckArguments(params, ['token'])
        debug.log({
            error
        })
        if (error) {
            debug.error({
                error
            })
            reject(error)
        } else {
            LoadToken(params, {
                resolve,
                reject
            })
        }
    })
}


module.exports = {
    ConfirmUser,
}
