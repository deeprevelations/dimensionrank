const mongoose = require('mongoose')
const rfr = require('rfr')
const bcrypt = require('bcrypt')
const random = rfr('util/random')
const globalOptions = rfr('globalOptions')

const debug = rfr('util/debug')
const objects = rfr('data/objects')
const cookieToken = rfr('data/cookieToken/reset')
const sendEmailToken = rfr('sendgrid/emailToken')
const logEvents = rfr('data/log/events')

function LogLoginAttempt(params, response) {
    debug.log({
        params
    })
    logEvents.LogLoginAttempt(params).then(logResponse => {
        debug.log({
            logResponse
        })
    }).catch(error => {
        debug.log({
            error
        })
    })
}

function SendTheEmail(params, output) {
    debug.log({
        params
    })
    sendEmailToken.SendTheEmail(params).then(result => {
        // Tell the user to check their email.
        output.resolve({
            email: params.email,
            userName: params.userName,
        })
    }).catch(error => {
        debug.log({
            error
        })
        output.reject(error)
    })
}

function CreateTheUser(params, output) {
    debug.log({
        params
    })
    const passwordHash = bcrypt.hashSync(params.password, 10)
    const user = new objects.PossibleUser({
        _id: new mongoose.Types.ObjectId(),
        userName: params.userName,
        email: params.email,
        passwordHash,
        confirmed: false,
    })
    debug.log({
        user
    })
    user.save().then(result => {
        debug.log({
            result,
            email: params.email,
        })

        const url = globalOptions.options().vueBasePath + '/user/confirm/' + user._id.toString()
        SendTheEmail({
            ...params,
            url,
        }, output)
    }).catch(error => {
        debug.error({
            error
        })
        output.reject(error)
    })
}

function CheckUserNameFree(params, output) {
    debug.log({
        params
    })
    objects.User.findOne({
        userName: params.userName
    }).then(result => {
        if (result) {
            output.reject({
                error: 'UserNameTaken',
                details: ['userName', params.userName]
            })
        } else {
            CreateTheUser(params, output)
        }
    }).catch(error => {
        debug.error({
            error
        })
        output.reject(error)
    })
}

function ValidUserName(email) {
    debug.log({
        email
    })
    var re = /^([a-zA-Z\_0-9]+)$/;
    return re.test(String(email));
}

function CheckUserNameValid(params, output) {
    debug.log({
        params
    })
    const validUserName = ValidUserName(params.userName)
    if (!validUserName) {
        debug.error({
            validUserName,
            email: params.email
        })
        output.reject({
            error: 'InvalidUserName',
            details: ['userName', params.userName]
        })
    } else {
        CheckUserNameFree(params, output)
    }
}

function CheckEmailFree(params, output) {
    debug.log({
        params
    })
    const query = {
        email: params.email
    }
    debug.log({
        query
    })
    debug.log({
        user: objects.User.findOne
    })
    objects.User.findOne(query).then(result => {
        console.log({})
        console.log({
            result
        })
        debug.log({
            result
        })
        if (result) {
            output.reject({
                error: 'EmailTaken',
                details: ['email', params.email]
            })
        } else {
            CheckUserNameValid(params, output)
        }
    }).catch(error => {
        console.error({})
        console.error({
            error
        })
        debug.error({
            error
        })
        output.reject(error)
    })
}

function ValidEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email)
}

function CheckEmailValid(params, output) {
    debug.log({
        params
    })
    const validEmail = ValidEmail(params.email)
    if (!validEmail) {
        debug.error({
            validEmail,
            email: params.email
        })
        output.reject({
            error: 'InvalidEmail',
            details: ['email', params.email]
        })
    } else {
        CheckEmailFree(params, output)
    }
}

// Returns a Promise to the Mongo creation result.
function CreateUser(params) {
    debug.log({
        params
    })
    return new Promise(function(resolve, reject) {
        const error = debug.CheckArguments(params, ['userName', 'email', 'password'])
        debug.error({
            params
        })
        if (error) {
            reject(error)
        }

        LogLoginAttempt({
            email: params.email,
        })

        CheckEmailValid(params, {
            resolve,
            reject
        })
    })
}


module.exports = {
    CreateUser,
}