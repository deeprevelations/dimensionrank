const mongoose = require('mongoose')
const rfr = require('rfr')
const debug = rfr('util/debug')

function ResetMongoClient(options) {
    debug.log('ResetMongoClient', {
        options: 'hidden'
    })
    mongoose.connect(options.uri, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
}

module.exports = {
    ResetMongoClient,
}
