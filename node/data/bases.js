const mongoose = require('mongoose')
const fs = require('fs')
const rfr = require('rfr')
const globalOptions = rfr('globalOptions')
const dataCassandra = rfr('data/cassandra')
const dataMongo = rfr('data/mongo')
const dataRedis = rfr('data/redis')
const debug = rfr('util/debug')
const timeKey = rfr('util/timeKey')

function LoadOptions(optionsFname) {
    const fileText = fs.readFileSync(optionsFname)
    return JSON.parse(fileText)
}

// 'serviceList' is a list of strings
async function InitFromFile(optionsFname, serviceList) {
    debug.log('InitFromFile', {
        optionsFname
    }, {
        serviceList
    })
    const options = LoadOptions(optionsFname)
    debug.log('InitFromFile', {
        options: 'hidden'
    })

    const loadResult = globalOptions.LoadOptions(options)
    debug.log('Main', {
        loadResult
    })

    if (serviceList.includes('redis')) {
        debug.log('wants redis')
        const redisResult = await dataRedis.ResetRedisClient(options.redis)
        debug.log('Main', {
            redisResult,
        })
    }
    if (serviceList.includes('mongo')) {
        debug.log('wants mongo')
        const mongoResult = await dataMongo.ResetMongoClient(options.mongo)
        debug.log('Main', {
            mongoResult,
        })
    }
    if (serviceList.includes('cassandra')) {
        debug.log('wants cassandra')
        const cassandraResult = await dataCassandra.ResetCassandraClient(options.cassandra)
        debug.log('Main', {
            cassandraResult,
        })
    }
}

// Require: 1) contains 'development', 2) does not contain 'production'
function EnsureDevelopmentFileName(optionsFname) {
    debug.warn({
        optionsFname
    })
    const development = optionsFname.includes('development')
    const production = optionsFname.includes('production')
    debug.warn({
        development,
        production,
    })
    if (production || !development) {
        return false
    } else if (!production && development) {
        return true
    } else {
        return false
    }
}

function ValidOverride(productionDataOverride) {
    debug.log('Main', {
        keyOk
    })
}

async function InitDevelopmentOnly(optionsFname, serviceList, productionDataOverride) {
    const isDevelopment = EnsureDevelopmentFileName(optionsFname)
    const validOverride = timeKey.CheckTimeKeyIsValid(productionDataOverride)
    if (!isDevelopment && !validOverride) {
        throw 'Must be a development file, or have an productionDataOverride.'
    } else {
        await InitFromFile(optionsFname, serviceList)
    }
}

module.exports = {
    InitFromFile,
    InitDevelopmentOnly,
}
