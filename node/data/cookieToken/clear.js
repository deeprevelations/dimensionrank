const rfr = require('rfr')
const dataRedis = rfr('data/redis')
const debug = rfr('util/debug')

function ClearCookieToken(cookieToken) {
    debug.log('ClearCookieToken', 'cookieToken', cookieToken)
    return new Promise(function(resolve, reject) {
        debug.log('ClearCookieToken', 'promise')
        dataRedis.client().hdel("mapCookieToken", cookieToken, function(error, result) {
            debug.log('ClearCookieToken', 'error', error)
            if (error) {
                reject(error)
            } else {
                resolve(result)
            }
        })
    })
}

module.exports = {
    ClearCookieToken,
}
