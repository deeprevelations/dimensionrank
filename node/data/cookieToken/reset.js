// This file contains the *utility* functions for accessing the in-memory
// Redis table 'mapCookieToken'.
//
// This is NOT the authentication middleware. This will be called by the
// middleware.
const rfr = require('rfr')
const dataRedis = rfr('data/redis')
const random = rfr('util/random')
const debug = rfr('util/debug')

function CookieTokenUserRepresentation(user) {
    debug.log('CookieTokenUserRepresentation', {
        user
    })
    if (!user) {
        return null
    }
    if (!user._id) {
        return null
    }
    return {
        _id: user._id.toString(),
        userName: user.userName, // can be '' if not set
    }
}

// Returns a Promise.
function ResetCookieToken(cookieToken, user) {
    debug.log('ResetCookieToken', {
        cookieToken,
        user,
    })
    const redactedUser = CookieTokenUserRepresentation(user)
    debug.log('ResetCookieToken', {
        redactedUser
    })

    return new Promise(function(resolve, reject) {
        dataRedis.client().hset('mapCookieToken', cookieToken, JSON.stringify(redactedUser), err => {
            debug.log('ResetCookieToken', {
                err
            })
            if (err) {
                reject(err)
            } else {
                resolve({
                    cookieToken,
                    authenticatedUser: redactedUser,
                })
            }
        })
    })
}

function ResetRandomCookieToken(user) {
    const cookieToken = random.RandomAlphaNumeric(32)
    debug.log('ResetCookieToken', {
        cookieToken
    })
    return ResetCookieToken(cookieToken, user)
}

module.exports = {
    ResetRandomCookieToken,
    ResetCookieToken,
}
