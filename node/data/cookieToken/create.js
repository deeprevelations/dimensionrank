const mongoose = require('mongoose')
const rfr = require('rfr')
const bcrypt = require('bcrypt')

const random = rfr('util/random')
const debug = rfr('util/debug')
const objects = rfr('data/objects')
const resetToken = rfr('data/cookieToken/reset')

function FinishToken(params, output) {
    debug.log({
        params
    })

    resetToken.ResetRandomCookieToken(params.user).then(tokenPair => {
        debug.log({
            tokenPair
        })
        output.resolve(tokenPair)
    }).catch(error => {
        debug.error({
            error
        })
        output.reject(error)
    })

}

function CheckPassword(params, output) {
const passwordHash = params.user.passwordHash
    debug.log({
        params,
        user: params.user,
        password: params.password,
        passwordHash,
    })
    const equals = bcrypt.compareSync(params.password, params.user.passwordHash)
    debug.log({
        equals
    })
    if (equals) {
        FinishToken(params, output)
    } else {
        const error = {
            error: 'PasswordIncorrect',
            details: ['userPart', params.userPart],
        }
        debug.error({
            error
        })
        output.reject(error)
    }
}

function LookupUser(params, output) {
    debug.log({
        params
    })

    var query = null
    if (params.userPart.indexOf('@') >= 0) {
        query = {
            email: params.userPart
        }
    } else {
        query = {
            userName: params.userPart
        }
    }
    objects.User.findOne(query).then(user => {
        debug.log({
            user
        })
        if (user) {
            CheckPassword({
                ...params,
                user,
            }, output)
        } else {
            const error = {
                error: 'UserNotFound',
                details: ['userPart', params.userPart],
            }
            debug.error({
                error
            })
            output.reject(error)
        }
    }).catch(error => {
        debug.error({
            error
        })
        output.reject(error)
    })
}

// Returns a Promise to the Mongo creation result.
function CreateCookieToken(params) {
    debug.log({
        params
    })
    return new Promise(function(resolve, reject) {
        const error = debug.CheckArguments(params, ['userPart', 'password'])
        debug.log({
            params
        })
        if (error) {
            debug.error({
                params
            })
            reject(error)
        }

        LookupUser(params, {
            resolve,
            reject
        })
    })
}


module.exports = {
    CreateCookieToken,
}
