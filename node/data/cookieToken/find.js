const rfr = require('rfr')
const dataRedis = rfr('data/redis')
const debug = rfr('util/debug')

function FindInternal(params, output) {
    dataRedis.client().hget('mapCookieToken', params.cookieToken, function(error, userRepresentation) {
        debug.log('FindCookieTokenUser', 'hget', {
            error
        }, {
            userRepresentation
        })
        if (userRepresentation) {
            output.resolve(JSON.parse(userRepresentation))
        } else {
            output.reject({
                loginFailed: true,
            })
        }
    })
}

function FindCookieTokenUser(cookieToken) {
    debug.log('FindCookieTokenUser', {
        cookieToken
    })
    return new Promise(function(resolve, reject) {
        FindInternal({
            cookieToken,
        }, {
            resolve,
            reject,
        })
    })
}

module.exports = {
    FindCookieTokenUser,
}
