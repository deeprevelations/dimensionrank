const cassandra = require('cassandra-driver')
const rfr = require('rfr')
const debug = rfr('util/debug')

var globalClient = null

// We use a global client because NodeJS is single-threaded.
function ResetCassandraClient(options) {
    debug.log({
        options: 'hidden'
    })
    globalClient = new cassandra.Client(options)

    return new Promise(function(resolve, reject) {
        globalClient.connect(function(error) {
            if (error) {
                reject(error)
            } else {
                resolve(undefined)
            }
        })
    })
}

function MakeColumnString(columnNames) {
    return columnNames.join(', ')
}

function MakeQuestionString(columnNames) {
    var buffer = ''
    for (var i = 0; i < columnNames.length; ++i) {
        if (i > 0) {
            buffer += ', '
        }
        buffer += '?'
    }
    return buffer
}

function MakeInsertQuery(tableName, columnNames, values) {
    var buffer = 'INSERT INTO '
    buffer += tableName
    buffer += ' ('
    const columnString = MakeColumnString(columnNames)
    buffer += columnString
    buffer += ') VALUES ('
    const questionString = MakeQuestionString(columnNames)
    buffer += questionString
    buffer += ')'
    return buffer
}

function MakeParamList(columnNames, values) {
    debug.log({
        columnNames
    }, {
        values
    })
    var buffer = []
    for (var i = 0; i < columnNames.length; ++i) {
        const columnName = columnNames[i]
        debug.log('MakeParamList', {
            columnName
        })
        const value = values[columnName]
        debug.log('MakeParamList', {
            value
        })
        buffer.push(value)
    }
    return buffer
}

function MakeFindByIdQuery(tableName, columnNames, id) {
    var buffer = 'SELECT '
    buffer += MakeColumnString(columnNames)
    buffer += ' FROM '
    buffer += tableName
    buffer += ' '
    buffer += " WHERE id = " + id
    return buffer
}

function MakeFindByIdSetQuery(tableName, columnNames, idList) {
    const idString = idList.join(', ')
    var buffer = 'SELECT '
    buffer += MakeColumnString(columnNames)
    buffer += ' FROM '
    buffer += tableName
    buffer += ' '
    buffer += " WHERE id in (" + idString + ")"
    return buffer
}

function MakeFindWhereQuery(tableName, columnNames, variable, value) {
    var buffer = 'SELECT '
    buffer += MakeColumnString(columnNames)
    buffer += ' FROM '
    buffer += tableName
    buffer += ' '
    buffer += " WHERE " + variable + " = '" + value + "'"
    return buffer
}

function SelectMostRecentQuery(tableName, columnNames, variable, value, orderBy, limit) {
    var buffer = 'SELECT '
    buffer += MakeColumnString(columnNames)
    buffer += ' FROM '
    buffer += tableName
    buffer += ' '
    buffer += " WHERE " + variable + " = '" + value + "'"
    buffer += ' ORDER BY ' + orderBy + ' DESC'
    buffer += ' LIMIT ' + limit
    return buffer
}

function Execute(query, params) {
//    return globalClient.execute(query, params, {
//        prepare: true
//    })
}

class CassandraTable {
    constructor(tableName, columnNames) {
        this.tableName = tableName
        this.columnNames = columnNames
    }
    Insert(values) {
        const query = MakeInsertQuery(this.tableName, this.columnNames, values)
        const valueList = MakeParamList(this.columnNames, values)
        debug.log({
            query,
            valueList,
        })
        return Execute(query, valueList)
    }

    FindById(id) {
        const query = MakeFindByIdQuery(this.tableName, this.columnNames, id)
        debug.log({
            query
        })
        return Execute(query, [])
    }

    FindByIdSet(idList) {
        const query = MakeFindByIdSetQuery(this.tableName, this.columnNames, idList)
        debug.log({
            query
        })
        return Execute(query, [])
    }

    FindWhere(variable, value) {
        const query = MakeFindWhereQuery(this.tableName, this.columnNames, variable, value)
        debug.log({
            query
        })
        return Execute(query, [])
    }

    SelectMostRecent(variable, value, orderBy, limit) {
        debug.log({
            variable,
            value,
            limit,
        })
        const query = SelectMostRecentQuery(this.tableName, this.columnNames, variable, value, orderBy, limit)
        debug.log({
            query
        })
        return Execute(query, [])
    }

    MakeDeleteQuery(id) {
        var buffer = 'DELETE FROM '
        buffer += this.tableName
        buffer += " WHERE id = " + id
        return buffer
    }

    Delete(id) {
        const query = MakeDeleteQuery(id)
        debug.log({
            query
        })
        return Execute(query, [])
    }
}

module.exports = {
    ResetCassandraClient,
    Execute,
    CassandraTable,
}
