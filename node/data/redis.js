const redis = require('redis')
const rfr = require('rfr')

const config = rfr('data/config')
const debug = rfr('util/debug')

var globalClient = null

function client() {
    return globalClient
}

function ResetRedisClient(options) {
    debug.log('ResetRedisClient', {
        options: 'hidden'
    })
    globalClient = redis.createClient(options)
    globalClient.on("error", function(error) {
        console.error('globalClient', error)
    })
    return undefined
}

module.exports = {
    ResetRedisClient,
    client,
}
