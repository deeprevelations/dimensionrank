const mongoose = require('mongoose')
const rfr = require('rfr')

const objects = rfr('data/objects')
const debug = rfr('util/debug')

// Returns a promise.
function CreateEmailAuthorization(email) {
    debug.log('CreateEmailAuthorization', {email})
    const emailApproval = new objects.EmailAuthorization({
        _id: new mongoose.Types.ObjectId(),
        email,
    })

    return emailApproval.save()
}

module.exports = {
    CreateEmailAuthorization,
}
