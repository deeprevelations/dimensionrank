const mongoose = require('mongoose')
const rfr = require('rfr')

const objects = rfr('data/objects')
const debug = rfr('util/debug')

// Returns a promise.
function CheckEmailAuthorization(email) {
	return objects.EmailAuthorization.findOne( { email } )
}

module.exports = {
    CheckEmailAuthorization,
}
