const redisConfig = {
    host: 'localhost',
}

const sizeOfHotBatch = 5
const sizeForHotReplenish = 5

const keyHotnessTarget = 'hot'

// The user's "hot queue" is the queue that they will get their recommendations
// from no matter what.
//
// The broadcasts on the hot queue do not need to be looked up. They are "hot".
function HotQueueName(subjectId) {
    return "hot:" + subjectId
}

function PersonalQueueName(subjectId) {
    return "personal:" + subjectId
}

module.exports = {
    redisConfig,
    sizeOfHotBatch,
    sizeForHotReplenish,
    HotQueueName,
    PersonalQueueName,
    keyHotnessTarget,
}
