const mongoose = require('mongoose')
const rfr = require('rfr')
const cassandraData = rfr('data/cassandra')
const debug = rfr('util/debug')
const TimeUuid = require('cassandra-driver').types.TimeUuid

const login_attempt_event = new cassandraData.CassandraTable('login_attempt_event', ['id', 'email'])
const application_event = new cassandraData.CassandraTable('application_event', ['id', 'email', 'fullName', 'profile'])

const ValidationAttemptSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    // Note: Email is the only thing currently required to create a user.
    // They can choose a unique userName later. Until then, their name is ''.
    token: {
        type: String,
        required: true
    },
})
const ValidationAttempt = mongoose.model('ValidationAttempt', ValidationAttemptSchema)

function LogValidationAttempt(params) {
    debug.log({
        params
    })
    const e = new ValidationAttempt({
        _id: new mongoose.Types.ObjectId(),
        token: params.token,
    })
    return e.save()
}

const LoginAttemptSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    // Note: Email is the only thing currently required to create a user.
    // They can choose a unique userName later. Until then, their name is ''.
    email: {
        type: String,
        required: true
    },
})
const LoginAttempt = mongoose.model('LoginAttempt', LoginAttemptSchema)

function LogLoginAttempt(params) {
    debug.log({
        params
    })
    const e = new LoginAttempt({
        _id: new mongoose.Types.ObjectId(),
        email: params.email,
    })
    return e.save()
}

const UserCreationSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    // Note: Email is the only thing currently required to create a user.
    // They can choose a unique userName later. Until then, their name is ''.
    email: {
        type: String,
        required: true
    },
})
const UserCreation = mongoose.model('UserCreation', UserCreationSchema)

function LogUserCreation(params) {
    debug.log({
        params
    })
    const e = new UserCreation({
        _id: new mongoose.Types.ObjectId(),
        email: params.email,
        userName: params.userName,
    })
    return e.save()
}

module.exports = {
    LogLoginAttempt,
    LogUserCreation,
    LogLoginAttempt,
    LogValidationAttempt,
    LogUserCreation,
}
