const rfr = require('rfr')
const cassandraData = rfr('data/cassandra')

function LogFeedSingleTransmission(params) {
    const tableName = 'feed_single_transmission'
    const columnNames = ['user_id', 'object_id']
    return cassandraData.Insert(tableName, columnNames, params)
}

module.exports = {
    LogFeedSingleTransmission,
}
