const getUrls = require('get-urls')
const rfr = require('rfr')
const mongoose = require('mongoose')

const dataRedis = rfr('data/redis')
const objects = rfr('data/objects')
const enums = rfr('data/enums')
const debug = rfr('util/debug')
const getResource = rfr('data/resource/get')
const config = rfr('data/config')
const updateRanking = rfr('ranking/update')
const tokenize = rfr('data/broadcast/tokenize')
const redact = rfr('data/broadcast/redact')

function AddForTraining(params) {
    debug.log({
        params
    })
    dataRedis.client().lpush('train_queue', JSON.stringify(params.alert_), function(error) {
        debug.error({
            error
        })
    })
}

function AddAlert(params) {
    debug.log({
        params
    })
    const variable = enums.ReducedVariableType(params.variable)
    const value = enums.ReducedValueType(parseInt(params.value))
    const alert_ = new objects.Alert({
        _id: new mongoose.Types.ObjectId(),
        consumerId: params.broadcast.producerId,
        producerId: params.authenticatedUser._id,
        producerName: params.authenticatedUser.userName,
        reference: params.referenceBroadcast ? params.referenceBroadcast._id : null,
        comment: params.broadcast._id,
    })
    debug.log({
        alert_
    })

    alert_.save().then(result => {
        debug.log({
            result
        })

        AddForTraining({
            ...params,
            alert_,
        })
        // Already returned.
    }).catch(error => {
        debug.error({
            error
        })
        // Already returned.
    })
}

function PropagateBroadcast(params, output) {
    debug.log({
        params
    })

		// Step 1: Update the alerts.
		AddAlert(params)

    // Step 2: Return.
    const publicBroadcast = redact.PolishBroadcast(params.broadcast, params.authenticatedUser)
    output.resolve(publicBroadcast)
}

function SaveBroadcast(params, output) {
    debug.log({
        params
    })
    const resource = params.resource ? params.resource._id.toString() : null
    const broadcast = new objects.Broadcast({
        _id: new mongoose.Types.ObjectId(),
        producerId: params.authenticatedUser._id,
        producerName: params.authenticatedUser.userName,
        reference: params.reference,
        resource,
        attachment: params.attachment,
        details: JSON.stringify({
            textarea: params.textarea,
            tokens: params.tokens,
            userName: params.authenticatedUser.userName,
            resource: params.resource,
            creationTime: Date.now(),
        }),
    })
    debug.log({
        broadcast
    })
    broadcast.save().then(broadcastResult => {
        debug.log({
            broadcast,
            broadcastResult,
        })
        PropagateBroadcast({
            ...params,
            broadcast
        }, output)
    }).catch(error => {
        debug.log({
            error
        })
        output.reject(error)
    })
}

function FetchPossibleResource(params, output) {
    debug.log({
        params
    })

    const urls = getUrls(params.textarea)

    var firstUrl = null
    for (const url of urls) {
        firstUrl = url
    }

    debug.log({
        firstUrl
    })

    if (firstUrl) {
        getResource.BlockingResouceFromUrl({
            ...params,
            url: firstUrl,
        }).then(resourceResult => {
            debug.log({
                resourceResult
            })
            SaveBroadcast({
                ...params,
                resource: resourceResult,
            }, output)
        }).catch(error => {
            debug.log({
                error
            })
            output.reject(error)
        })
    } else {
        // pass
        SaveBroadcast({
            ...params,
            resource: null,
        }, output)
    }
}

function FinishCreation(params, output) {
    debug.log({
        params
    })
    const tokens = tokenize.TokenizeTextarea(params.textarea)
    FetchPossibleResource({
        ...params,
        tokens,
    }, output)
}

module.exports = {
    FinishCreation,
}
