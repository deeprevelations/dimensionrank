function TokenizeTextarea(textarea) {
    if (!textarea) {
        return []
    }

    var buffer = []

    const lines = textarea.split('\n')
    for (const line of lines) {
        const parts = line.split(/(\s+)/)
        for (const part of parts) {
            const token = part.trim()
            if (token.length > 0) {
                buffer.push(token)
            }
        }
        buffer.push('<br>')
    }

    return buffer
}


module.exports = {
    TokenizeTextarea,
}