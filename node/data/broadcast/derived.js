const getUrls = require('get-urls')
const rfr = require('rfr')
const mongoose = require('mongoose')

const objects = rfr('data/objects')
const enums = rfr('data/enums')
const debug = rfr('util/debug')
const getResource = rfr('data/resource/get')
const config = rfr('data/config')
const updateRanking = rfr('ranking/update')
const tokenize = rfr('data/broadcast/tokenize')
const redact = rfr('data/broadcast/redact')

const common = rfr('data/broadcast/common')

function ComputeAttachment(params, output) {
    const redacted = redact.PolishBroadcast(params.referenceBroadcast, params.authenticatedUser)
    debug.log({
        redacted: JSON.stringify(redacted, null, 4),
    })

    common.FinishCreation({
        ...params,
        attachment: redacted.selfRatings,
    }, output)
}

// Look-up the reference, in every case.
function LookupReference(params, output) {
    debug.log({
        params
    })
    const oid = new mongoose.Types.ObjectId(params.reference)
    objects.Broadcast.findOne(oid)
        .then(referenceBroadcast => {
            debug.log({
                referenceBroadcast
            })
            if (referenceBroadcast) {
                ComputeAttachment({
                    ...params,
                    referenceBroadcast,
                }, output)
            } else {
                output.reject({
                    error: 'ObjectNotFound',
                    details: ['reference', params.reference],
                })
            }
        })
        .catch(error => {
            debug.error({
                error
            })
            output.reject(error)
        })
}

function CreateDerivedBroadcast(params) {
    debug.log({
        params
    })
    return new Promise(function(resolve, reject) {
        const error = debug.CheckArguments(params, ['authenticatedUser', 'textarea', 'reference', ])
        if (error) {
            reject(error)
        } else {
            LookupReference(params, {
                resolve,
                reject,
            })
        }
    })
}

module.exports = {
    CreateDerivedBroadcast,
}
