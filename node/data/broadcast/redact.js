const mongoose = require('mongoose')

const rfr = require('rfr')
const debug = rfr('util/debug')
const enums = rfr('data/enums')

function SelfRatings(userCounts, authenticatedUser) {
    var selfRatings = {}
    for (var variable = 0; variable < 3; ++variable) {
        const rating = userCounts[variable][authenticatedUser.userName]
        selfRatings[variable] = rating
    }
    return selfRatings
}

function SummarizeRatings(ratings, authenticatedUser) {
    debug.log({
        ratings,
    })
    var userCounts = {
        0: {},
        1: {},
        2: {},
    }
    var buffer = []
    for (const rating of ratings) {
        debug.log({
            rating,
        })
        userCounts[rating.variable][rating.userName] = rating.value
    }

    var stats = {
        0: {
            1: 0,
            0: 0,
        },
        1: {
            1: 0,
            0: 0,
        },
        2: {
            1: 0,
            0: 0,
        },
    }

    for (const [variable, nameToValue] of Object.entries(userCounts)) {
        debug.log({
            variable,
            nameToValue
        })
        for (const [name, value] of Object.entries(nameToValue)) {
            stats[variable][value] += 1
        }
    }

    const selfRatings = SelfRatings(userCounts, authenticatedUser)
    debug.log({
        stats,
        selfRatings,
    })
    return {
        stats,
        selfRatings,
    }
}

function ReturnLive(broadcast, authenticatedUser) {
    debug.log({
        broadcast,
        authenticatedUser,
    })
    const statsPart = SummarizeRatings(broadcast.ratings, authenticatedUser)
    const parsed = JSON.parse(broadcast.details)
    const key = broadcast._id.toString() + (new Date()).toString()
    const redacted = {
        _id: broadcast._id,
        key,
        producerId: broadcast.producerId,
        producerName: broadcast.producerName,
        reference: broadcast.reference,
        attachment: broadcast.attachment,
        task_embedding0: broadcast.task_embedding0,
        task_embedding1: broadcast.task_embedding1,
        ...statsPart,
        details: parsed,
        deleted: false,
    }
    debug.log({
        redacted
    })

    return redacted
}

function ReturnDeleted(broadcast, authenticatedUser) {
    debug.log({
        broadcast,
        authenticatedUser,
    })
    var parsed = JSON.parse(broadcast.details)
    parsed.textarea = 'This revelation has been deleted'
    parsed.tokens = ['This', 'revelation', 'has', 'been', 'deleted.']
    const key = broadcast._id.toString() + (new Date()).toString()
    const redacted = {
        _id: broadcast._id,
        deleted: true,
        key,
        producerId: broadcast.producerId,
        producerName: broadcast.producerName,
        //        mode: broadcast.mode,
        //        judgment: null,
        //        reference: null,
        ratings: [],
        details: parsed,
    }
    debug.log({
        redacted
    })

    return redacted
}

// Steps:
// 1) Make sure 'details' is parsed.
// 2) Remove all 'non public' votes from the broadcast.
function PolishBroadcast(broadcast, authenticatedUser) {
    debug.log({
        broadcast,
        authenticatedUser,
    })

    if (broadcast.deleted) {
        return ReturnDeleted(broadcast, authenticatedUser)
    } else {
        return ReturnLive(broadcast, authenticatedUser)
    }
}

module.exports = {
    PolishBroadcast,
}
