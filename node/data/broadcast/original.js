const getUrls = require('get-urls')
const rfr = require('rfr')
const mongoose = require('mongoose')

const objects = rfr('data/objects')
const enums = rfr('data/enums')
const debug = rfr('util/debug')
const getResource = rfr('data/resource/get')
const config = rfr('data/config')
const updateRanking = rfr('ranking/update')
const tokenize = rfr('data/broadcast/tokenize')

const common = rfr('data/broadcast/common')

function CreateOriginalBroadcast(params) {
    debug.log({
        params
    })
    return new Promise(function(resolve, reject) {
        const error = debug.CheckArguments(params, ['authenticatedUser', 'textarea'])
        if (error) {
            reject(error)
        } else {
            common.FinishCreation({
                ...params,
                reference: null,
								attachment: {},
            }, {
                resolve,
                reject,
            })
        }
    })
}

module.exports = {
    CreateOriginalBroadcast,
}
