const getUrls = require('get-urls')
const rfr = require('rfr')
const mongoose = require('mongoose')

const objects = rfr('data/objects')
const debug = rfr('util/debug')
const config = rfr('data/config')

function MarkDeleted(params, output) {
    debug.log({
        params
    })
    const query = {
        _id: params.broadcastId
    }
    const update = {
        $set: {
            deleted: true
        }
    }
    objects.Broadcast.updateOne(query, update).then(updateResult => {
        debug.log({
            updateResult
        })
        output.resolve(undefined)
    }).catch(error => {
        debug.log({
            error
        })
        output.reject(error)
    })
}

function CheckOwnership(params, output) {
    debug.log({
        params
    })

    const broadcastProducerId = params.broadcast.producerId
    const authenticatedUserId = params.authenticatedUser._id
    const isSame = broadcastProducerId == authenticatedUserId
    debug.log({
        broadcastProducerId,
        authenticatedUserId,
        isSame,
    })

    if (!isSame) {
        output.reject({
            error: 'NotTheOwner',
            details: [broadcastProducerId, authenticatedUserId, isSame],
        })
    } else {
        MarkDeleted(params, output)
    }
}

function RetrieveBroadcast(params, output) {
    debug.log({
        params
    })
    objects.Broadcast.findById(params.broadcastId).then(broadcast => {
        debug.log({
            broadcast
        })
        if (!broadcast) {
            output.reject({
                success: false,
                error: 'ObjectNotFound',
                details: ['broadcast', params.broadcastId],
            })
        } else {
            CheckOwnership({
                ...params,
                broadcast,
            }, output)
        }
    }).catch(error => {
        debug.log({
            error
        })
        output.reject(error)
    })
}

function DeleteBroadcast(params) {
    debug.log({
        params
    })
    return new Promise(function(resolve, reject) {
        const error = debug.CheckArguments(params, ['authenticatedUser', 'broadcastId'])
        if (error) {
            reject(error)
        } else {
            RetrieveBroadcast(params, {
                resolve,
                reject,
            })
        }
    })
}

module.exports = {
    DeleteBroadcast,
}
