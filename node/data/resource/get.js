const cheerio = require('cheerio')
const assert = require('assert')
const getUrls = require('get-urls')
const nodeFetch = require('node-fetch')
const mongoose = require('mongoose')
const rfr = require('rfr')
const debug = rfr('util/debug')

const objects = rfr('data/objects')

function ReduceHtmlTuple(params) {
    const $ = cheerio.load(params.html)
    const getMetatag = (name) =>
        $(`meta[name=${name}]`).attr('content') ||
        $(`meta[name="og:${name}"]`).attr('content') ||
        $(`meta[name="twitter:${name}"]`).attr('content')
    return {
        url: params.url,
        title: $('title').first().text(),
        favicon: $('link[rel="shortcut icon"]').attr('href'),
        // description: $('meta[name=description]').attr('content'),
        description: getMetatag('description'),
        image: getMetatag('image'),
        author: getMetatag('author'),
    }
}

function SaveResourceToDisk(params, output) {
    debug.log('SaveResourceToDisk', {
        params: 'hidden',
    })
    const tuple = ReduceHtmlTuple({
        url: params.url,
        html: params.html,
    })
    debug.log('SaveResourceToDisk', {
        tuple,
    })

    const resource = new objects.Resource({
        _id: new mongoose.Types.ObjectId(),
        ...tuple,
    })
    resource.save().then(saveResult => {
        debug.log('SaveResourceToDisk', {
            saveResult,
            resource,
        })
        output.resolve(resource)
    }).catch(error => {
        debug.log('SaveResourceToDisk', {
            error,
        })
        output.reject(error)
    })

}

function DownloadResource(params, output) {
    debug.log('DownloadResource', {
        params: 'hidden',
    })
    nodeFetch(params.url).then(fetchResult => {
        debug.log('DownloadResource', {
            fetchResult: 'hidden',
        })
        fetchResult.text().then(htmlResult => {
            debug.log('DownloadResource', {
                htmlResult: 'hidden',
            })
            SaveResourceToDisk({
                ...params,
                html: htmlResult,
            }, output)
        }).catch(error => {
            debug.log('DownloadResource', {
                error
            })
            output.reject(error)
        })
    }).catch(error => {
        debug.log('DownloadResource', {
            error
        })
        output.reject(error)
    })
}

function BlockingResouceFromUrlImpl(params, output) {
    debug.log('BlockingResouceFromUrlImpl', {
        params
    })
    objects.Resource.findOne({
        url: params.url,
    }).then(findResult => {
        debug.log('BlockingResouceFromUrlImpl', {
            findResult
        })
        if (findResult) {
            debug.log('BlockingResouceFromUrlImpl', 'branch-1')
            // return result early
            output.resolve(findResult)
            debug.log('BlockingResouceFromUrlImpl', 'branch-1-f')
        } else {
            debug.log('BlockingResouceFromUrlImpl', 'branch-2')
            // it's null, need to look-up
            DownloadResource(params, output)
        }
    }).catch(error => {
        debug.log('BlockingResouceFromUrlImpl', {
            error
        })
        output.reject(error)
    })
}

function BlockingResouceFromUrl(params) {
    debug.log('BlockingResouceFromUrl', {
        params
    })

    return new Promise(function(resolve, reject) {
        if (!'url' in params) {
            reject({
                error: 'InternalMissingArguments',
                details: ['url', params.url, ],
            })
        }
        BlockingResouceFromUrlImpl({
            url: params.url,
        }, {
            resolve,
            reject,
        })
    })
}

module.exports = {
    BlockingResouceFromUrl,
}
