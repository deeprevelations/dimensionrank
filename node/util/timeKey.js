const rfr = require('rfr')
const debug = rfr('util/debug')

// NOTE: Returns "true" iff "problem".
function CheckTimeKeyIsValid(keyMs) {
    debug.warn({
        keyMs
    })
    if (!keyMs) {
        return false
    }
    const currentMs = new Date().getTime()
    const diffMs = keyMs - currentMs

    const keyExpired = diffMs < 0
    const keyTooFar = diffMs > 5 * 60 * 1000
    const keyOk = !keyExpired && !keyTooFar
    debug.warn({
        currentMs,
        diffMs,
        keyMs,
        keyExpired,
        keyTooFar,
    })

    return keyOk
}

module.exports = {
    CheckTimeKeyIsValid,
}
