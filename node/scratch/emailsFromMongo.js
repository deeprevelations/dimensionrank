const mongoose = require('mongoose')

const rfr = require('rfr')
const globalOptions = rfr('globalOptions')
const app = rfr('api/app')
const objects = rfr("data/objects")
const dataBases = rfr('data/bases')
const dataCassandra = rfr('data/cassandra')
const debug = rfr('util/debug')

async function Main(argv) {
    const loadResult = await dataBases.InitFromFile(argv, ['mongo'])
    console.log('Main', {
        loadResult
    })

    var emailSet = new Set()
    const results = await objects.Application.find({})
    for (const result of results) {
        emailSet.add(result.email)
    }

    const size = emailSet.size
    console.log({
        size
    })
}

Main(process.argv)
