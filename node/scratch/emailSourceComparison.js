const mongoose = require('mongoose')

const rfr = require('rfr')
const globalOptions = rfr('globalOptions')
const app = rfr('api/app')
const objects = rfr("data/objects")
const dataBases = rfr('data/bases')
const dataCassandra = rfr('data/cassandra')
const debug = rfr('util/debug')

// Things in left, minus those in right.
function LeftDifference(left, right) {
	var result = []
	for (const e of left) {
		if (!right.has(e)) {
			result.push(e)
		}
	}
	return result
}

function SetUnion(left, right) {
	var result = new Set()
	for (const e of left) {
		result.add(e)
	}
	for (const e of right) {
		result.add(e)
	}
	return result
}

async function Main(argv) {
    const loadResult = await dataBases.InitFromFile(argv, ['cassandra', 'mongo'])
    console.log('Main', {
        loadResult
    })

		////////////////////////////////////////////////////////////////////////
    var emailSet1 = new Set()
    const results = await objects.Application.find({})
    for (const result of results) {
        emailSet1.add(result.email)
    }

    const size1 = emailSet1.size
    console.log({
        size1
    })

    const query = 'select email from login_attempt_event'
    const valueList = []
    console.log('Insert', {
        query
    })
    console.log('Insert', {
        valueList
    })

		////////////////////////////////////////////////////////////////////////
    const emailsResult = await dataCassandra.Execute(query, valueList)
    var emailSet2 = new Set()
    for (const row of emailsResult) {
        const email = row.email
        emailSet2.add(email)
    }

    const size2 = emailSet2.size
    console.log({
        size2
    })


		const difference1 = LeftDifference(emailSet1, emailSet2)
		const difference2 = LeftDifference(emailSet2, emailSet1)
		const union = SetUnion(emailSet1, emailSet2)
		console.log({
			difference1,
			difference2,
		})

		console.log({
			size1: difference1.length,
			size2: difference2.length,
			union: union.size,
		})


}

Main(process.argv)
