const mongoose = require('mongoose')

const rfr = require('rfr')
const globalOptions = rfr('globalOptions')
const app = rfr('api/app')
const debug = rfr('util/debug')
const dataBases = rfr('data/bases')
const dataCassandra = rfr('data/cassandra')

async function Main(argv) {
    const loadResult = await dataBases.InitFromFile(argv, ['cassandra'])
    console.log('Main', {
        loadResult
    })

    const query = 'select email from login_attempt_event'
    const valueList = []
    console.log('Insert', {
        query
    })
    console.log('Insert', {
        valueList
    })

    const emailsResult = await dataCassandra.Execute(query, valueList)
    var emailSet = new Set()
    for (const row of emailsResult) {
        const email = row.email
        emailSet.add(email)
    }

    const size = emailSet.size
    console.log({
        size
    })
}

Main(process.argv)
