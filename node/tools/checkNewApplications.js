const mongoose = require('mongoose')

const rfr = require('rfr')
const globalOptions = rfr('globalOptions')
const options = rfr('options')
const objects = rfr('data/objects')
const dataBases = rfr('data/bases')
const dataCassandra = rfr('data/cassandra')

async function Main(options) {
    console.log('Main', {
        options: 'hidden'
    })

    const loadResult = await dataBases.InitFromFile(options.config, ['mongo'])
    console.log('Main', {
        loadResult
    })

    const applicationResults = await objects.Application.find({})
    const authorizationResults = await objects.EmailAuthorization.find({})

    var authorizationSet = new Set()
    for (const authorization of authorizationResults) {
        authorizationSet.add(authorization.email.toLowerCase())
    }

    var applicationSet = new Set()
    for (const application of applicationResults) {
        applicationSet.add(application.email.toLowerCase())
    }


    console.log({
        authorizationSize: authorizationSet.size,
        applicationSize: applicationSet.size,
    })

    for (const email of applicationSet) {
        const contained = authorizationSet.has(email)
        if (!contained) {
            console.log(email)
        }
    }

		process.exit(0)
}

Main(options({
    'config': String,
}))
