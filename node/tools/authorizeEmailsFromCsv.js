const kReallyGo = false

const mongoose = require('mongoose')

const fs = require('fs')
const rfr = require('rfr')
const globalOptions = rfr('globalOptions')
const app = rfr('api/app')
const objects = rfr("data/objects")
const dataBases = rfr('data/bases')
const dataCassandra = rfr('data/cassandra')
const createEmailAuthorization = rfr('data/emailAuthorization/create')

async function Main(argv) {
    const loadResult = await dataBases.InitFromFile(argv, ['mongo'])
    console.log('Main', {
        loadResult
    })

    const bodyFname = argv[3]
    if (!bodyFname) {
        console.log('argv[3] must be the list of emails')
    }
    const data = fs.readFileSync(bodyFname, 'utf8')

    const emailList = data.split('\n')

    for (const line of emailList) {
        const email = line.trim()
        if (!email) {
            continue
        }

        console.log({
            email
        })
        const findResult = await objects.EmailAuthorization.findOne({
            email
        })
        console.log({
            findResult
        })
        if (!findResult) {
            if (kReallyGo) {
                const createResult = await createEmailAuthorization.CreateEmailAuthorization(email)
                console.log({
                    createResult
                })
            }
        }

    }
}

Main(process.argv)
