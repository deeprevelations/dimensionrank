CONFIG_FILE=$1
HOT_KEY=$2

if [ -z ${CONFIG_FILE} ]; then
	echo CONFIG_FILE not set;
	exit 1
fi


node tools/checkNewApplications.js --config ${CONFIG_FILE}

if [ -z ${HOT_KEY} ]; then
	echo HOT_KEY not set;
	echo quitting early;
	exit 0
fi

node tools/authorizeNewApplications.js --config ${CONFIG_FILE} --stage 1 --hot ${HOT_KEY}
node tools/authorizeNewApplications.js --config ${CONFIG_FILE} --stage 2 --hot ${HOT_KEY}
