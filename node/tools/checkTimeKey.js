const rfr = require('rfr')
const options = rfr('options')
const timeKey = rfr('util/timeKey')

function Main(options) {
    console.warn('Main', {
        options
    })

    const result = timeKey.CheckTimeKeyIsValid(options.key)
    console.warn({
        result
    })
}

Main(options({
    'key': Number,
}))
