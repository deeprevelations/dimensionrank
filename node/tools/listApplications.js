const objects = require("../data/objects")
const mongoose = require("mongoose")

const uri = process.env.DEEPREV_MONGOOSE_URI
console.log('mongoose uri', uri)
mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

objects.Application.find({}).exec().then(function(result) {
    var allParts = []
    var emails = new Set()
    for (const part of result) {
        console.log({
            part
        })
        allParts.push(part)
        emails.add(part.email)
    }
    const len = allParts.length
    console.log('Main', {
        len
    })
    console.log('Main', {
        emailLen: emails.size
    })

    for (const email of emails) {
        console.log({
            email
        })
    }
    mongoose.connection.close()
}).catch(function(error) {
    console.log('error', error)
    mongoose.connection.close()

})
