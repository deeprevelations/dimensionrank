// TODO(greg) make these command line options
const kReallySend = true
const kReallyUpdate = true

const fs = require('fs')
const rfr = require('rfr')
const sgMail = require('@sendgrid/mail')
const striptags = require('striptags')

const dataBases = rfr('data/bases')
const globalOptions = rfr('globalOptions')
const objects = rfr('data/objects')
const options = rfr('options')

function ReadNickname(emailLines) {
    return emailLines[0].trim()
}

function ReadSubject(emailLines) {
    return emailLines[1].trim()
}

function DoSubstitution(line, params) {
    // console.log('DoSubstitution', { params })
    var buffer = line
    for (const [key, value] of Object.entries(params)) {
        buffer = buffer.replace(key, value)
    }
    return buffer
}

function ReadHtml(emailLines, params) {
    const slice = emailLines.slice(2, emailLines.length)
    const raw = slice.join('\n')
    return DoSubstitution(raw, params)
}

function ReadText(emailLines, params) {
    var r = []
    const slice = emailLines.slice(1, emailLines.length)
    for (const line of slice) {
        r.push(striptags(line))
    }
    const raw = r.join('\n')
    return DoSubstitution(raw, params)
}

async function Main(options) {
    const loadResult = await dataBases.InitFromFile(options.config, ['mongo'])
    console.log('Main', {
        loadResult
    })

    const emailLines = fs.readFileSync(options.input, 'utf8').split('\n')

    const nickname = ReadNickname(emailLines)
    const subject = ReadSubject(emailLines)
    const list = options.list

    console.log('Main', {
        emailLines,
        nickname,
        subject,
        list,
    })

    var findResult = null
    if (!options.debug) {
        console.log('Main', 'Using production emailList')
        findResult = await objects.EmailAuthorization.find({})
    } else {
        console.log('Main', 'Using development emailList')
        findResult = [{
                email: 'greg@thinkdifferentagain.art',
                emailHistory: [],
                lists: ['HasUserName', 'any'],
            },
            {
                email: 'g.reg@thinkdifferentagain.art',
                emailHistory: [],
                lists: ['any'],
            },
        ]
    }

    const apiKey = globalOptions.options().sendgridApiKey
    var emailsHypotheticallySent = 0
    for (const user of findResult) {
        console.log({
            user
        })
        const email = user.email
        const emailHistory = user.emailHistory
        const lists = user.lists

        const params = {
            '$email': email,
        }
        const html = ReadHtml(emailLines, params)
        const text = ReadText(emailLines, params)


        if (emailHistory.indexOf(nickname) >= 0) {
            continue
        }
        const indexOf = lists.indexOf(list)
        if (indexOf < 0) {
            continue
        }

        console.log({
            lists,
            list,
            indexOf
        })
        console.log('Main', {
            html,
            text,
            email,
            emailHistory,
            index: emailHistory.indexOf(nickname),
        })

        emailsHypotheticallySent += 1
        if (options.hot && kReallySend) {
            sgMail.setApiKey(apiKey)
            const sendgridMessage = {
                to: email,
                from: 'Deep Revelations <mailman@deeprevelations.com>',
                subject,
                text,
                html,
            }
            console.log('Main', {
                sendgridMessage,
            })
            const sendResult = await sgMail.send(sendgridMessage)
            console.log('Main', {
                sendResult
            })
        } else {
            console.log('skip send')
        }


        if (options.hot && kReallyUpdate) {
            const updateResult = await objects.EmailAuthorization.updateOne({
                _id: user._id,
            }, {
                $push: {
                    emailHistory: nickname
                },
            })
        } else {
            console.log('skip update')
        }
    }

    console.log({
        emailsHypotheticallySent
    })

    process.exit(0)
}


Main(options({
    'config': String,
    'input': String,
    'list': String,
    'hot': Boolean,
    'debug': Boolean,
}))
