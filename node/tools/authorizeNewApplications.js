const mongoose = require('mongoose')
const rfr = require('rfr')
const options = rfr('options')
const objects = rfr('data/objects')
const dataBases = rfr('data/bases')
const createEmailAuthorization = rfr('data/emailAuthorization/create')
const timeKey = rfr('util/timeKey')

async function Main(options) {
    console.log('Main', {
        options
    })

    if (options.hot) {
        const keyOk = timeKey.CheckTimeKeyIsValid(options.hot)
        if (!keyOk) {
            console.error('key failed', {
                keyOk
            })
            process.exit(1)
        }
    }

    const loadResult = await dataBases.InitFromFile(options.config, ['mongo'])
    console.log('Main', {
        loadResult
    })


    var inputInterface = null
    var outputInterface = null
    if (options.stage == 1) {
        console.log('stage 1')
        inputInterface = objects.Application
        outputInterface = objects.EmailAuthorization
    } else if (options.stage == 2) {
        console.log('stage 2')
        inputInterface = objects.EmailAuthorization
        outputInterface = objects.User
    } else {
        console.log("'stage' must be in [1, 2]")
        process.exit(1)
    }

    // Step 1: Read in the data to sets.
    const applicationResults = await inputInterface.find({})
    // const outputResults = await objects.EmailAuthorization.find({})
    const outputResults = await outputInterface.find({})

    var applicationSet = new Set()
    for (const application of applicationResults) {
        applicationSet.add(application.email.toLowerCase())
    }
    console.log({
        applicationSet
    })

    var outputSet = new Set()
    for (const output of outputResults) {
        outputSet.add(output.email.toLowerCase())
    }
    console.log({
        outputSet
    })

    console.log({
        outputSize: outputSet.size,
        applicationSize: applicationSet.size,
    })

    // Step 2: Compute the difference.
    var difference = []
    for (const email of applicationSet) {
        const contained = outputSet.has(email)
        if (!contained) {
            console.log(email)
            difference.push(email)
        }
    }

    // Step 3: Add each output in the difference to the output set.
    for (const email of difference) {
        console.log({
            email
        })
        if (options.hot) {
            // const output = objects.EmailAuthorization({
            const output = outputInterface({
                _id: new mongoose.Types.ObjectId(),
                email,
            })

            const createResult = await output.save()

            console.log({
                createResult
            })
        } else {
            console.log('skip')
        }
    }

    process.exit(0)

}

Main(options({
    'config': String,
    'stage': Number,
    'hot': Number,
}))
