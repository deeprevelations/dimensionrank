const sgMail = require('@sendgrid/mail')
const nopt = require('nopt')
const mongoose = require('mongoose')
const fs = require('fs')

const rfr = require('rfr')

const globalOptions = rfr('globalOptions')
const objects = rfr('data/objects')
const createEmailAuthorization = rfr('data/emailAuthorization/create')
const mongoData = rfr('data/mongo')
const sendInviteUser = rfr('sendgrid/inviteUser')

const parsedArgv = nopt({
        'applicationId': String,
        'options': String,
    }, {},
    process.argv, 2)

console.log(parsedArgv)

const fileText = fs.readFileSync(parsedArgv.options)
const optionsTuple = JSON.parse(fileText)

// Init.
globalOptions.LoadOptions(optionsTuple)
const opts = globalOptions.options()
console.log({
    opts
})
mongoData.ResetMongoClient(globalOptions.options().mongo)

async function MarkApplicationResolved(params, output) {
    const query = {
        _id: new mongoose.Types.ObjectId(params.applicationId)
    }
    const update = {
        $set: {
            invitationTime: Date.now(),
        }
    }
    objects.Application.updateOne(query, update).then(modifyResult => {
        console.log('MarkApplicationResolved', {
            modifyResult
        })
        output.resolve(modifyResult)
    }).catch(error => {
        console.log('MarkApplicationResolved', {
            error
        })
        output.reject(error)
    })
}

async function SendTheEmail(params, output) {
    sendInviteUser.SendTheEmail(params).then(result => {
        console.log('SendTheEmail', {
            result
        })
        MarkApplicationResolved(params, output)
    }).catch(error => {
        console.log('SendTheEmail', {
            error
        })
        output.reject(error)
    })
}

async function CreateEmailToken(params, output) {
    console.log('CreateEmailToken', {
        params
    })
    const emailToken = new objects.EmailToken({
        _id: new mongoose.Types.ObjectId(),
        email: params.email,
    })

    console.log('CreateEmailToken', {
        emailToken
    })

    emailToken.save().then(function(result) {
        console.log({
            result
        })

        const token = result._id
        const url = globalOptions.options().vueBasePath + '/invitation/accept/' + token
        console.log('CreateEmailToken', {
            url
        })

        SendTheEmail({
            ...params,
            url
        }, output)

    }).catch(function(error) {
        console.log('error', error)
        output.reject(error)
    })
}

async function CreateEmailAuthorization(params, output) {
    console.log('CreateEmailAuthorization', {
        params
    })
    createEmailAuthorization.CreateEmailAuthorization(params.email).then(result => {
        if (result) {
            CreateEmailToken(params, output)
        } else {
            output.reject({
                error: 'unexpected',
                details: ['result is null']
            })
        }
    }).catch(error => {
        output.reject(error)
    })
}

async function LookupApplication(params, output) {
    const id = params.applicationId
    console.log('LookupApplication', {
        params
    })
    objects.Application.findById(id).then(application => {
        if (application) {
            CreateEmailAuthorization({
                ...params,
                email: application.email,
                fullName: application.fullName
            }, output)
        } else {
            output.reject({
                error: 'dbObjectNotFound',
                details: ['application', params.applicationId]
            })
        }
    }).catch(error => {
        output.reject(error)

    })
}

async function Main(params) {
    console.log('Main', {
        params
    })

    return new Promise(function(resolve, reject) {
        LookupApplication(params, {
            resolve,
            reject
        })
    })
}

Main(parsedArgv).then(success => {
    console.log('Main', {
        success
    })
    process.exit(0)
}).catch(error => {
    console.log('Main', {
        error
    })
    process.exit(1)
})