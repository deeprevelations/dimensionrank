const rfr = require('rfr')

const options = rfr('options')
const dataBases = rfr('data/bases')

async function Main(options) {
    console.log('Main', {
        options
    })

    const loadResult = await dataBases.InitDevelopmentOnly(options.config, ['mongo'])
    console.log({
        loadResult
    })

    process.exit(0)
}

Main(options({
    'config': String,
}))
