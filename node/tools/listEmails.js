// Changes the email field in some collection to make the email lowercase.
const mongoose = require('mongoose')
const rfr = require('rfr')
const options = rfr('options')
const objects = rfr('data/objects')
const dataBases = rfr('data/bases')

async function Main(options) {
    console.log('Main', {
        options: 'hidden'
    })

    const loadResult = await dataBases.InitFromFile(options.config, ['mongo'])
    console.log('Main', {
        loadResult: 'hidden'
    })


    const dataInterface = objects[options.collection]

    const results = await dataInterface.find({})
    var emails = []
    for (const result of results) {
        const email = result.email
        console.log({
            email,
        })
    }

    process.exit(0)
}

Main(options({
    'config': String,
    'collection': String,
}))