set -v

# 1. Install the cassandra database.
cd ~
echo "deb http://www.apache.org/dist/cassandra/debian 39x main" | sudo tee -a /etc/apt/sources.list.d/cassandra.sources.list # curl https://www.apache.org/dist/cassandra/KEYS | sudo apt-key add -
curl https://downloads.apache.org/cassandra/KEYS | sudo apt-key add -

sudo apt update
sudo apt install cassandra
sudo systemctl enable cassandra
sudo systemctl start cassandra
